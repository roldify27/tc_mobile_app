# Welcome to TC Mobile App!
### If you are reading this then you are chosen to be one of the pioneers for this game changing movement.
What you are seeing right now is the repository for TC Mobile App Version 2. Be one of its _**developers**_. A ton of effort is needed to reach the current features of the existing TC Mobile App. One might say, why **transfer**? why **change**?

The answer is simple. Times are changing, digital processes slowly requires more and more performance from the mobile device itself. Mobile Applications now requires a more intimate relationship to the device it runs to. And no relationship is more intimate than being an app that is native to the device itself.


This Project has phases and 3 phases are currently identified:

## Phase 1: Layout and Tasking
- [x] Dashboard UI
- [x] Savings Details UI
- [x] Fund Transfer UI
- [ ] Loans UI
- [ ] Insurance UI
- [ ] Bills Payment UI
- [ ] Loan Calc.

## Phase 2: Theming and State Management(Provider)
- [ ] Set default colors/swatches to global environment and use it across widgets. (1 for each Phase 1 task)
- [ ] Model the dummy Data which are used across widgets. (1 for each Phase 1 task)
- [ ] Use Provider State Management in accessing these Data. (1 for each Phase 1 task)

## Phase 3: APIs
- [x] Use _**existing**_ API in _**existing**_ features. (1 for each Phase 1 task)
