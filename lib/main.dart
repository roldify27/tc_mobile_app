import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_provider.dart';
import 'package:tcmobapp/provider/member_registration/member_registration_class.dart';
import 'package:tcmobapp/provider/timeout_session.dart';
import 'package:tcmobapp/provider/savings_details/savings_details_provider.dart';
import 'package:tcmobapp/provider/member_registration/member_registration_provider.dart';
import 'package:tcmobapp/provider/login/login_provider.dart';
import 'package:tcmobapp/provider/idle_provider.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/transaction_trapping/transactions_trapping_provider.dart';
import 'package:tcmobapp/screens/dashboard.dart';
import 'package:tcmobapp/screens/forgotpassword_verification.dart';
import 'package:tcmobapp/screens/forgotpassword_change.dart';
import 'package:tcmobapp/screens/login_screen.dart';
import 'package:tcmobapp/screens/savings_details.dart';
import 'package:tcmobapp/screens/terms_condition.dart';
import 'package:tcmobapp/screens/membership_verification.dart';
import 'package:tcmobapp/screens/membership_fields.dart';
import 'package:tcmobapp/screens/savings_details_view_all_trans.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:tcmobapp/screens/fund_transfer.dart';
import 'package:easy_debounce/easy_debounce.dart';
import 'package:tcmobapp/provider/user/user_details_provider.dart';

void main() {
  /* SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]); */

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _isLoggedIn = true;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (ctx) => TimeoutSession()),
        ChangeNotifierProvider(create: (ctx) => SavingsDetailsClassProvider()),
        ChangeNotifierProvider(create: (ctx) => MemberRegistrationProvider()),
        ChangeNotifierProvider(create: (ctx) => LoginProvider()),
        ChangeNotifierProvider(create: (ctx) => IdleProvider()),
        ChangeNotifierProvider(create: (ctx) => UserDetailsProvider()),
        ChangeNotifierProvider(create: (ctx) => FundTransferProvider()),
        ChangeNotifierProvider(create: (ctx) => TransactionTrappingProvider()),
      ],
      child: MaterialApp(
        builder: (context, widget) => ResponsiveWrapper.builder(
            BouncingScrollWrapper.builder(context, widget),
            // maxWidth: 1200,
            minWidth: 450,
            defaultScale: true,
            breakpoints: [
              ResponsiveBreakpoint.resize(450, name: MOBILE),
              ResponsiveBreakpoint.autoScale(800, name: TABLET),
              ResponsiveBreakpoint.autoScale(1000, name: TABLET),
              ResponsiveBreakpoint.resize(1200, name: DESKTOP),
              ResponsiveBreakpoint.autoScale(2460, name: "4K"),
            ],
            background: Container(color: Color(0xFFF5F5F5))),
        title: 'Flutter Demo',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: createMaterialColor(Color(0xFF062d9e)),
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
          // Define the default font family.
          fontFamily: 'Montserrat',
          scaffoldBackgroundColor: Colors.white,
          disabledColor: Colors.white,
        ),
        home: !_isLoggedIn ? Dashboard() : LoginScreen(),
        routes: {
          Dashboard.routeName: (ctx) => Dashboard(),
          LoginScreen.routeName: (ctx) => LoginScreen(),
          SavingsDetails.routeName: (ctx) => SavingsDetails(),
          SavingsDetailsViewAllTransScreen.routeName: (ctx) =>
              SavingsDetailsViewAllTransScreen(),
          TermsConditions.routeName: (ctx) => TermsConditions(),
          MembershipVerification.routeName: (ctx) => MembershipVerification(),
          MembershipFields.routeName: (ctx) => MembershipFields(),
          ForgotPasswordVerification.routeName: (ctx) =>
              ForgotPasswordVerification(),
          ForgotPasswordChange.routeName: (ctx) => ForgotPasswordChange(),
          FundTransfer.routeName: (ctx) => FundTransfer(),
        },
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
