// import 'dart:html';

import 'package:flutter/material.dart';

import 'package:tcmobapp/constants.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tcmobapp/screens/fund_transfer.dart';

class MenuIcons extends StatelessWidget {
  final IconData iconPath;
  final String iconTitle;
  final Widget menuPath;

//  MenuIcons({this.icon});

  const MenuIcons({Key key, this.iconPath, this.iconTitle, this.menuPath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          InkWell(
            highlightColor: Colors.red,
            onTap: () {
              Navigator.of(context).push(
                PageTransition(
                    type: PageTransitionType.fade,
                    duration: Duration(milliseconds: 150),
                    reverseDuration: Duration(milliseconds: 150),
                    child: menuPath,
                    inheritTheme: true,
                    ctx: context),
              );
            },
            child: Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: kWhiteColor,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 2,
                    blurRadius: 5,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: Icon(
                iconPath,
                size: 20,
                color: kPrimaryColor,
              ),
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Text(iconTitle,
              style: TextStyle(
                  fontSize: 10,
                  // fontWeight: FontWeight.bold,
                  color: Colors.white)),
        ],
      ),
    );
  }
}
