import 'package:flutter/material.dart';
import 'package:tcmobapp/screens/dashboard.dart';

class HolderForTimer extends StatefulWidget {
  @override
  _HolderForTimerState createState() => _HolderForTimerState();
}

class _HolderForTimerState extends State<HolderForTimer>
    with WidgetsBindingObserver {
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('sure nakaabot ko diri.');
    // TODO: implement didChangeAppLifecycleState

    if (state == AppLifecycleState.inactive) {
      // Navigator.of(context)
      //     .pushNamedAndRemoveUntil(Dashboard.routeName, (route) => false);
      Navigator.of(context).pushReplacementNamed(Dashboard.routeName);
    }

    if (state == AppLifecycleState.paused) {
      print('i went for a pause for a while.');
    }

    super.didChangeAppLifecycleState(state);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
