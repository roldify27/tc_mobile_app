import 'package:flutter/material.dart';

enum TextFieldType { Email, RegularText, Password, Number }

class RoundedTextField extends StatefulWidget {
  final FocusNode focusNode;
  final TextFieldType textFieldType;
  final String labelText;

  const RoundedTextField({
    Key key,
    @required this.focusNode,
    @required this.textFieldType,
    @required this.labelText,
  }) : super(key: key);

  @override
  _RoundedTextFieldState createState() => _RoundedTextFieldState();
}

class _RoundedTextFieldState extends State<RoundedTextField> {
  TextInputType getInputType() {
    if (widget.textFieldType != TextFieldType.Number)
      return TextInputType.text;
    else
      return TextInputType.numberWithOptions(decimal: true);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.focusNode.addListener(_handleFocusChanged);
  }

  void _handleFocusChanged() {
    setState(() {
      FocusScope.of(context).requestFocus(widget.focusNode);
    });
    print('hi' + widget.focusNode.hasFocus.toString());

    if (!widget.focusNode.hasFocus) {
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  }

  /* // Initially password is obscure
  bool _obscureText = true;

  String _password;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  } */

  @override
  void dispose() {
    // TODO: implement dispose
    widget.focusNode.removeListener(_handleFocusChanged);
    // The attachment will automatically be detached in dispose().
    widget.focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText:
          widget.textFieldType == TextFieldType.Password ? true : false,
      focusNode: widget.focusNode,
      cursorColor: Color(0xFF353887),
      decoration: InputDecoration(
        labelText: widget.labelText,
        labelStyle: TextStyle(
            color: widget.focusNode.hasFocus
                ? Color(0xFF353887)
                : Color(0xFF828282)),
        fillColor: Color(0xffF2F2F2),
        filled: true,
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(10.0),
          borderSide: new BorderSide(),
        ),
        enabledBorder: new OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: BorderSide(
            color: Color(0xFFE0E0E0),
          ),
        ),
        focusedBorder: new OutlineInputBorder(
          borderRadius: BorderRadius.circular(10.0),
          borderSide: BorderSide(
            color: Color(0xFF353887),
          ),
        ),
      ),
      keyboardType: getInputType(),
      validator: (value) {
        if (value.isEmpty || !value.contains('@')) {
          return 'Invalid email!';
        }
      },
    );
  }
}
