/* Notes
- Remove Unused Packages
- Categorized Imports 
- Put Commas' for Proper Indexing*/

// DART

// PACKAGES
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// CONSTANT
import '../../constants.dart';

// MODELS

// PROVIDERS
import 'package:tcmobapp/provider/savings_details/savings_details_provider.dart';

// SCREENS

// WIDGETS

class FundTransferHeaderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final savingsDetailsProvider =
        Provider.of<SavingsDetailsClassProvider>(context);

    return Container(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            // height: double.infinity,
            // margin: EdgeInsets.only(top:),
            decoration: BoxDecoration(
                // borderRadius: BorderRadius.only(
                //     topLeft: Radius.circular(10),
                //     topRight: Radius.circular(10)),
                ),
            child: Container(
              decoration: BoxDecoration(
                /* border: Border.all(
                                      color: Colors.grey.withOpacity(0.50),
                                    ), */
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              margin: EdgeInsets.all(20),
              // padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  savingsDetailsProvider.isLoading
                      ? skeletonInstance()
                      : Container(
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  // decoration: BoxDecoration(color: Colors.red),
                                  // padding: EdgeInsets.only(left: 10),
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.arrow_back_ios_rounded,
                                      size: 20,
                                    ),
                                    color: kPrimaryColor,
                                    onPressed: () {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                      Navigator.of(context)
                                          .pop(); //-->context here is this widget's position in the widget tree.
                                    },
                                  ),
                                  alignment: Alignment.centerLeft,
                                ),
                              ),
                              Expanded(
                                flex: 4,
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Fund Transfer',
                                    style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold,
                                        color: kDarkerFontColor3),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(),
                              ),

                              /* Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 5),
                                child: Align(
                                  alignment: Alignment
                                      .centerRight, /* Text(
                                    'View All',
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: kPrimarySwatchColor,
                                        fontWeight: FontWeight.bold),
                                  ), */
                                ),
                              ),
                            ), */
                            ],
                          ),
                        ),
                  /* Container(
                    margin: EdgeInsets.only(top: 10),
                    child: savingsDetailsProvider.isLoading
                        ? Row(
                            children: [
                              recentTransactionsFilterLazyLoader(),
                              recentTransactionsFilterLazyLoader(),
                              recentTransactionsFilterLazyLoader(),
                            ],
                          )
                        : Row(
                            children: [
                              recentTransactionsFilter('All'),
                              recentTransactionsFilter('Share Capital'),
                              recentTransactionsFilter('Savings'),
                              recentTransactionsFilter('Time Deposits'),
                            ],
                          ),
                  ), */
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Container recentTransactionsFilter(String rtfText) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: ElevatedButton(
        onPressed: () {},
        /* shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(kButtonCircularRadius),
        ), */ // optional, in order to add additional space around text if needed
        style: ElevatedButton.styleFrom(
          primary: kWhiteColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(kButtonCircularRadius),
          ),
        ),
        child: Text(
          rtfText,
          style: TextStyle(
            color: kPrimaryColor,
            fontSize: 10,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Container recentTransactionsFilterLazyLoader() {
    return Container(
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
        color: kWhiteColor,
        borderRadius: BorderRadius.circular(30),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 1,
            blurRadius: 1,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: SizedBox(width: 100, child: skeletonInstance()),
    );
  }
}
