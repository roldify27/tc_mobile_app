import 'dart:io';
import 'dart:typed_data';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart'
    as WidgetConditional;
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'dart:ui';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_own_result_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_provider.dart';
import 'package:ticketview/ticketview.dart';
import 'package:screenshot/screenshot.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ProgressIndicatorWidget extends StatefulWidget {
  const ProgressIndicatorWidget({
    Key key,
  }) : super(key: key);

  @override
  _ProgressIndicatorWidgetState createState() =>
      _ProgressIndicatorWidgetState();
}

class _ProgressIndicatorWidgetState extends State<ProgressIndicatorWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AlertDialog(
            backgroundColor: Colors.transparent,
            /* shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.0))), */
            // contentPadding: EdgeInsets.all(32.0),
            // actionsPadding: EdgeInsets.all(15),
            title: Center(
              child: Container(
                // padding: EdgeInsets.all(20),
                // width: 100,
                color: Colors.transparent,
                child: Column(
                  children: [
                    Container(
                      color: Colors.transparent,
                      child: Center(
                        child: Image.asset(
                          "assets/images/tc-logo-looper.gif",
                          height: 100.0,
                          width: 100.0,
                        ),
                      ),
                    ),
                    /* Lottie.asset(
                              'assets/images/loading.json',
                              alignment: Alignment.center,
                              addRepaintBoundary: false,
                              fit: BoxFit.contain,
                              repeat: true,
                              height: 200,
                              width: 200,
                            ), */
                    /* Container(
                              margin: EdgeInsets.symmetric(
                                horizontal: 10,
                                vertical: 15,
                              ),
                              child: Text(
                                "Processing Fund Transfer",
                                style: TextStyle(
                                    color: Color(0xFF00238A),
                                    fontSize: kDialogHeaderFontSize,
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 1),
                              ),
                            ) */
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
