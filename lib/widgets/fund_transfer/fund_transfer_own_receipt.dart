import 'dart:io';
import 'dart:typed_data';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart'
    as WidgetConditional;
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'dart:ui';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_own_result_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_provider.dart';
import 'package:tcmobapp/screens/dashboard.dart';
import 'package:ticketview/ticketview.dart';
import 'package:screenshot/screenshot.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';

class FundTransferOwnReceipt extends StatefulWidget {
  final FundTransferOwnResultClass result;

  const FundTransferOwnReceipt({
    @required this.result,
    Key key,
  }) : super(key: key);

  @override
  _FundTransferOwnReceiptState createState() => _FundTransferOwnReceiptState();
}

class _FundTransferOwnReceiptState extends State<FundTransferOwnReceipt> {
  FundTransferClass fundTransferDetails = new FundTransferClass();

  ScreenshotController screenshotController = ScreenshotController();
  Uint8List _imageFile;
  bool _isScreenshotNotAllowed = false;

  @override
  void initState() {
    super.initState();
    syncMethods();
    _checkPlatform();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  Future<bool> syncMethods() async {
    try {
      await getFundTransferDetails();
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<void> getFundTransferDetails() async {
    try {
      var _fundTransferProvider =
          await Provider.of<FundTransferProvider>(context, listen: false);

      fundTransferDetails = await _fundTransferProvider.getFundTransferDetails;
    } catch (e) {
      print(e);
    } finally {}
  }

  Future<void> getReceiptScreenShot() async {
    if (await Permission.storage.isGranted) {
      screenshotController.capture().then((Uint8List image) async {
        //print("Capture Done");
        setState(() {
          _imageFile = image;
        });
        final result = await ImageGallerySaver.saveImage(
            image); // Save image to gallery,  Needs plugin  https://pub.dev/packages/image_gallery_saver
        _toastInfo("Receipt saved to gallery");
      }).catchError((onError) {
        print(onError);
        _toastInfo("Error saving receipt");
      });
    } else {
      _requestPermission();
    }
  }

  _requestPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();

    final info = statuses[Permission.storage].toString();
    print(info);
    _toastInfo(info);

    if (await Permission.storage.isGranted) {
      getReceiptScreenShot();
    }
  }

  _toastInfo(String info) {
    Fluttertoast.showToast(msg: info, toastLength: Toast.LENGTH_LONG);
  }

  _checkPlatform() {
    if (Platform.isWindows) {
      _isScreenshotNotAllowed = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AlertDialog(
              backgroundColor: Colors.transparent,
              /* shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.0))), */
              // contentPadding: EdgeInsets.all(32.0),
              // actionsPadding: EdgeInsets.all(15),
              title: FutureBuilder(
                  future: syncMethods(),
                  builder: (context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      return buildReceipt();
                    } else {
                      return new Container();
                    }
                  })

              /* Container(
                      // padding: EdgeInsets.all(20),
                      // width: 40,
                      child: Column(
                        children: [
                          Lottie.asset(
                            'assets/images/success.json',
                            alignment: Alignment.center,
                            fit: BoxFit.contain,
                            repeat: false,
                            height: 70,
                            width: 70,
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                              horizontal: 10,
                              vertical: 15,
                            ),
                            child: Text(
                              "Fund Transfer Succesful",
                              style: TextStyle(
                                  color: Color(0xFF00238A),
                                  fontSize: kDialogHeaderFontSize,
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 1),
                            ),
                          ),
                          Container(
                            child: FadeIn(
                              delay: Duration(milliseconds: 500),
                              child: Column(children: [
                                Text("asfdqweqweq"),
                                Text("asdasdqweqweq"),
                                Text("asdasdqweqweq"),
                                Text("asdasdqweqweq"),
                              ]),
                            ),
                          ),
                        ],
                      ),
                    ) */

              /* WidgetConditional.Conditional.single(
              context: context,
              conditionBuilder: (BuildContext context) => onProgress,
              widgetBuilder: (BuildContext context) {
                return x;
              },
              fallbackBuilder: (BuildContext context) {
                return Container(
                  // padding: EdgeInsets.all(20),
                  child: Lottie.asset(
                    'assets/images/progress-2.json',
                    alignment: Alignment.center,
                    fit: BoxFit.contain,
                    repeat: true,
                    height: 170,
                    width: 170,
                  ),
                );
              },
            ), */
              ),
          FadeInUp(
            duration: Duration(milliseconds: 500),
            delay: Duration(milliseconds: 1000),
            from: 30,
            child: ElevatedButton(
              onPressed: () {
                // Navigator.of(context).pop();
                Navigator.of(context).popUntil((route) => route.isFirst);
                Navigator.of(context).pushReplacementNamed(Dashboard.routeName);
              },
              /* shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(kButtonCircularRadius),
                                                    ), */ // optional, in order to add additional space around text if needed
              style: ElevatedButton.styleFrom(
                primary: kPrimaryColor,
                padding: EdgeInsets.all(25),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(kButtonCircularRadius),
                ),
              ),
              child: Text(
                'Continue',
                style: TextStyle(
                  color: kWhiteColor,
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  FadeIn buildReceipt() {
    return FadeIn(
      child: Screenshot(
        controller: screenshotController,
        child: TicketView(
          /* backgroundPadding:
                            EdgeInsets.symmetric(vertical: 0, horizontal: 0), */
          backgroundColor: Colors.transparent,
          contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
          drawArc: false,
          triangleAxis: Axis.vertical,
          borderRadius: 10,
          // drawDivider: false,
          dividerColor: kWhiteColor,
          drawTriangle: false,
          /* trianglePos: .5, */
          child: Container(
            padding: EdgeInsets.all(30),
            // width: 40,
            child: Column(
              children: [
                FadeIn(
                  duration: Duration(milliseconds: 500),
                  delay: Duration(milliseconds: 1000),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: IconButton(
                      onPressed: () {
                        if (_isScreenshotNotAllowed == false) {
                          getReceiptScreenShot();
                        }
                      },
                      icon: Icon(
                        Icons.download_rounded,
                        size: 24,
                        color: (_isScreenshotNotAllowed)
                            ? Colors.grey
                            : kPrimaryColor,
                      ),
                    ),
                  ),
                ),
                Lottie.asset(
                  'assets/images/success-4.json',
                  alignment: Alignment.center,
                  fit: BoxFit.contain,
                  repeat: false,
                  height: 150,
                  width: 150,
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 15,
                  ),
                  child: Text(
                    "Transaction Succesful",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color(0xFF00238A),
                        fontSize: kDialogHeaderFontSize - 3,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                  ),
                ),
                FadeIn(
                  delay: Duration(milliseconds: 500),
                  child: Container(
                    // padding: EdgeInsets.all(10),
                    margin: EdgeInsets.only(
                      top: 15,
                      bottom: 30,
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: kPrimaryColor)),
                    child: Column(children: [
                      Container(
                        decoration: BoxDecoration(
                            color: kPrimaryColor,
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(15),
                              topRight: Radius.circular(15),
                            ),
                            border: Border.all(color: Colors.transparent)),
                        // padding: EdgeInsets.all(15),
                        child: IntrinsicHeight(
                          child: Row(children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(15)),
                                    border:
                                        Border.all(color: Colors.transparent)),
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: FittedBox(
                                    alignment: Alignment.center,
                                    fit: BoxFit.scaleDown,
                                    child: Text(
                                      'Ref',
                                      style: TextStyle(
                                          color: kWhiteColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12,
                                          letterSpacing: 1),
                                    )),
                              ),
                            ),
                            VerticalDivider(
                              color: kPrimaryColor.withOpacity(0.50),
                              thickness: 0,
                              width: 0,
                            ),
                            Expanded(
                              flex: 3,
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(15)),
                                    border:
                                        Border.all(color: Colors.transparent)),
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: FittedBox(
                                    alignment: Alignment.centerRight,
                                    fit: BoxFit.scaleDown,
                                    child: Text(
                                      widget.result.reference,
                                      style: TextStyle(
                                          color: kWhiteColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16,
                                          letterSpacing: 1),
                                      overflow: TextOverflow.clip,
                                    )),
                              ),
                            ),
                          ]),
                        ),
                      ),
                      Divider(
                        color: kPrimaryColor.withOpacity(0.50),
                        thickness: 0,
                        height: 0,
                      ),
                      Container(
                        child: IntrinsicHeight(
                          child: Row(children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 10),
                                child: FittedBox(
                                    alignment: Alignment.center,
                                    fit: BoxFit.scaleDown,
                                    child: Text(
                                      'Type',
                                      style: TextStyle(
                                          fontSize: 12, letterSpacing: 1),
                                    )),
                              ),
                            ),
                            VerticalDivider(
                              color: kPrimaryColor.withOpacity(0.50),
                              thickness: 0,
                              width: 0,
                            ),
                            Expanded(
                              flex: 3,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: FittedBox(
                                    alignment: Alignment.center,
                                    fit: BoxFit.scaleDown,
                                    child: Text(
                                      fundTransferDetails.transferTypeFull,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 16, letterSpacing: 1),
                                      overflow: TextOverflow.clip,
                                    )),
                              ),
                            ),
                          ]),
                        ),
                      ),
                      Divider(
                        color: kPrimaryColor.withOpacity(0.50),
                        thickness: 0,
                        height: 0,
                      ),
                      Container(
                        child: IntrinsicHeight(
                          child: Row(children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 10),
                                child: FittedBox(
                                    alignment: Alignment.center,
                                    fit: BoxFit.scaleDown,
                                    child: Text(
                                      'Amt',
                                      style: TextStyle(
                                          fontSize: 12, letterSpacing: 1),
                                    )),
                              ),
                            ),
                            VerticalDivider(
                              color: kPrimaryColor.withOpacity(0.50),
                              thickness: 0,
                              width: 0,
                            ),
                            Expanded(
                              flex: 3,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: FittedBox(
                                    alignment: Alignment.centerLeft,
                                    fit: BoxFit.scaleDown,
                                    child: Text(
                                      NumberFormat.currency(
                                        locale: 'en_PH',
                                        name: '₱',
                                        decimalDigits: 2,
                                      )
                                          .format(fundTransferDetails.amount)
                                          .toString(),
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontSize: 14, letterSpacing: 1),
                                      overflow: TextOverflow.clip,
                                    )),
                              ),
                            ),
                          ]),
                        ),
                      ),
                      Divider(
                        color: kPrimaryColor.withOpacity(0.50),
                        thickness: 0,
                        height: 0,
                      ),
                      Container(
                        child: IntrinsicHeight(
                          child: Row(children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 10),
                                child: FittedBox(
                                    alignment: Alignment.center,
                                    fit: BoxFit.contain,
                                    child: Text(
                                      'From',
                                      style: TextStyle(
                                          fontSize: 12, letterSpacing: 1),
                                    )),
                              ),
                            ),
                            VerticalDivider(
                              color: kPrimaryColor.withOpacity(0.50),
                              thickness: 0,
                              width: 0,
                            ),
                            Expanded(
                              flex: 3,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        /* padding: EdgeInsets.symmetric(
                                                          vertical: 5,
                                                          horizontal: 10), */
                                        child: Text(
                                          kUserDetails.transactionName,
                                          style: TextStyle(
                                              fontSize: 10, letterSpacing: 1),
                                          overflow: TextOverflow.clip,
                                        ),
                                      ),
                                      Container(
                                        /* padding: EdgeInsets.symmetric(
                                                          vertical: 5,
                                                          horizontal: 10), */
                                        child: Text(
                                          fundTransferDetails
                                              .source.productName,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: kPrimaryColor
                                                  .withOpacity(0.70),
                                              fontSize: 12,
                                              letterSpacing: 1),
                                          overflow: TextOverflow.clip,
                                        ),
                                      ),
                                      Container(
                                        /* padding: EdgeInsets.symmetric(
                                                          vertical: 5,
                                                          horizontal: 10), */
                                        child: Text(
                                          fundTransferDetails
                                              .source.accountNumber,
                                          style: TextStyle(
                                              fontSize: 10, letterSpacing: 1),
                                          overflow: TextOverflow.clip,
                                        ),
                                      ),
                                    ]),
                              ),
                              /* Container(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 5, horizontal: 10),
                                              child: FittedBox(
                                                alignment: Alignment.centerRight,
                                                fit: BoxFit.scaleDown,
                                                child: Text(
                                                  '12345',
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      letterSpacing: 1),
                                                  overflow: TextOverflow.clip,
                                                ),
                                              ),
                                            ), */
                            ),
                          ]),
                        ),
                      ),
                      Divider(
                        color: kPrimaryColor.withOpacity(0.50),
                        thickness: 0,
                        height: 0,
                      ),
                      Container(
                        child: IntrinsicHeight(
                          child: Row(children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 10),
                                child: FittedBox(
                                    alignment: Alignment.center,
                                    fit: BoxFit.contain,
                                    child: Text(
                                      'To',
                                      style: TextStyle(
                                          fontSize: 12, letterSpacing: 1),
                                    )),
                              ),
                            ),
                            VerticalDivider(
                              color: kPrimaryColor.withOpacity(0.50),
                              thickness: 0,
                              width: 0,
                            ),
                            Expanded(
                              flex: 3,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        /* padding: EdgeInsets.symmetric(
                                                          vertical: 5,
                                                          horizontal: 10), */
                                        child: Text(
                                          kUserDetails.transactionName,
                                          style: TextStyle(
                                              fontSize: 10, letterSpacing: 1),
                                          overflow: TextOverflow.clip,
                                        ),
                                      ),
                                      Container(
                                        /* padding: EdgeInsets.symmetric(
                                                          vertical: 5,
                                                          horizontal: 10), */
                                        child: Text(
                                          fundTransferDetails
                                              .transferToOwn.productName,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: kPrimaryColor
                                                  .withOpacity(0.70),
                                              fontSize: 12,
                                              letterSpacing: 1),
                                          overflow: TextOverflow.clip,
                                        ),
                                      ),
                                      Container(
                                        /* padding: EdgeInsets.symmetric(
                                                          vertical: 5,
                                                          horizontal: 10), */
                                        child: Text(
                                          fundTransferDetails
                                              .transferToOwn.accountNumber,
                                          style: TextStyle(
                                              fontSize: 10, letterSpacing: 1),
                                          overflow: TextOverflow.clip,
                                        ),
                                      ),
                                    ]),
                              ),
                              /* Container(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 5, horizontal: 10),
                                              child: FittedBox(
                                                alignment: Alignment.centerRight,
                                                fit: BoxFit.scaleDown,
                                                child: Text(
                                                  '12345',
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      letterSpacing: 1),
                                                  overflow: TextOverflow.clip,
                                                ),
                                              ),
                                            ), */
                            ),
                          ]),
                        ),
                      ),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
