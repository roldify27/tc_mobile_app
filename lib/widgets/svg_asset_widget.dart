import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SVGAssetWidget extends StatelessWidget {
  final String assetName;
  final Color color;
  final String labelText;

  const SVGAssetWidget({
    Key key,
    this.assetName,
    this.color,
    this.labelText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      assetName,
      color: Colors.red,
      semanticsLabel: labelText,
    );
  }
}
