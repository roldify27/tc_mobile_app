import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/widgets/svg_asset_widget.dart';

class OTPCodeVerificationWidget extends StatefulWidget {
  final String phoneNumber;
  final String otp;
  final Function onSubmit;
  final String transactionType;

  OTPCodeVerificationWidget({
    Key key,
    @required this.phoneNumber,
    @required this.otp,
    @required this.onSubmit,
    @required this.transactionType,
  }) : super(key: key);

  @override
  _OTPCodeVerificationWidgetState createState() =>
      _OTPCodeVerificationWidgetState();
}

class _OTPCodeVerificationWidgetState extends State<OTPCodeVerificationWidget> {
  TextEditingController textEditingController = TextEditingController();
  // ..text = "123456";

  // ignore: close_sinks
  StreamController<ErrorAnimationType> errorController;
  FocusNode otpFocusNode = new FocusNode();

  bool hasError = false;
  bool pinCodeLenghtIsMax = false;
  String currentText = "";
  final formKey = GlobalKey<FormState>();
  String code = "";

  Timer _timer;
  // int _start = 300;
  bool _isInit = true;

  void startResendTimer() {
    code = widget.otp;
    String transactionType = widget.transactionType;

    if (kTimerTransactionType != transactionType) {
      //not same transaction
      setState(() {
        kTimerTransactionType = transactionType;
        kRestoredCode = widget.otp;
        kResetOTPTimer();
      });
    } else {
      //same transaction
      if (kStart < 1) {
        //if resend duration is 0
        kResetOTPTimer();
      } else {
        //if resend duration is still not depleted
        code = kRestoredCode;
        kRestored = DateTime.now();
        kGetSetRestoredTimer();
      }
    }

    const oneSec = const Duration(seconds: 1);
    kTimer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (kStart == 0) {
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            kStart--;
          });
        }
      },
    );

    print("OTPPPPPPPPPPPPP: $code");
  }

  /* void startResendTimer2() {
    String transactionType = widget.transactionType;

    if (kTimerTransactionType != transactionType) {
      kTimerTransactionType = transactionType;
      kStart = kTimerDuration;
    }

    const oneSec = const Duration(seconds: 1);
    kTimer = new Timer.periodic(
      oneSec,
      (Timer timer) {
        if (kStart == 0) {
          setState(() {
            kTimer.cancel();
          });
        } else {
          setState(() {
            kStart--;
          });
        }
      },
    );
  } */

  void focusOTPField(BuildContext context) {
    FocusScope.of(context).requestFocus(otpFocusNode);
  }

  void resendOTP() {
    code = kOTPCodeGenerate();
    kResetOTPTimer();
    startResendTimer();
  }

  void sendOTP() {}

  @override
  void initState() {
    super.initState();

    errorController = StreamController<ErrorAnimationType>();
    startResendTimer();
    WidgetsBinding.instance.addPostFrameCallback((_) => focusOTPField(context));
  }

  @override
  void didChangeDependencies() {
    /* if (_isInit) {
      FocusScope.of(context).requestFocus(otpFocusNode);
    }
    _isInit = false; */

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    kCancelled = DateTime.now();
    errorController.close();
    kTimer.cancel();
    super.dispose();
  }

  // snackBar Widget
  snackBar(String message) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 400),
          child: AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            // contentPadding: EdgeInsets.all(32.0),
            actionsPadding: EdgeInsets.all(15),
            title: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Lottie.asset(
                    'assets/images/otp-auth-3.json',
                    alignment: Alignment.center,
                    height: 200,
                    width: 200,
                  ),
                ),
                Text(
                  "OTP Verification".toUpperCase(),
                  style: TextStyle(
                      color: Color(0xFF00238A),
                      fontSize: kDialogHeaderFontSize,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1),
                  textAlign: TextAlign.center,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child: Column(children: [
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "Enter the 5-digit authentication code sent to your registered mobile number ending with",
                        style: TextStyle(
                          // color: Color(0xFF00238A),
                          fontSize: kParagraphFontSize,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: Text(
                        "+63 9** *** " +
                            widget.phoneNumber
                                .substring(widget.phoneNumber.length - 4),
                        style: TextStyle(
                          // color: Color(0xFF00238A),
                          fontSize: kParagraphFontSize,
                          letterSpacing: 1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "OTP is " + code.toString(),
                        style: TextStyle(
                          // color: Color(0xFF00238A),
                          color: Colors.orange,
                          fontSize: kParagraphFontSize,
                          letterSpacing: 1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ]),
                ),
              ],
            ),
            content: Form(
              key: formKey,
              child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
                  child: PinCodeTextField(
                    appContext: context,
                    pastedTextStyle: TextStyle(
                      color: kPrimaryColorFaded2,
                      fontWeight: FontWeight.bold,
                    ),
                    length: 5,
                    cursorHeight: 15,
                    focusNode: otpFocusNode,
                    obscureText: true,
                    obscuringCharacter: '*',
                    textStyle: TextStyle(color: kPrimaryColor),
                    obscuringWidget: Icon(
                      Icons.lens_rounded,
                      color: kPrimaryColor,
                      size: 14,
                    ),
                    autoDisposeControllers: true,
                    autoFocus: true,
                    textInputAction: TextInputAction.done,
                    useHapticFeedback: true,
                    blinkWhenObscuring: false,
                    blinkDuration: Duration(milliseconds: 1500),
                    animationType: AnimationType.fade,
                    validator: (v) {
                      /* WidgetsBinding.instance.addPostFrameCallback((_) {
                        if (v.length == 5) {
                          setState(() {
                            pinCodeLenghtIsMax = true;
                          });
                        } else {
                          setState(() {
                            pinCodeLenghtIsMax = false;
                          });
                        }
                      }); */

                      if (v != code && v.length == 5) {
                        return "Incorrect Code";
                      } else {
                        return null;
                      }
                    },
                    pinTheme: PinTheme(
                      shape: PinCodeFieldShape.underline,
                      // borderRadius: BorderRadius.circular(5),
                      /* fieldHeight: 50,
                      fieldWidth: 40, */

                      activeFillColor: hasError ? kPrimaryColor : Colors.white,
                      activeColor: kPrimaryColor,
                      selectedColor: kPrimaryColor,
                      selectedFillColor: kWhiteColor,
                      inactiveFillColor: kWhiteColor,
                      inactiveColor: kPrimaryColorFaded2,
                    ),
                    cursorColor: kPrimaryColor,
                    animationDuration: Duration(milliseconds: 300),
                    enableActiveFill: true,
                    errorAnimationController: errorController,
                    controller: textEditingController,
                    keyboardType: TextInputType.number,
                    /* boxShadows: [
                      BoxShadow(
                        offset: Offset(0, 1),
                        color: Colors.black12,
                        blurRadius: 10,
                      )
                    ], */
                    onCompleted: (v) {
                      print("Completed");

                      if (v == code) {
                        /* Future.delayed(Duration(milliseconds: 1500), () {
                          setState(() {
                            Navigator.of(context).pop();

                            return showDialog(
                              context: context,
                              barrierDismissible: false,
                              barrierColor: Colors.black.withOpacity(.90),
                              builder: (BuildContext context) {
                                return ProgressIndicatorWidget();
                              },
                            );
                          });
                        }); */

                        /*  Navigator.of(context).pop();
                        widget.onSubmit(); */
                        // Navigator.of(context).pop();

                        /* Future.delayed(Duration(milliseconds: 1500), () {
                          
                        }); */

                        Navigator.of(context).pop();
                        widget.onSubmit();
                        kResetAfterSuccess();
                        kTimer.cancel();
                      }
                    },
                    // onTap: () {
                    //   print("Pressed");
                    // },
                    onChanged: (value) {
                      print(value);
                      setState(() {
                        currentText = value;
                      });
                    },
                    beforeTextPaste: (text) {
                      print("Allowing to paste $text");
                      //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                      //but you can show anything you want here, like your pop up saying wrong paste format or etc
                      return true;
                    },
                    errorTextSpace: 20,
                  )),
            ),
            actions: [
              Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: double.maxFinite,
                      margin: EdgeInsets.only(
                          left: 20, right: 20, top: 5, bottom: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          (kStart > 0)
                              ? Row(
                                  children: [
                                    Text(
                                      "Resend code in ",
                                      style: TextStyle(
                                        fontSize: kParagraphFontSize - 2,
                                      ),
                                    ),
                                    Text(
                                      ((kStart / 60).truncate() % 60)
                                              .toString()
                                              .padLeft(2, '0') +
                                          ":" +
                                          (kStart % 60)
                                              .toString()
                                              .padLeft(2, '0'),
                                      style: TextStyle(
                                        color: kPrimaryColor,
                                        fontWeight: FontWeight.bold,
                                        fontSize: kParagraphFontSize - 2,
                                      ),
                                    )
                                  ],
                                )
                              : Row(
                                  children: [
                                    Text(
                                      "Didn't receive the code? ",
                                      style: TextStyle(
                                        fontSize: kParagraphFontSize - 2,
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () => resendOTP(),
                                      child: Text(
                                        "Resend".toUpperCase(),
                                        style: TextStyle(
                                          color: kPrimaryColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: kParagraphFontSize - 2,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                        ],
                      ),
                    ),
                  ]),
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Container(
            child: CircleAvatar(
              backgroundColor: Colors.blueGrey.withOpacity(0.30),
              radius: 20,
              child: Icon(
                Icons.close_rounded,
                color: kWhiteColor,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
