import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/screens/login_screen.dart';

class PromptConfirmationDialog extends StatelessWidget {
  final String title;
  final String subtitle;
  final Function onOK;
  final String okText;
  final Function onCancel;
  final String cancelText;

  const PromptConfirmationDialog({
    @required this.title,
    @required this.subtitle,
    @required this.onOK,
    @required this.okText,
    @required this.onCancel,
    @required this.cancelText,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.all(32.0),
      title: Text(
        title,
        style: TextStyle(
            color: Color(0xFF00238A),
            fontSize: kDialogHeaderFontSize,
            fontWeight: FontWeight.bold,
            letterSpacing: 1),
        textAlign: TextAlign.center,
      ),
      content: Text(
        subtitle,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: kParagraphFontSize),
      ),
      actions: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.maxFinite,
                margin: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                child: ElevatedButton(
                  onPressed: onOK,
                  style: ElevatedButton.styleFrom(
                    primary: kPrimaryColor,
                    padding: EdgeInsets.all(25),
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(kButtonCircularRadius),
                    ),
                  ),
                  child: Text(
                    okText,
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 15.5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                  ),
                ),
              ),
              Container(
                  // width: double.maxFinite,
                  margin:
                      EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                  child: TextButton(
                    child: Text(
                      cancelText,
                      style: TextStyle(
                          color: Color(0xFF00238A),
                          fontSize: 15.5,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: onCancel,
                  ))
            ]),
      ],
    );
  }
}

class PromptAlertDialog extends StatelessWidget {
  final String title;
  final String subtitle;
  final String okText;
  final Function onOk;

  const PromptAlertDialog({
    @required this.title,
    @required this.subtitle,
    @required this.okText,
    this.onOk,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.all(32.0),
      title: Text(
        title,
        style: TextStyle(
            color: Color(0xFF00238A),
            fontSize: kDialogHeaderFontSize,
            fontWeight: FontWeight.bold,
            letterSpacing: 1),
        textAlign: TextAlign.center,
      ),
      content: Text(
        subtitle,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: kParagraphFontSize),
      ),
      actions: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.maxFinite,
                margin:
                    EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                child: ElevatedButton(
                  onPressed: () {
                    onOk;

                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                    primary: kPrimaryColor,
                    padding: EdgeInsets.all(25),
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(kButtonCircularRadius),
                    ),
                  ),
                  child: Text(
                    okText,
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 15.5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                  ),
                ),
              ),
            ]),
      ],
    );
  }
}

class PromptAlertDialogNotify extends StatelessWidget {
  final String title;
  final String subtitle;
  final String okText;
  final Function onOk;

  const PromptAlertDialogNotify({
    @required this.title,
    @required this.subtitle,
    @required this.okText,
    this.onOk,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.all(32.0),
      title: Column(children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
          child: Lottie.asset(
            'assets/images/notify.json',
            alignment: Alignment.center,
            repeat: false,
            height: 70,
            width: 70,
          ),
        ),
        Text(
          title,
          style: TextStyle(
              color: Color(0xFF00238A),
              fontSize: kDialogHeaderFontSize,
              fontWeight: FontWeight.bold,
              letterSpacing: 1),
          textAlign: TextAlign.center,
        ),
      ]),
      content: Text(
        subtitle,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: kParagraphFontSize),
      ),
      actions: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.maxFinite,
                margin:
                    EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                child: ElevatedButton(
                  onPressed: () {
                    onOk;

                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                    primary: kPrimaryColor,
                    padding: EdgeInsets.all(25),
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(kButtonCircularRadius),
                    ),
                  ),
                  child: Text(
                    okText,
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 15.5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                  ),
                ),
              ),
            ]),
      ],
    );
  }
}

class PromptAlertDialogError extends StatelessWidget {
  final String title;
  final String subtitle;
  final String okText;
  final Function onOk;

  const PromptAlertDialogError({
    @required this.title,
    @required this.subtitle,
    @required this.okText,
    this.onOk,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.all(32.0),
      title: Column(children: [
        Container(
          padding: EdgeInsets.all(20),
          child: Lottie.asset(
            'assets/images/error.json',
            alignment: Alignment.center,
            repeat: false,
            height: 120,
            width: 120,
          ),
        ),
        Text(
          title,
          style: TextStyle(
              color: Color(0xFF00238A),
              fontSize: kDialogHeaderFontSize,
              fontWeight: FontWeight.bold,
              letterSpacing: 1),
          textAlign: TextAlign.center,
        ),
      ]),
      content: Text(
        subtitle,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: kParagraphFontSize),
      ),
      actions: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.maxFinite,
                margin:
                    EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                child: ElevatedButton(
                  onPressed: () {
                    onOk;

                    Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                    primary: kPrimaryColor,
                    padding: EdgeInsets.all(25),
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(kButtonCircularRadius),
                    ),
                  ),
                  child: Text(
                    okText,
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 15.5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                  ),
                ),
              ),
            ]),
      ],
    );
  }
}

class PromptAlertDialogRegistrationDone extends StatelessWidget {
  final String title;
  final String subtitle;
  final String okText;
  final Function onOk;

  const PromptAlertDialogRegistrationDone({
    @required this.title,
    @required this.subtitle,
    @required this.okText,
    this.onOk,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.all(32.0),
      title: Column(children: [
        Container(
          padding: EdgeInsets.all(20),
          child: Lottie.asset(
            'assets/images/done-registration.json',
            alignment: Alignment.center,
            repeat: false,
            fit: BoxFit.contain,
            height: 120,
            width: 120,
          ),
        ),
        Text(
          title,
          style: TextStyle(
              color: Color(0xFF00238A),
              fontSize: kDialogHeaderFontSize,
              fontWeight: FontWeight.bold,
              letterSpacing: 1),
          textAlign: TextAlign.center,
        ),
      ]),
      content: Text(
        subtitle,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: kParagraphFontSize),
      ),
      actions: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.maxFinite,
                margin:
                    EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                child: ElevatedButton(
                  onPressed: () {
                    onOk;
                    Navigator.of(context)
                        .pushReplacementNamed(LoginScreen.routeName);
                    // Navigator.of(context).pop();
                  },
                  style: ElevatedButton.styleFrom(
                    primary: kPrimaryColor,
                    padding: EdgeInsets.all(25),
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(kButtonCircularRadius),
                    ),
                  ),
                  child: Text(
                    okText,
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 15.5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                  ),
                ),
              ),
            ]),
      ],
    );
  }
}

class PromptAlertDialogMaintenance extends StatelessWidget {
  final String title;
  final String subtitle;
  final String okText;
  final Function onOk;

  const PromptAlertDialogMaintenance({
    @required this.title,
    @required this.subtitle,
    @required this.okText,
    this.onOk,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.all(32.0),
      title: Column(children: [
        Container(
          padding: EdgeInsets.all(20),
          child: Lottie.asset(
            'assets/images/maintenance.json',
            alignment: Alignment.center,
            // repeat: false,
            fit: BoxFit.contain,
            height: 120,
            width: 120,
          ),
        ),
        Text(
          title,
          style: TextStyle(
              color: Color(0xFF00238A),
              fontSize: kDialogHeaderFontSize,
              fontWeight: FontWeight.bold,
              letterSpacing: 1),
          textAlign: TextAlign.center,
        ),
      ]),
      content: Text(
        subtitle,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: kParagraphFontSize),
      ),
      actions: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.maxFinite,
                margin:
                    EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                child: ElevatedButton(
                  onPressed: onOk
                  /* 
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    Navigator.of(context)
                        .pushReplacementNamed(LoginScreen.routeName); */
                  // Navigator.of(context).pop();
                  ,
                  style: ElevatedButton.styleFrom(
                    primary: kPrimaryColor,
                    padding: EdgeInsets.all(25),
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(kButtonCircularRadius),
                    ),
                  ),
                  child: Text(
                    okText,
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 15.5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                  ),
                ),
              ),
            ]),
      ],
    );
  }
}

class PromptAlertSessionTimeout extends StatelessWidget {
  /* final String title;
  final String subtitle;
  final String okText;
  final Function onOk; */

  const PromptAlertSessionTimeout({
    /* @required this.title,
    @required this.subtitle,
    @required this.okText,
    this.onOk, */
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.all(32.0),
      title: Column(children: [
        Container(
          padding: EdgeInsets.all(20),
          child: Lottie.asset(
            'assets/images/maintenance.json',
            alignment: Alignment.center,
            // repeat: false,
            fit: BoxFit.contain,
            height: 120,
            width: 120,
          ),
        ),
        Text(
          "Session Timeout",
          style: TextStyle(
              color: Color(0xFF00238A),
              fontSize: kDialogHeaderFontSize,
              fontWeight: FontWeight.bold,
              letterSpacing: 1),
          textAlign: TextAlign.center,
        ),
      ]),
      content: Text(
        "Your session has ended or you have been idle for too long. Logging out...",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: kParagraphFontSize),
      ),
      actions: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.maxFinite,
                margin:
                    EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                child: ElevatedButton(
                  onPressed: () {
                    //run log out function here
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    Navigator.of(context)
                        .pushReplacementNamed(LoginScreen.routeName);
                  },
                  style: ElevatedButton.styleFrom(
                    primary: kPrimaryColor,
                    padding: EdgeInsets.all(25),
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(kButtonCircularRadius),
                    ),
                  ),
                  child: Text(
                    "Log Out",
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 15.5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                  ),
                ),
              ),
            ]),
      ],
    );
  }
}

class PromptAlertDialogMissingOBSA extends StatelessWidget {
  final String title;
  final String subtitle;
  final String okText;
  final Function onOk;

  const PromptAlertDialogMissingOBSA({
    @required this.title,
    @required this.subtitle,
    @required this.okText,
    this.onOk,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.all(32.0),
      title: Column(children: [
        Container(
          padding: EdgeInsets.all(20),
          child: Lottie.asset(
            'assets/images/missing-obsa.json',
            alignment: Alignment.center,
            // repeat: false,
            fit: BoxFit.contain,
            height: 120,
            width: 120,
          ),
        ),
        Text(
          title,
          style: TextStyle(
              color: Color(0xFF00238A),
              fontSize: kDialogHeaderFontSize,
              fontWeight: FontWeight.bold,
              letterSpacing: 1),
          textAlign: TextAlign.center,
        ),
      ]),
      content: Text(
        subtitle,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: kParagraphFontSize),
      ),
      actions: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.maxFinite,
                margin:
                    EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                child: ElevatedButton(
                  onPressed: onOk
                  /* 
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    Navigator.of(context)
                        .pushReplacementNamed(LoginScreen.routeName); */
                  // Navigator.of(context).pop();
                  ,
                  style: ElevatedButton.styleFrom(
                    primary: kPrimaryColor,
                    padding: EdgeInsets.all(25),
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(kButtonCircularRadius),
                    ),
                  ),
                  child: Text(
                    okText,
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 15.5,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                  ),
                ),
              ),
            ]),
      ],
    );
  }
}
