import 'package:flutter/material.dart';

class SavingsInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          Container(
            height: 200,
            padding: EdgeInsets.all(15),
            width: 380,
            decoration: BoxDecoration(
              color: Colors.amber[300],
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'TAGUM COOPERATIVE',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1,
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Container(),
                    ),
                    Text(
                      'Passbook No: 123456',
                      style: TextStyle(fontSize: 12),
                    )
                  ],
                ),
                Expanded(
                  child: Container(),
                ),
                SizedBox(
                  width: double.infinity,
                  child: FittedBox(
                    child: Text(
                      '001-1001-123456789',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                        letterSpacing: 8,
                        shadows: [
                          Shadow(
                              offset: Offset(3, 3),
                              color: Colors.black38,
                              blurRadius: 10),
                          Shadow(
                              offset: Offset(-3, -3),
                              color: Colors.white.withOpacity(0.85),
                              blurRadius: 10)
                        ],
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'SHARE CAPITAL',
                      style: TextStyle(fontSize: 12),
                    ),
                    Text(
                      'Opening Date: 1/26/1956',
                      style: TextStyle(fontSize: 12),
                    )
                  ],
                ),
                Expanded(
                  flex: 1,
                  child: Container(),
                ),
              ],
            ),
          ),
          Container(
            width: 380,
            padding: EdgeInsets.all(15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.check_circle_outline,
                      color: Colors.green,
                      size: 15,
                    ),
                    Text(
                      'Active',
                      style: TextStyle(color: Colors.green, fontSize: 15),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Bal: 3,000.00',
                      style: TextStyle(fontSize: 15),
                    ),
                    Text('2.5% per annum'),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
