/* Notes
- Remove Unused Packages
- Categorized Imports 
- Put Commas' for Proper Indexing*/

// DART

// PACKAGES
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:tcmobapp/provider/savings_details/savings_details_class.dart';
import 'package:skeleton_text/skeleton_text.dart';
import 'package:tcmobapp/main.dart';
import "package:collection/collection.dart";
import 'package:tcmobapp/provider/savings_details/savings_details_transaction_class.dart';
import 'dart:developer' as developer;

// PRESENTATIONS
import 'package:tcmobapp/presentations/tc_mobapp_icons_icons.dart';

// CONSTANT
import '../../constants.dart';

// MODELS
// import 'package:tcmobapp/models/savings_details/savings_details_data.dart';
import 'package:tcmobapp/provider/savings_details/grouped_transactions_class.dart';

// PROVIDERS
import 'package:tcmobapp/provider/savings_details/savings_details_provider.dart';

// SCREENS
import 'package:tcmobapp/screens/savings_details.dart';
import 'package:tcmobapp/screens/savings_details_view_all_trans.dart';

// WIDGETS

class SavingsDetailsRecentTransactions extends StatefulWidget {
  @override
  _SavingsDetailsRecentTransactionsState createState() =>
      _SavingsDetailsRecentTransactionsState();
}

class _SavingsDetailsRecentTransactionsState
    extends State<SavingsDetailsRecentTransactions> {
  /* var _isLoading = true;

  var _isInit = true;

  bool isLoadingVertical = false;

  bool isLoadingHorizontal = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      _loadProducts();
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  Future<void> _loadProducts() async {
    try {
      await new Future.delayed(const Duration(seconds: 5));
      await Provider.of<SavingsDetailsClassProvider>(context, listen: false)
          .fetchData();
    } catch (error) {
      await showDialog(
          context: context,
          builder: (ctx) => AlertDialog(
                title: Text('An error occured!'),
                content: Text('Something went wrong.'),
                actions: <Widget>[
                  FlatButton(
                      onPressed: () {
                        Navigator.of(ctx).pop();
                      },
                      child: Text('Okay'))
                ],
              ));
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  } */

  @override
  Widget build(BuildContext context) {
    final savingsDetailsProvider =
        Provider.of<SavingsDetailsClassProvider>(context, listen: true);

    final _items = savingsDetailsProvider.transactions;

    var newMap = groupBy(_items, (obj) => obj.dateGroup);

    print("newMaps: ${_items}");

    List<GroupedTransactions> newList = [];

    newMap.forEach((key, value) {
      newList.add(GroupedTransactions(
        date: DateTime.parse(key.toString()),
        transactions: value,
      ));
    });

    developer.log(newList.toString());

    // print("newMap: ${newMap}");

    /* for (var i in newMap) {
      i
    }

    developer.log(newMap) */

    /* newMap.forEach((k, v) => print('${k}: ${v}')); */

    return Container(
      width: double.infinity,
      // height: double.infinity,
      // margin: EdgeInsets.only(top:),
      decoration: BoxDecoration(
          // color: kOffWhiteColor,
          // borderRadius: BorderRadius.only(
          //     topLeft: Radius.circular(10),
          //     topRight: Radius.circular(10)),
          ),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            margin: EdgeInsets.all(20),
            // padding: EdgeInsets.all(20),
            child: Column(
              children: [
                Container(
                  child: Row(
                    children: [
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: savingsDetailsProvider.isLoading
                              ? Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 5),
                                  child: skeletonInstance(),
                                )
                              : Text(
                                  'Recent Transactions',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                      color: kDarkerFontColor3),
                                ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(right: 5),
                          child: GestureDetector(
                            onTap: () {
                              /* Navigator.of(context).pushNamed(
                                  SavingsDetailsViewAllTransScreen.routeName); */
                              if (!savingsDetailsProvider.isLoading &&
                                  newList.length > 0) {
                                Navigator.of(context).push(
                                  PageTransition(
                                      type: PageTransitionType.fade,
                                      duration: Duration(milliseconds: 150),
                                      reverseDuration:
                                          Duration(milliseconds: 150),
                                      child: SavingsDetailsViewAllTransScreen(
                                          accountNumberPassed:
                                              savingsDetailsProvider
                                                  .getFocusedAccountNumber),
                                      inheritTheme: true,
                                      ctx: context),
                                );
                              }
                            },
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: savingsDetailsProvider.isLoading
                                  ? Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 5),
                                      child: skeletonInstance(),
                                    )
                                  : Text(
                                      'View All',
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: (newList.length > 0)
                                              ? kPrimarySwatchColor
                                              : Colors.grey,
                                          fontWeight: FontWeight.bold),
                                    ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                (savingsDetailsProvider.getLoadingStatus ||
                        savingsDetailsProvider.getLoadingStatusTransactions)
                    ? Container(
                        margin: EdgeInsets.only(top: 30),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              // alignment: Alignment.centerLeft,
                              width: MediaQuery.of(context).size.width * 0.20,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: skeletonInstance(),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              /* padding: EdgeInsets.symmetric(horizontal: 20), */
                              decoration: BoxDecoration(
                                color: kWhiteColor,
                                borderRadius: BorderRadius.circular(20),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.1),
                                    spreadRadius: 1,
                                    blurRadius: 1,
                                    offset: Offset(
                                        0, 1), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  // skeletonInstance(paddingLeft: 20),
                                  kBuildRecentTransactionsDashboardLazyLoader(),
                                  kBuildRecentTransactionsDashboardLazyLoader(),
                                  kBuildRecentTransactionsDashboardLazyLoader(),
                                  kBuildRecentTransactionsDashboardLazyLoader(),
                                  kBuildRecentTransactionsDashboardLazyLoader(),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              // alignment: Alignment.centerLeft,
                              width: MediaQuery.of(context).size.width * 0.20,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: skeletonInstance(),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 20),
                              /* padding: EdgeInsets.symmetric(horizontal: 20), */
                              decoration: BoxDecoration(
                                color: kWhiteColor,
                                borderRadius: BorderRadius.circular(20),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.1),
                                    spreadRadius: 1,
                                    blurRadius: 1,
                                    offset: Offset(
                                        0, 1), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  // skeletonInstance(paddingLeft: 20),
                                  kBuildRecentTransactionsDashboardLazyLoader(),
                                  kBuildRecentTransactionsDashboardLazyLoader(),
                                  kBuildRecentTransactionsDashboardLazyLoader(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    : /* Container() */ (newList.length > 0)
                        ? buildGroupedTransactions(newList)
                        : Center(
                            child: Container(
                              margin: EdgeInsets.only(top: 50),
                              child: Column(children: [
                                SvgPicture.asset(
                                  "assets/images/no-data-2.svg",
                                  width: 80,
                                  height: 100,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 15),
                                  child: Text(
                                    'No Transaction Found',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.grey.withOpacity(0.80),
                                    ),
                                  ),
                                ),
                              ]),
                            ),
                          )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container buildGroupedTransactions(
      List<GroupedTransactions> groupedTransactions) {
    return Container(
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: [
            for (var i in groupedTransactions)
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      (DateFormat.yMMMMd().format(i.date) ==
                              DateFormat.yMMMMd().format(DateTime.now()))
                          ? "Today"
                          : DateFormat.yMMMd().format(i.date).toString(),
                      style: TextStyle(
                          fontSize: 14,
                          color: kPrimaryColorFaded2,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    /* padding: EdgeInsets.symmetric(
                                                          horizontal: 20, vertical: 10), */
                    decoration: BoxDecoration(
                      color: kWhiteColor,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 1,
                          blurRadius: 1,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ],
                    ),
                    child:
                        /* Container(), */ Column(
                      children: i.transactions.map<Widget>(
                        (e) {
                          return Column(children: [
                            recentTransactions(
                              e.balanceLocator +
                                  " of ₱ " +
                                  NumberFormat.decimalPattern()
                                      .format(e.amount),
                              "Ref: " + e.referenceNo,
                              e.runningBalance,
                              (e.balanceLocator == "Deposit") ? "+" : "-",
                              DateFormat.yMMMd()
                                  .format(e.transactionTime)
                                  .toString(),
                            ),
                            Divider(
                              color: kOffWhiteColor,
                              height: 1,
                              thickness: 1,
                            ),
                          ]);
                        },
                      ).toList(),
                    ),
                  ),
                ],
              ),
          ],
        ));
  }
}

Row buildRecentTransactionsLazyLoader() {
  return Row(
    children: [
      Container(
        margin: EdgeInsets.all(20),
        child: SizedBox(
          width: 50,
          height: 50,
          child: recentTransactionsLazyLoader(),
        ),
      ),
      Expanded(
        child: Container(
          padding: EdgeInsets.only(top: 5),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 5, right: 20),
                child: recentTransactionsLazyLoader(),
              ),
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 10, right: 20),
                child: recentTransactionsLazyLoader(),
              ),
            ],
          ),
        ),
      )
    ],
  );
}

Container recentTransactions(String transDetail, String transtType,
    double transAmount, String transSign, String transDate) {
  IconData iconAssetPath;
  String amount = transSign +
      ' ₱ ' +
      NumberFormat.decimalPattern().format(transAmount).toString();

  print(amount);

  /* if (transtType == 'Fund Transfer') {
    iconAssetPath = 'assets/images/trans-fund-transfer.png';
  } else if (transtType == 'Bills Payment') {
    iconAssetPath = 'assets/images/trans-bills-payment.png';
  } else if (transtType == 'Cash In') {
    iconAssetPath = 'assets/images/trans-cash-in.png';
  } else if (transtType == 'Deposit') {
    iconAssetPath = 'assets/images/trans-fund-transfer.png';
  } else if (transtType == 'Withdrawal') {
    iconAssetPath = 'assets/images/trans-cash-in.png';
  } */

  if (transSign == '-') {
    iconAssetPath = TcMobappIcons.withdrawal;
  } else if (transSign == '+') {
    iconAssetPath = TcMobappIcons.deposit;
  }

  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.only(right: 15),
          decoration: BoxDecoration(
            color: kOffWhiteColor,
            borderRadius: BorderRadius.circular(15),
          ),
          child: ClipRRect(
            child: Icon(
              iconAssetPath,
              size: 20,
              color: Colors.grey,
            ),
          ),
        ),
        Expanded(
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            dense: true,
            title: Text(transDetail, style: TextStyle(color: kPrimaryColor)),
            subtitle: Text(
              transtType,
              style: TextStyle(
                fontSize: 11.5,
                color: kTransactionSubtitleColor,
              ),
            ),
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    margin: EdgeInsets.only(bottom: 2),
                    child: Text(amount,
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: (transSign == '+')
                                ? Colors.green
                                : Colors.red))),
                Container(
                  margin: EdgeInsets.only(top: 2),
                  child: Text(
                    transDate,
                    style: TextStyle(
                      fontSize: 11.5,
                      color: kTransactionSubtitleColor,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

Container recentTransactionsLazyLoader() {
  return Container(child: skeletonInstance());
}
