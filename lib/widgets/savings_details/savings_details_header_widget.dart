/* Notes
- Remove Unused Packages
- Categorized Imports 
- Put Commas' for Proper Indexing*/

// DART

// PACKAGES
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tcmobapp/provider/savings_details/savings_details_class.dart';

// CONSTANT
import '../../constants.dart';

// MODELS

// PROVIDERS
import 'package:tcmobapp/provider/savings_details/savings_details_provider.dart';

// SCREENS

// WIDGETS

class SavingsDetailsHeaderWidget extends StatefulWidget {
  @override
  _SavingsDetailsHeaderWidgetState createState() =>
      _SavingsDetailsHeaderWidgetState();
}

class _SavingsDetailsHeaderWidgetState
    extends State<SavingsDetailsHeaderWidget> {
  CarouselController carouselController = CarouselController();
  bool _filterAll = true;
  bool _filterShareCap = false;
  bool _filterSavings = false;
  bool _filterTimeDeposit = false;
  int _intFilterSelected = 1;
  bool _isInit = false;
  int _current_savings = 0;
  bool _isSnackbarActive = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit == false) {
      _changeFilterCards(1);
      _isInit = true;
    }

    super.didChangeDependencies();
  }

  setFiltersFalse(int selectedStatus) {
    _filterAll = false;
    _filterShareCap = false;
    _filterSavings = false;
    _filterTimeDeposit = false;

    switch (selectedStatus) {
      case 1:
        {
          if (_intFilterSelected != selectedStatus) {
            _changeFilterCards(1);
          }

          _filterAll = true;
          _intFilterSelected = selectedStatus;
        }
        break;

      case 2:
        {
          if (_intFilterSelected != selectedStatus) {
            _changeFilterCards(2);
          }

          _filterShareCap = true;
          _intFilterSelected = selectedStatus;
        }
        break;

      case 3:
        {
          if (_intFilterSelected != selectedStatus) {
            _changeFilterCards(3);
          }

          _filterSavings = true;
          _intFilterSelected = selectedStatus;
        }
        break;

      case 4:
        {
          if (_intFilterSelected != selectedStatus) {
            _changeFilterCards(4);
          }

          _filterTimeDeposit = true;
          _intFilterSelected = selectedStatus;
        }
        break;

      default:
        {
          if (_intFilterSelected != selectedStatus) {
            _changeFilterCards(1);
          }

          _filterAll = true;
          _intFilterSelected = 1;
        }
        break;
    }
  }

  Future<void> loadSelectedAccountsTransactions(String accountNumber) async {
    await Provider.of<SavingsDetailsClassProvider>(context, listen: false)
        .setIsLoadingTransactions();

    try {
      // await new Future.delayed(const Duration(seconds: 5));
      await Provider.of<SavingsDetailsClassProvider>(context, listen: false)
          .fetchDataTransactions(accountNumber);
    } catch (error) {} finally {
      await Provider.of<SavingsDetailsClassProvider>(context, listen: false)
          .setIsLoadingTransactions();

      print(
          await Provider.of<SavingsDetailsClassProvider>(context, listen: false)
              .getLoadingStatusTransactions);
    }
  }

  Future<void> _changeFilterCards(int _filterNum) async {
    final savingsDetailsProvider =
        Provider.of<SavingsDetailsClassProvider>(context, listen: false);

    try {
      // await new Future.delayed(const Duration(seconds: 5));
      await savingsDetailsProvider.setFilterOption(_filterNum);
      List<SavingsDetailsClass> savings = await savingsDetailsProvider.items;
      await savingsDetailsProvider.onFilterChange();
    } catch (error) {
      print("ERROR: $error");
    } finally {
      try {
        if (!_isInit) {
          carouselController.animateToPage(0);
        }
      } catch (err) {
        print("finally error: $err");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final savingsDetailsProvider =
        Provider.of<SavingsDetailsClassProvider>(context, listen: true);

    List<SavingsDetailsClass> savings = savingsDetailsProvider.items;

    return Container(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            // height: double.infinity,
            // margin: EdgeInsets.only(top:),
            decoration: BoxDecoration(
                // borderRadius: BorderRadius.only(
                //     topLeft: Radius.circular(10),
                //     topRight: Radius.circular(10)),
                ),
            child: Container(
              decoration: BoxDecoration(
                /* border: Border.all(
                                      color: Colors.grey.withOpacity(0.50),
                                    ), */
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              margin: EdgeInsets.all(20),
              // padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  savingsDetailsProvider.isLoading
                      ? skeletonInstance()
                      : Container(
                          child: Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Container(
                                  // decoration: BoxDecoration(color: Colors.red),
                                  // padding: EdgeInsets.only(left: 10),
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.arrow_back_ios_rounded,
                                      size: 20,
                                    ),
                                    color: kPrimaryColor,
                                    onPressed: () {
                                      Navigator.of(context)
                                          .pop(); //-->context here is this widget's position in the widget tree.
                                    },
                                  ),
                                  alignment: Alignment.centerLeft,
                                ),
                              ),
                              Expanded(
                                flex: 4,
                                child: Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Savings Accounts',
                                    style: TextStyle(
                                        fontSize: 24,
                                        fontWeight: FontWeight.bold,
                                        color: kDarkerFontColor3),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Container(),
                              ),

                              /* Expanded(
                              child: Container(
                                margin: EdgeInsets.only(right: 5),
                                child: Align(
                                  alignment: Alignment
                                      .centerRight, /* Text(
                                    'View All',
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: kPrimarySwatchColor,
                                        fontWeight: FontWeight.bold),
                                  ), */
                                ),
                              ),
                            ), */
                            ],
                          ),
                        ),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: savingsDetailsProvider.isLoading
                        ? Row(
                            children: [
                              recentTransactionsFilterLazyLoader(),
                              recentTransactionsFilterLazyLoader(),
                              recentTransactionsFilterLazyLoader(),
                            ],
                          )
                        : Row(
                            children: [
                              recentTransactionsFilter('All', _filterAll, 1),
                              recentTransactionsFilter(
                                  'Share Capital', _filterShareCap, 2),
                              recentTransactionsFilter(
                                  'Savings', _filterSavings, 3),
                              recentTransactionsFilter(
                                  'Time Deposits', _filterTimeDeposit, 4),
                            ],
                          ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            child: savingsDetailsProvider.getLoadingStatus
                ? Container(
                    margin: EdgeInsets.symmetric(
                      vertical: 15,
                      horizontal: 50,
                    ),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          spreadRadius: 2,
                          blurRadius: 4,
                          offset: Offset(0, 2), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 30),
                      child: Column(children: [
                        skeletonInstance(paddingLeft: 20),
                        SizedBox(height: 12),
                        skeletonInstance(paddingLeft: 150),
                        SizedBox(height: 12),
                        skeletonInstance(paddingLeft: 25),
                        SizedBox(height: 12),
                        skeletonInstance(paddingLeft: 100),
                        SizedBox(height: 12),
                        skeletonInstance(paddingLeft: 30),
                      ]),
                    ),
                  )
                : Column(
                    children: [
                      (savings.length < 1)
                          ? Container(
                              margin: EdgeInsets.only(bottom: 28),
                              child: AspectRatio(
                                aspectRatio: 2.0,
                                child: Container(
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 20, horizontal: 40),
                                  decoration: BoxDecoration(
                                    color: kPrimaryColor.withOpacity(0.08),
                                    borderRadius: BorderRadius.circular(30.0),
                                    border: Border(
                                      top: BorderSide(
                                        width: 1.0,
                                        color: kPrimaryColor.withOpacity(0.08),
                                      ),
                                      left: BorderSide(
                                        width: 1.0,
                                        color: kPrimaryColor.withOpacity(0.08),
                                      ),
                                      right: BorderSide(
                                        width: 1.0,
                                        color: kPrimaryColor.withOpacity(0.08),
                                      ),
                                      bottom: BorderSide(
                                        width: 1.0,
                                        color: kPrimaryColor.withOpacity(0.08),
                                      ),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: kPrimaryColor.withOpacity(0.05),
                                        spreadRadius: 3,
                                        blurRadius: 10,
                                        offset: Offset(
                                            0, 2), // changes position of shadow
                                      ),
                                    ],
                                  ),
                                  child: Text(
                                    'No Savings Found',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: kPrimaryColor.withOpacity(0.50),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : CarouselSlider(
                              carouselController: carouselController,
                              options: CarouselOptions(
                                  aspectRatio: 2.0,
                                  enlargeCenterPage: true,
                                  enableInfiniteScroll: false,
                                  onPageChanged: (index, reason) {
                                    setState(() {
                                      // print("Index: " + index.toString());
                                      _current_savings = index;
                                      // print("Current: " + _current_savings.toString());
                                    });

                                    print("Account Number: " +
                                        savings[index].accountNumber);

                                    savingsDetailsProvider
                                        .setFocusedAccountNumber(
                                            savings[index].accountNumber);

                                    loadSelectedAccountsTransactions(
                                        savings[index].accountNumber);
                                  }),
                              items: savings.map((item) {
                                return ChangeNotifierProvider.value(
                                  // create: (c) => savings[savings.indexOf(item)],
                                  value: savings[savings.indexOf(item)],
                                  child: FlipCard(
                                    direction: FlipDirection.HORIZONTAL,
                                    speed: 1000,
                                    onFlipDone: (status) {
                                      // print(status);
                                    },
                                    front: Container(
                                      margin:
                                          EdgeInsets.symmetric(vertical: 15),
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                        boxShadow: [
                                          BoxShadow(
                                            color:
                                                kPrimaryColor.withOpacity(0.2),
                                            spreadRadius: 3,
                                            blurRadius: 10,
                                            offset: Offset(0,
                                                2), // changes position of shadow
                                          ),
                                        ],
                                      ),
                                      child: GestureDetector(
                                        /* onTap: () => Navigator.of(context).pushNamed(
                                SavingsDetailsMoreDetailsScreen.routeName,
                                arguments: item.accountNumber), */
                                        child: Stack(
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(30.0),
                                              child: Image.asset(
                                                "assets/images/background-1.jpg",
                                                fit: BoxFit.cover,
                                                color: Colors.black.withOpacity(
                                                    item.cardOpacity),
                                                colorBlendMode:
                                                    BlendMode.darken,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.80,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                vertical: 10,
                                                horizontal: 20,
                                              ),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Expanded(
                                                    flex: 1,
                                                    child: Row(children: [
                                                      Expanded(
                                                        child: FittedBox(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          fit: BoxFit.scaleDown,
                                                          child: Container(
                                                            padding:
                                                                EdgeInsets.all(
                                                                    1),
                                                            decoration:
                                                                BoxDecoration(
                                                              color:
                                                                  kWhiteColor,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          50.0),
                                                            ),
                                                            child: Container(
                                                              child: Icon(
                                                                item.isActive
                                                                    ? Icons
                                                                        .check_circle
                                                                    : Icons
                                                                        .remove_circle,
                                                                color: item
                                                                        .isActive
                                                                    ? Colors.greenAccent[
                                                                        700]
                                                                    : Colors
                                                                        .red,
                                                                size: 20,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      Expanded(
                                                        child: FittedBox(
                                                          alignment: Alignment
                                                              .centerRight,
                                                          fit: BoxFit.scaleDown,
                                                          child: IconButton(
                                                            icon: Icon(
                                                              Icons
                                                                  .content_copy_rounded,
                                                              color:
                                                                  kWhiteColor,
                                                            ),
                                                            tooltip:
                                                                "Copy Account Number",
                                                            onPressed: () {
                                                              Clipboard.setData(
                                                                  new ClipboardData(
                                                                      text: item
                                                                          .accountNumber));

                                                              if (!_isSnackbarActive) {
                                                                _isSnackbarActive =
                                                                    true;

                                                                final copySnack =
                                                                    SnackBar(
                                                                  behavior:
                                                                      SnackBarBehavior
                                                                          .floating,
                                                                  margin:
                                                                      EdgeInsets
                                                                          .all(
                                                                              5),
                                                                  shape:
                                                                      RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            10),
                                                                  ),
                                                                  /* backgroundColor:
                                                                      Colors
                                                                          .grey, */
                                                                  content: Row(
                                                                    children: [
                                                                      Container(
                                                                        margin: EdgeInsets.only(
                                                                            right:
                                                                                10),
                                                                        child:
                                                                            Icon(
                                                                          Icons
                                                                              .check_circle,
                                                                          size:
                                                                              20,
                                                                          color:
                                                                              Colors.white,
                                                                        ),
                                                                      ),
                                                                      Text(
                                                                          "Account Number Copied to Clipboard!")
                                                                    ],
                                                                  ),
                                                                  action:
                                                                      SnackBarAction(
                                                                    label:
                                                                        'Okay',
                                                                    textColor:
                                                                        Colors
                                                                            .grey,
                                                                    onPressed:
                                                                        () {
                                                                      // Some code to undo the change.
                                                                    },
                                                                  ),
                                                                );

                                                                ScaffoldMessenger.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                        copySnack)
                                                                    .closed
                                                                    .then((value) =>
                                                                        _isSnackbarActive =
                                                                            false);

                                                                print(
                                                                    "Copieeeed");
                                                              }
                                                            },
                                                          ),
                                                        ),
                                                      ),
                                                      /* Expanded(
                                            child: Container(
                                              alignment: Alignment.centerRight,
                                              // padding: EdgeInsets.all(10),
                                              child: Image.asset(
                                                'assets/images/tc-logo-mockup.png',
                                                fit: BoxFit.scaleDown,
                                                height: 60,
                                                width: 60,
                                              ),
                                            ),
                                          ), */
                                                    ]),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Container(
                                                      padding: EdgeInsets.only(
                                                          left: 10),
                                                      child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            FittedBox(
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              fit: BoxFit
                                                                  .fitWidth,
                                                              child: Text(
                                                                item.accountNumber,
                                                                style:
                                                                    TextStyle(
                                                                  fontSize: 16,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .white,
                                                                  letterSpacing:
                                                                      4,
                                                                ),
                                                              ),
                                                            ),
                                                            FittedBox(
                                                              alignment: Alignment
                                                                  .centerLeft,
                                                              fit: BoxFit
                                                                  .scaleDown,
                                                              child: Text(
                                                                item.accountTitle
                                                                    .toUpperCase(),
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  letterSpacing:
                                                                      2,
                                                                ),
                                                              ),
                                                            ),
                                                          ]),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Row(
                                                      children: [
                                                        /* Expanded(
                                              child: FittedBox(
                                                fit: BoxFit.scaleDown,
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      'Opened Date'
                                                          .toUpperCase(),
                                                      style: TextStyle(
                                                        color:
                                                            kPrimaryColorFaded,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        letterSpacing: 2,
                                                      ),
                                                    ),
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          top: 5),
                                                      child: Text(
                                                        DateFormat.yMMMd()
                                                            .format(
                                                                item.openedDate)
                                                            .toString(),
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          letterSpacing: 2,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ), */
                                                        Expanded(
                                                          child: Container(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    right: 15),
                                                            child: FittedBox(
                                                              alignment: Alignment
                                                                  .centerRight,
                                                              fit: BoxFit
                                                                  .scaleDown,
                                                              child: Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .center,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  Text(
                                                                    'Balance'
                                                                        .toUpperCase(),
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          kPrimaryColorFaded,
                                                                      fontSize:
                                                                          12,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      letterSpacing:
                                                                          2,
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    margin: EdgeInsets
                                                                        .only(
                                                                            top:
                                                                                5),
                                                                    child: Text(
                                                                      '₱ ' +
                                                                          NumberFormat.decimalPattern()
                                                                              .format(item.availableBalance),
                                                                      style:
                                                                          TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                        letterSpacing:
                                                                            2,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                      alignment:
                                                          Alignment.center,
                                                      child: FittedBox(
                                                        alignment:
                                                            Alignment.center,
                                                        fit: BoxFit.scaleDown,
                                                        child: Row(children: [
                                                          Text(
                                                            "tap for more details "
                                                                .toUpperCase(),
                                                            style: TextStyle(
                                                              color:
                                                                  kPrimaryColorFaded2,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 8,
                                                              letterSpacing: 1,
                                                            ),
                                                          ),
                                                          Icon(
                                                            Icons.flip_to_back,
                                                            color:
                                                                kPrimaryColorFaded2,
                                                            size: 10,
                                                          ),
                                                        ]),
                                                      ),
                                                    ),
                                                  ),
                                                  /* ListTile(
                                        trailing: Container(
                                          // padding: EdgeInsets.all(10),
                                          child: Image.asset(
                                            'assets/images/tc-logo-mockup.png',
                                            height: 100,
                                            width: 100,
                                          ),
                                        ),
                                        /* title: Text(
                                          item.accountTitle.toUpperCase(),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                            color: kWhiteColor,
                                          ),
                                        ), */
                                        leading: Container(
                                          // padding: EdgeInsets.all(1),
                                          decoration: BoxDecoration(
                                            color: kOffWhiteColor,
                                            borderRadius: BorderRadius.circular(50.0),
                                          ),
                                          child: Icon(
                                            Icons.check_circle,
                                            color: item.isActive
                                                ? Colors.green
                                                : Colors.red,
                                            size: 24,
                                          ),
                                        ),
                                      ), */
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    back: Container(
                                      margin:
                                          EdgeInsets.symmetric(vertical: 15),
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                        boxShadow: [
                                          BoxShadow(
                                            color:
                                                Colors.black.withOpacity(0.2),
                                            spreadRadius: 3,
                                            blurRadius: 7,
                                            offset: Offset(0,
                                                2), // changes position of shadow
                                          ),
                                        ],
                                      ),
                                      child: GestureDetector(
                                        /* onTap: () => Navigator.of(context).pushNamed(
                                SavingsDetailsMoreDetailsScreen.routeName,
                                arguments: item.accountNumber), */
                                        child: Stack(
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(30.0),
                                              child: Image.asset(
                                                "assets/images/background-1.jpg",
                                                fit: BoxFit.cover,
                                                color: Colors.black.withOpacity(
                                                    item.cardOpacity),
                                                colorBlendMode:
                                                    BlendMode.darken,
                                                width: 1000,
                                              ),
                                            ),
                                            /* 
                                Container(
                                  padding: EdgeInsets.all(10),
                                  alignment: Alignment.centerRight,
                                  child: Image.asset(
                                    'assets/images/vector-1.png',
                                    fit: BoxFit.cover,
                                    width: 130,
                                  ),
                                ), */
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                vertical: 10,
                                                horizontal: 20,
                                              ),
                                              child: Column(
                                                children: [
                                                  Expanded(
                                                    flex: 1,
                                                    child: Center(
                                                      child: ListTile(
                                                        title: FittedBox(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          fit: BoxFit.scaleDown,
                                                          child: Text(
                                                            item.accountTitle
                                                                .toUpperCase(),
                                                            style: TextStyle(
                                                              fontSize: 18,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color:
                                                                  Colors.white,
                                                              letterSpacing: 4,
                                                            ),
                                                          ),
                                                        ),
                                                        subtitle: FittedBox(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          fit: BoxFit.scaleDown,
                                                          child: Text(
                                                            item.accountNumber
                                                                .toUpperCase(),
                                                            style: TextStyle(
                                                              fontSize: 10,
                                                              color:
                                                                  Colors.white,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              letterSpacing: 2,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceEvenly,
                                                      children: [
                                                        Row(
                                                          children: [
                                                            Text(
                                                              'Opened Date: '
                                                                  .toUpperCase(),
                                                              style: TextStyle(
                                                                color:
                                                                    kPrimaryColorFaded,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                letterSpacing:
                                                                    2,
                                                              ),
                                                            ),
                                                            Container(
                                                              // margin: EdgeInsets.only(top: 5),
                                                              child: Text(
                                                                DateFormat.yMd()
                                                                    .format(item
                                                                        .openedDate)
                                                                    .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 12,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  letterSpacing:
                                                                      2,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Row(
                                                          children: [
                                                            Text(
                                                              'Passbook No.: '
                                                                  .toUpperCase(),
                                                              style: TextStyle(
                                                                color:
                                                                    kPrimaryColorFaded,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                letterSpacing:
                                                                    2,
                                                              ),
                                                            ),
                                                            Container(
                                                              // margin: EdgeInsets.only(top: 5),
                                                              child: Text(
                                                                item.passbookNo,
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 12,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  letterSpacing:
                                                                      2,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Row(
                                                          children: [
                                                            Text(
                                                              'Interest: '
                                                                  .toUpperCase(),
                                                              style: TextStyle(
                                                                color:
                                                                    kPrimaryColorFaded,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                letterSpacing:
                                                                    2,
                                                              ),
                                                            ),
                                                            Container(
                                                              // margin: EdgeInsets.only(top: 5),
                                                              child: Text(
                                                                item.interestRate
                                                                        .toString() +
                                                                    '%',
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 12,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  letterSpacing:
                                                                      2,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),

                                                  /* ListTile(
                                        trailing: Container(
                                          // padding: EdgeInsets.all(10),
                                          child: Image.asset(
                                            'assets/images/tc-logo-mockup.png',
                                            height: 100,
                                            width: 100,
                                          ),
                                        ),
                                        /* title: Text(
                                          item.accountTitle.toUpperCase(),
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20,
                                            color: kWhiteColor,
                                          ),
                                        ), */
                                        leading: Container(
                                          // padding: EdgeInsets.all(1),
                                          decoration: BoxDecoration(
                                            color: kOffWhiteColor,
                                            borderRadius: BorderRadius.circular(50.0),
                                          ),
                                          child: Icon(
                                            Icons.check_circle,
                                            color: item.isActive
                                                ? Colors.green
                                                : Colors.red,
                                            size: 24,
                                          ),
                                        ),
                                      ), */
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              }).toList(),
                            ),
                      Visibility(
                        visible: savings.length < 8 ? true : false,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: savings.map((url) {
                            int index = savings.indexOf(url);
                            // print(savings);
                            // print('length ${savings.length}');
                            return Container(
                              width: 8.0,
                              height: 8.0,
                              margin: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 2.0),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: _current_savings == index
                                    ? Color.fromRGBO(0, 35, 138, 0.9)
                                    : Color.fromRGBO(0, 35, 138, 0.4),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                    ],
                  ),
          ),
        ],
      ),
    );
  }

  Container recentTransactionsFilter(
      String rtfText, bool selectedFilterBool, int selectedFilterBoolInt) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.symmetric(vertical: 6),
      margin: EdgeInsets.symmetric(horizontal: 7),
      child: ElevatedButton(
        onPressed: () {
          setState(() {
            setFiltersFalse(selectedFilterBoolInt);
          });
        },
        style: ElevatedButton.styleFrom(
          primary: (selectedFilterBool) ? kPrimaryColor : kWhiteColor,
          enableFeedback: true,
          elevation: 1,
          // padding: EdgeInsets.all(14),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(kButtonCircularRadius),
          ),
        ),
        child: Text(
          rtfText,
          style: TextStyle(
              color: (selectedFilterBool) ? kWhiteColor : kPrimaryColor,
              fontSize: 9,
              fontWeight: FontWeight.w600,
              letterSpacing: 1),
        ),
      ),
    );
  }

  Container recentTransactionsFilterLazyLoader() {
    return Container(
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
        color: kWhiteColor,
        borderRadius: BorderRadius.circular(30),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 1,
            blurRadius: 1,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: SizedBox(width: 100, child: skeletonInstance()),
    );
  }
}
