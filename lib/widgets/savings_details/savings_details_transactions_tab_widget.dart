/* Notes
- Remove Unused Packages
- Categorized Imports */

// DART

// PACKAGES
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// CONSTANT

// MODELS
import '../../models/share_transactions_data.dart';

// PROVIDERS

// SCREENS

// WIDGETS

class SavingsDetailsTransactionTabWidget extends StatelessWidget {
  final ShareTransactionsData shareTrans;

  const SavingsDetailsTransactionTabWidget({Key key, this.shareTrans})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      padding: EdgeInsets.all(10),
      child: ListView.builder(
        itemCount: shareTrans.tranList.length,
        itemBuilder: (ctx, index) {
          return Card(
            elevation: 7,
            child: Container(
              height: 100,
              padding: EdgeInsets.only(
                top: 15,
                left: 15,
                right: 15,
              ),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: shareTrans.tranList[index].labelType == 'BATCM'
                        ? [
                            const Color(0xFF5dfe60),
                            const Color(0xFF28d1ab),
                          ]
                        : [
                            const Color(0xFFB00707),
                            const Color(0xFFFA3C3C),
                          ],
                    stops: [0, 1],
                    tileMode: TileMode.clamp),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${shareTrans.tranList[index].labelType} OF',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  Row(
                    children: [
                      Text(
                        shareTrans.tranList[index].amount.toString(),
                        style: TextStyle(fontSize: 35),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      Container(
                        width: 110,
                        decoration: BoxDecoration(
                          border: Border(
                            left: BorderSide(color: Colors.black, width: 1),
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(shareTrans.tranList[index].totalBalance
                                .toString()),
                            Text(
                              DateFormat('MMMM dd, yyyy').format(
                                  shareTrans.tranList[index].transactionDate),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
