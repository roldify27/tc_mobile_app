import 'dart:async';
import 'dart:ui';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tcmobapp/constants.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:tcmobapp/presentations/tc_mobapp_icons_icons.dart';
import 'package:tcmobapp/provider/savings_details/grouped_transactions_class.dart';
import 'package:tcmobapp/provider/user/user_details_provider.dart';
import 'package:tcmobapp/screens/fund_transfer.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart'
    as WidgetConditional;

int _current = 0;

final List<String> imgList = [
  'assets/images/advert-7.jpg',
  'assets/images/advert-8.jpg',
  'assets/images/advert-9.jpg',
  'assets/images/advert-10.jpg',
  'assets/images/advert-11.png',
  'assets/images/advert-12.png'
];

class BuildDashboardRecentTransactionsWidget extends StatefulWidget {
  final List<GroupedTransactions> transactionList;

  const BuildDashboardRecentTransactionsWidget({
    Key key,
    this.transactionList,
  }) : super(key: key);

  @override
  _BuildDashboardRecentTransactionsWidgetState createState() =>
      _BuildDashboardRecentTransactionsWidgetState();
}

class _BuildDashboardRecentTransactionsWidgetState
    extends State<BuildDashboardRecentTransactionsWidget> {
  bool _filterAll = true;
  bool _filterFundTransfer = false;
  bool _filterPayments = false;
  bool _filterCashIn = false;
  bool _isRecentTransactionLoading = false;

  int _intFilterSelected = 1;

  setFiltersFalse(int selectedStatus) {
    _filterAll = false;
    _filterFundTransfer = false;
    _filterPayments = false;
    _filterCashIn = false;

    switch (selectedStatus) {
      case 1:
        {
          if (_intFilterSelected != selectedStatus) {
            _getDashboardRecentTransactions();
          }

          _filterAll = true;
          _intFilterSelected = selectedStatus;
        }
        break;

      case 2:
        {
          if (_intFilterSelected != selectedStatus) {
            _getDashboardRecentTransactions();
          }

          _filterFundTransfer = true;
          _intFilterSelected = selectedStatus;
        }
        break;

      case 3:
        {
          if (_intFilterSelected != selectedStatus) {
            _getDashboardRecentTransactions();
          }

          _filterPayments = true;
          _intFilterSelected = selectedStatus;
        }
        break;

      case 4:
        {
          if (_intFilterSelected != selectedStatus) {
            _getDashboardRecentTransactions();
          }

          _filterCashIn = true;
          _intFilterSelected = selectedStatus;
        }
        break;

      default:
        {
          if (_intFilterSelected != selectedStatus) {
            _getDashboardRecentTransactions();
          }

          _filterAll = true;
          _intFilterSelected = 1;
        }
        break;
    }
  }

  Future<void> _getDashboardRecentTransactions() async {
    // await Provider.of<LoginProvider>(context, listen: false).setIsLoading();
    setState(() {
      _isRecentTransactionLoading = true;
    });

    await Future.delayed(Duration(seconds: 3), () async {
      // 5s over, navigate to a new page
    });

    var userDetailsProvider =
        await Provider.of<UserDetailsProvider>(context, listen: false);

    try {
      // await new Future.delayed(const Duration(seconds: 5));
      await userDetailsProvider
          .getDashboardRecentTransactionsFiltered(_intFilterSelected);
    } catch (error) {
      print("ERROR: ${error}");
    } finally {
      setState(() {
        _isRecentTransactionLoading = false;
      });
    }
  }

  Container recentTransactionsFilter(
      String rtfText, bool selectedFilterBool, int selectedFilterBoolInt) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.symmetric(vertical: 6),
      margin: EdgeInsets.symmetric(horizontal: 7),
      child: ElevatedButton(
        onPressed: () {
          setState(() {
            setFiltersFalse(selectedFilterBoolInt);
          });
        },
        style: ElevatedButton.styleFrom(
          primary: (selectedFilterBool) ? kPrimaryColor : kWhiteColor,
          enableFeedback: true,
          elevation: 1,
          // padding: EdgeInsets.all(14),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(kButtonCircularRadius),
          ),
        ),
        child: Text(
          rtfText,
          style: TextStyle(
              color: (selectedFilterBool) ? kWhiteColor : kPrimaryColor,
              fontSize: 9,
              fontWeight: FontWeight.w600,
              letterSpacing: 1),
        ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.transactionList);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        color: kOffWhiteColor,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30),
          topLeft: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            spreadRadius: 1,
            blurRadius: 20,
            offset: Offset(0, -20), // changes position of shadow
          ),
        ],
      ),
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.fromLTRB(20, 1, 20, 20),
        child: Container(
          margin: EdgeInsets.only(top: 19),
          child: Column(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(21.0),
                child: Stack(children: [
                  CarouselSlider(
                    options: CarouselOptions(

                        // height: 250,
                        autoPlay: true,
                        autoPlayAnimationDuration: Duration(milliseconds: 3000),
                        autoPlayInterval: Duration(seconds: 8),

                        // enlargeCenterPage: true,
                        aspectRatio: 3,
                        viewportFraction: 1,
                        onPageChanged: (index, reason) {
                          setState(() {
                            _current = index;
                          });
                        }),
                    items: imgList
                        .map((item) => ClipRRect(
                              // borderRadius: BorderRadius.circular(30.0),
                              child: Image.asset(
                                item,
                                fit: BoxFit.cover,
                                width: 1000,
                              ),
                            ))
                        .toList(),
                  ),
                  Positioned.fill(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: imgList.map((url) {
                          int index = imgList.indexOf(url);
                          print(index);
                          return Container(
                            width: 8.0,
                            height: 8.0,
                            margin: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 2.0),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: _current == index
                                  ? Color.fromRGBO(0, 35, 138, 0.9)
                                  : Color.fromRGBO(0, 35, 138, 0.4),
                            ),
                          );
                        }).toList(),
                      ),
                    ),
                  )
                ]),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                // padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Expanded(
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Recent Transactions',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16,
                                        color: kDarkerFontColor3),
                                  ))),
                          Expanded(
                            child: Container(
                              margin: EdgeInsets.only(right: 5),
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: InkWell(
                                  hoverColor: Colors.red,
                                  onTap: () {
                                    print('TAPPED!');
                                  },
                                  child: Text(
                                    'View All',
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: kPrimarySwatchColor,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 15),
                      child: ClipRRect(
                        // borderRadius: BorderRadius.circular(30.0),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              recentTransactionsFilter('All', _filterAll, 1),
                              recentTransactionsFilter(
                                  'Fund Transfer', _filterFundTransfer, 2),
                              recentTransactionsFilter(
                                  'Payments', _filterPayments, 3),
                              recentTransactionsFilter(
                                  'Cash In', _filterCashIn, 4),
                            ],
                          ),
                        ),
                      ),
                    ),
                    (_isRecentTransactionLoading)
                        ? SingleChildScrollView(
                            child: Container(
                              margin: EdgeInsets.only(top: 15),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    // alignment: Alignment.centerLeft,
                                    width: MediaQuery.of(context).size.width *
                                        0.20,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: skeletonInstance(),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    /* padding: EdgeInsets.symmetric(horizontal: 20), */
                                    decoration: BoxDecoration(
                                      color: kWhiteColor,
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 1,
                                          blurRadius: 1,
                                          offset: Offset(0,
                                              1), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      children: [
                                        // skeletonInstance(paddingLeft: 20),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    // alignment: Alignment.centerLeft,
                                    width: MediaQuery.of(context).size.width *
                                        0.20,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: skeletonInstance(),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    /* padding: EdgeInsets.symmetric(horizontal: 20), */
                                    decoration: BoxDecoration(
                                      color: kWhiteColor,
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 1,
                                          blurRadius: 1,
                                          offset: Offset(0,
                                              1), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      children: [
                                        // skeletonInstance(paddingLeft: 20),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container(
                            // margin: EdgeInsets.only(top: 15),
                            child: buildGroupedTransactions2(
                                widget.transactionList),
                          ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container recentTransactions(String transDetail, String transtType,
      double transAmount, String transSign, String transDate) {
    IconData iconAssetPath;
    String amount = transSign +
        ' ₱ ' +
        NumberFormat.decimalPattern().format(transAmount).toString();

    print(amount);

    /* if (transtType == 'Fund Transfer') {
    iconAssetPath = 'assets/images/trans-fund-transfer.png';
  } else if (transtType == 'Bills Payment') {
    iconAssetPath = 'assets/images/trans-bills-payment.png';
  } else if (transtType == 'Cash In') {
    iconAssetPath = 'assets/images/trans-cash-in.png';
  } else if (transtType == 'Deposit') {
    iconAssetPath = 'assets/images/trans-fund-transfer.png';
  } else if (transtType == 'Withdrawal') {
    iconAssetPath = 'assets/images/trans-cash-in.png';
  } */

    /* if (transSign == '-') {
      iconAssetPath = 'assets/images/withdrawal.png';
    } else if (transSign == '+') {
      iconAssetPath = 'assets/images/deposit.png';
    } */

    if (transSign == '-') {
      iconAssetPath = TcMobappIcons.withdrawal;
    } else if (transSign == '+') {
      iconAssetPath = TcMobappIcons.deposit;
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(right: 15),
            decoration: BoxDecoration(
              color: kOffWhiteColor,
              borderRadius: BorderRadius.circular(15),
            ),
            child: ClipRRect(
              child: Icon(
                iconAssetPath,
                size: 20,
                color: Colors.grey,
              ),
            ),
          ),
          Expanded(
            child: ListTile(
              contentPadding: EdgeInsets.zero,
              dense: true,
              title: Text(transDetail,
                  style: TextStyle(color: kPrimaryColor, fontSize: 12)),
              subtitle: Text(
                transtType,
                style: TextStyle(
                  fontSize: 11.5,
                  color: kTransactionSubtitleColor,
                ),
              ),
              trailing: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.only(bottom: 2),
                      child: Text(amount,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: (transSign == '+')
                                  ? Colors.green
                                  : Colors.red))),
                  Container(
                    margin: EdgeInsets.only(top: 2),
                    child: Text(
                      transDate,
                      style: TextStyle(
                        fontSize: 11.5,
                        color: kTransactionSubtitleColor,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Container buildGroupedTransactions2(
      List<GroupedTransactions> groupedTransactions) {
    return Container(
        // margin: EdgeInsets.only(top: 5),
        child: Column(
      children: [
        (groupedTransactions.length > 0)
            ? Column(
                children: [
                  for (var i in groupedTransactions)
                    Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            (DateFormat.yMMMMd().format(i.date) ==
                                    DateFormat.yMMMMd().format(DateTime.now()))
                                ? "Today"
                                : DateFormat.yMMMd().format(i.date).toString(),
                            style: TextStyle(
                                fontSize: 14,
                                color: kPrimaryColorFaded2,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 20),
                          /* padding: EdgeInsets.symmetric(
                                                          horizontal: 20, vertical: 10), */
                          decoration: BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.1),
                                spreadRadius: 1,
                                blurRadius: 1,
                                offset:
                                    Offset(0, 1), // changes position of shadow
                              ),
                            ],
                          ),
                          child:
                              /* Container(), */ Column(
                            children: i.transactions.map<Widget>(
                              (e) {
                                return Column(children: [
                                  recentTransactions(
                                    e.title.toString(),
                                    e.category.toString(),
                                    e.amount,
                                    (e.balanceLocator == "Credit") ? "+" : "-",
                                    DateFormat.yMMMd()
                                        .format(e.trandate)
                                        .toString(),
                                  ),
                                  Divider(
                                    color: kOffWhiteColor,
                                    height: 1,
                                    thickness: 1,
                                  ),
                                ]);
                              },
                            ).toList(),
                          ),
                        ),
                      ],
                    ),
                ],
              )
            : Center(
                child: Container(
                  margin: EdgeInsets.only(top: 50),
                  child: Column(children: [
                    SvgPicture.asset(
                      "assets/images/no-data-2.svg",
                      width: 80,
                      height: 100,

                      // color: Colors.grey,
                      // colorBlendMode: BlendMode.color,
                      // color: Colors.red,
                      // semanticsLabel: 'A red up arrow',
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 15),
                      child: Text(
                        'No Transaction Found',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.grey.withOpacity(0.80),
                        ),
                      ),
                    ),
                  ]),
                ),
              ),

        /* WidgetConditional.Conditional.single(
          context: context,
          conditionBuilder: (BuildContext context) =>
              groupedTransactions.length > 0,
          widgetBuilder: (BuildContext context) {
            for (var i = 0; i < 10; i++) {
              print(groupedTransactions);

              return Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      (DateFormat.yMMMMd()
                                  .format(groupedTransactions[i].date) ==
                              DateFormat.yMMMMd().format(DateTime.now()))
                          ? "Today"
                          : DateFormat.yMMMd()
                              .format(groupedTransactions[i].date)
                              .toString(),
                      style: TextStyle(
                          fontSize: 14,
                          color: kPrimaryColorFaded2,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    /* padding: EdgeInsets.symmetric(
                                                          horizontal: 20, vertical: 10), */
                    decoration: BoxDecoration(
                      color: kWhiteColor,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 1,
                          blurRadius: 1,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ],
                    ),
                    child:
                        /* Container(), */ Column(
                      children: groupedTransactions[i].transactions.map<Widget>(
                        (e) {
                          return Column(children: [
                            recentTransactions(
                              e.title.toString(),
                              e.category.toString(),
                              e.amount,
                              (e.balanceLocator == "Credit") ? "+" : "-",
                              DateFormat.yMMMd().format(e.trandate).toString(),
                            ),
                            Divider(
                              color: kOffWhiteColor,
                              height: 1,
                              thickness: 1,
                            ),
                          ]);
                        },
                      ).toList(),
                    ),
                  ),
                ],
              );
            }
          },
          fallbackBuilder: (BuildContext context) {
            return Center(
              child: Container(
                margin: EdgeInsets.only(top: 50),
                child: Column(children: [
                  SvgPicture.asset(
                    "assets/images/no-data-2.svg",
                    width: 80,
                    height: 100,

                    // color: Colors.grey,
                    // colorBlendMode: BlendMode.color,
                    // color: Colors.red,
                    // semanticsLabel: 'A red up arrow',
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: Text(
                      'No Transaction Found',
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey.withOpacity(0.80),
                      ),
                    ),
                  ),
                ]),
              ),
            );
          },
        ), */

        /* for (var i in groupedTransactions) 
          Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 10),
                alignment: Alignment.centerLeft,
                child: Text(
                  DateFormat.yMMMd().format(i.date).toString(),
                  style: TextStyle(
                      fontSize: 14,
                      color: kPrimaryColorFaded2,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                /* padding: EdgeInsets.symmetric(
                                                          horizontal: 20, vertical: 10), */
                decoration: BoxDecoration(
                  color: kWhiteColor,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.1),
                      spreadRadius: 1,
                      blurRadius: 1,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                ),
                child:
                    /* Container(), */ Column(
                  children: i.transactions.map<Widget>(
                    (e) {
                      return Column(children: [
                        recentTransactions(
                          e.title.toString(),
                          e.category.toString(),
                          e.amount,
                          (e.balanceLocator == "Credit") ? "+" : "-",
                          DateFormat.yMMMd().format(e.trandate).toString(),
                        ),
                        Divider(
                          color: kOffWhiteColor,
                          height: 1,
                          thickness: 1,
                        ),
                      ]);
                    },
                  ).toList(),
                ),
              ),
            ],
          ), */
      ],
    ));
  }

  Container buildGroupedTransactions(
      List<GroupedTransactions> groupedTransactions) {
    return Container(
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: [
            for (var i in groupedTransactions)
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      DateFormat.yMMMd().format(i.date).toString(),
                      style: TextStyle(
                          fontSize: 14,
                          color: kPrimaryColorFaded2,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    /* padding: EdgeInsets.symmetric(
                                                          horizontal: 20, vertical: 10), */
                    decoration: BoxDecoration(
                      color: kWhiteColor,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 1,
                          blurRadius: 1,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ],
                    ),
                    child:
                        /* Container(), */ Column(
                      children: i.transactions.map<Widget>(
                        (e) {
                          return Column(children: [
                            recentTransactions(
                              e.title.toString(),
                              e.category.toString(),
                              e.amount,
                              (e.balanceLocator == "Credit") ? "+" : "-",
                              DateFormat.yMMMd().format(e.trandate).toString(),
                            ),
                            Divider(
                              color: kOffWhiteColor,
                              height: 1,
                              thickness: 1,
                            ),
                          ]);
                        },
                      ).toList(),
                    ),
                  ),
                ],
              ),
          ],
        ));
  }
}
