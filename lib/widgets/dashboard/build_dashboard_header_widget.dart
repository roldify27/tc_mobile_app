import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tcmobapp/constants.dart';
import 'package:fl_chart/fl_chart.dart';

final spots = List.generate(101, (i) => (i - 50) / 10)
    .map((x) => FlSpot(x, sin(x)))
    .toList();

class BuildDashboardHeaderWidget extends StatefulWidget {
  final double totalShare;
  final double totalSavings;
  final double totalTimeDepo;
  final double totalLoans;
  final Uint8List photo;

  const BuildDashboardHeaderWidget({
    Key key,
    this.totalShare,
    this.totalSavings,
    this.totalTimeDepo,
    this.totalLoans,
    this.photo,
    @required GlobalKey<ScaffoldState> scaffoldKey,
  })  : _scaffoldKey = scaffoldKey,
        super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey;

  @override
  _BuildDashboardHeaderWidgetState createState() =>
      _BuildDashboardHeaderWidgetState();
}

class _BuildDashboardHeaderWidgetState
    extends State<BuildDashboardHeaderWidget> {
  Timer _timer;
  bool _visible1 = true;
  bool _visible2 = false;
  bool _visible3 = false;
  bool _visible4 = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _timer = new Timer(const Duration(seconds: 10), () {
      setState(() {
        _visible1 = false;
      });
    });
    print("TIMER: ${_timer}");
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // print("PHOTO: ${widget.photo.toString()}");

    return Container(
      margin: EdgeInsets.only(
        left: 30,
        right: 30,
        top: 30,
        bottom: 10,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Stack(
              children: [
                AnimatedOpacity(
                  onEnd: () {
                    if (_visible1 == false) {
                      setState(() {
                        _visible2 = true;
                        _timer = new Timer(const Duration(seconds: 10), () {
                          setState(() {
                            _visible2 = false;
                          });
                        });
                      });
                    }
                  },
                  opacity: _visible1 ? 1 : 0,
                  duration: Duration(milliseconds: 300),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          margin: EdgeInsets.only(top: 2),
                          child: Row(
                            children: [
                              Text(
                                'Total Share Capital',
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              )
                            ],
                          )),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                padding: EdgeInsets.only(top: 5),
                                child: Text(
                                  '₱ ',
                                  style: TextStyle(
                                    fontSize: 22,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                            Text(
                              NumberFormat.decimalPattern()
                                  .format(widget.totalShare),
                              style: TextStyle(
                                  fontSize: 33,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 2),
                          child: Row(
                            children: [
                              Text(
                                'Total Share Contributions',
                                style: TextStyle(
                                    fontSize: 12,
                                    // fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                AnimatedOpacity(
                  onEnd: () {
                    if (_visible2 == false) {
                      setState(() {
                        _visible3 = true;
                        _timer = new Timer(const Duration(seconds: 10), () {
                          setState(() {
                            _visible3 = false;
                          });
                        });
                      });
                    }
                  },
                  opacity: _visible2 ? 1 : 0,
                  duration: Duration(milliseconds: 300),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          margin: EdgeInsets.only(top: 2),
                          child: Row(
                            children: [
                              Text(
                                'Total Savings',
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              )
                            ],
                          )),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                padding: EdgeInsets.only(top: 5),
                                child: Text(
                                  '₱ ',
                                  style: TextStyle(
                                    fontSize: 22,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                            Text(
                              NumberFormat.decimalPattern()
                                  .format(widget.totalSavings),
                              style: TextStyle(
                                  fontSize: 33,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 2),
                          child: Row(
                            children: [
                              Text(
                                'Available Balance',
                                style: TextStyle(
                                    fontSize: 12,
                                    // fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                AnimatedOpacity(
                  onEnd: () {
                    if (_visible3 == false) {
                      setState(() {
                        _visible4 = true;
                        _timer = new Timer(const Duration(seconds: 10), () {
                          setState(() {
                            _visible4 = false;
                          });
                        });
                      });
                    }
                  },
                  opacity: _visible3 ? 1 : 0,
                  duration: Duration(milliseconds: 300),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          margin: EdgeInsets.only(top: 2),
                          child: Row(
                            children: [
                              Text(
                                'Total Time Deposit',
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              )
                            ],
                          )),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                padding: EdgeInsets.only(top: 5),
                                child: Text(
                                  '₱ ',
                                  style: TextStyle(
                                    fontSize: 22,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                            Text(
                              NumberFormat.decimalPattern()
                                  .format(widget.totalTimeDepo),
                              style: TextStyle(
                                  fontSize: 33,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )
                          ],
                        ),
                      ),
                      /* Container(
                          margin: EdgeInsets.only(top: 2),
                          child: Row(
                            children: [
                              Text(
                                'Available Balance',
                                style: TextStyle(
                                    fontSize: 12,
                                    // fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              )
                            ],
                          )), */
                    ],
                  ),
                ),
                AnimatedOpacity(
                  onEnd: () {
                    if (_visible4 == false) {
                      setState(() {
                        _visible1 = true;
                        _timer = new Timer(const Duration(seconds: 10), () {
                          setState(() {
                            _visible1 = false;
                          });
                        });
                      });
                    }
                  },
                  opacity: _visible4 ? 1 : 0,
                  duration: Duration(milliseconds: 300),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          margin: EdgeInsets.only(top: 2),
                          child: Row(
                            children: [
                              Text(
                                'Total Loans',
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              )
                            ],
                          )),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                padding: EdgeInsets.only(top: 5),
                                child: Text(
                                  '₱ ',
                                  style: TextStyle(
                                    fontSize: 22,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )),
                            Text(
                              NumberFormat.decimalPattern()
                                  .format(widget.totalLoans),
                              style: TextStyle(
                                  fontSize: 33,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            )
                          ],
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.only(top: 2),
                          child: Row(
                            children: [
                              Text(
                                'Current and Past Due Loans',
                                style: TextStyle(
                                    fontSize: 12,
                                    // fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                /* AnimatedOpacity(
                  onEnd: () {
                    if (_visible4 == false) {
                      setState(() {
                        _visible1 = true;
                        _timer = new Timer(const Duration(seconds: 3), () {
                          setState(() {
                            _visible1 = false;
                          });
                        });
                      });
                    }
                  },
                  opacity: _visible4 ? 1 : 0,
                  duration: Duration(milliseconds: 500),
                  child: AspectRatio(
                    aspectRatio: 1.70,
                    child: Container(
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(18),
                          ),
                          color: Color(0xff232d37)),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            right: 18.0, left: 12.0, top: 24, bottom: 12),
                        child: LineChart(
                          mainData(),
                        ),
                      ),
                    ),
                  ),
                ), */
              ],
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.topRight,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  new Stack(
                    children: <Widget>[
                      new Icon(
                        Icons.notifications,
                        size: 30,
                        color: kDashboardBackColor,
                      ),
                      new Positioned(
                        right: 0,
                        child: new Container(
                          padding: EdgeInsets.all(1),
                          decoration: new BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          constraints: BoxConstraints(
                            minWidth: 12,
                            minHeight: 12,
                          ),
                          child: new Text(
                            '!',
                            style: new TextStyle(
                              color: Colors.white,
                              fontSize: 8,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: GestureDetector(
                      onTap: () {
                        widget._scaffoldKey.currentState.openDrawer();
                      },
                      child: Column(
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: (widget.photo == null)
                                ? Image.asset(
                                    'assets/images/no-photo-male.jpg',
                                    height: 35,
                                    width: 35,
                                  )
                                : Image.memory(
                                    widget.photo,
                                    height: 35,
                                    width: 35,
                                  ), /* Image.asset(
                          'assets/images/sample-dp.png',
                          height: 35,
                          width: 35,
                        ), */
                          ),
                          Icon(
                            Icons.more_horiz,
                            color: kWhiteColor,
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

LineChartData mainData() {
  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];

  return LineChartData(
    gridData: FlGridData(
      show: false,
      drawVerticalLine: false,
      getDrawingHorizontalLine: (value) {
        return FlLine(
          color: const Color(0xff37434d),
          strokeWidth: 1,
        );
      },
      getDrawingVerticalLine: (value) {
        return FlLine(
          color: const Color(0xff37434d),
          strokeWidth: 1,
        );
      },
    ),
    titlesData: FlTitlesData(
      show: true,
      bottomTitles: SideTitles(
        showTitles: true,
        reservedSize: 22,
        getTextStyles: (value) => const TextStyle(
            color: Color(0xff68737d),
            fontWeight: FontWeight.bold,
            fontSize: 16),
        getTitles: (value) {
          switch (value.toInt()) {
            case 2:
              return 'SV';
            case 5:
              return 'TD';
            case 8:
              return 'L';
          }
          return '';
        },
        margin: 8,
      ),
      leftTitles: SideTitles(
        showTitles: false,
        getTextStyles: (value) => const TextStyle(
          color: Color(0xff67727d),
          fontWeight: FontWeight.bold,
          fontSize: 15,
        ),
        getTitles: (value) {
          switch (value.toInt()) {
            case 1:
              return '10k';
            case 3:
              return '30k';
            case 5:
              return '50k';
          }
          return '';
        },
        reservedSize: 28,
        margin: 12,
      ),
    ),
    borderData: FlBorderData(
        show: true,
        border: Border.all(color: const Color(0xff37434d), width: 1)),
    minX: 0,
    maxX: 11,
    minY: 0,
    maxY: 6,
    lineBarsData: [
      LineChartBarData(
        spots: [
          FlSpot(0, 3),
          FlSpot(2.6, 2),
          FlSpot(4.9, 5),
          FlSpot(6.8, 3.1),
          FlSpot(8, 4),
          FlSpot(9.5, 3),
          FlSpot(11, 4),
        ],
        isCurved: true,
        colors: gradientColors,
        barWidth: 5,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: false,
        ),
        belowBarData: BarAreaData(
          show: false,
          colors:
              gradientColors.map((color) => color.withOpacity(0.3)).toList(),
        ),
      ),
    ],
  );
}

/* Container(
                            child: Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.fromLTRB(25, 25, 25, 0),
                                  width: double.infinity,
                                  child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 2),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            /* border: Border.all(
                                                color: Colors.blueAccent), */
                                            borderRadius: BorderRadius.circular(
                                                kButtonCircularRadius),
                                          ),
                                          child: Container(
                                            padding: EdgeInsets.only(left: 5),
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              /* border: Border.all(
                                                color: Colors.blueAccent), */
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      kButtonCircularRadius),
                                            ),
                                            child: DropdownButton<String>(
                                              value: dropdownValue,
                                              icon: Icon(Icons.arrow_drop_down),
                                              iconSize: 20,
                                              isDense: true,
                                              // elevation: 16,
                                              style: TextStyle(
                                                  color: kPrimaryColor,
                                                  fontSize: 10),
                                              onChanged: (String newValue) {
                                                setState(() {
                                                  dropdownValue = newValue;
                                                });
                                              },
                                              underline: Container(
                                                color: kPrimaryColor,
                                              ),
                                              items: <String>[
                                                'One',
                                                'Two',
                                                'Free',
                                                'Four'
                                              ].map<DropdownMenuItem<String>>(
                                                  (String value) {
                                                return DropdownMenuItem<String>(
                                                  value: value,
                                                  child:
                                                      Text(value.toUpperCase()),
                                                );
                                              }).toList(),
                                            ),
                                          ),
                                        ),
                                        Expanded(child: Container()),
                                        new Stack(
                                          children: <Widget>[
                                            new Icon(
                                              Icons.notifications,
                                              size: 30,
                                              color: kDashboardBackColor,
                                            ),
                                            new Positioned(
                                              right: 0,
                                              child: new Container(
                                                padding: EdgeInsets.all(1),
                                                decoration: new BoxDecoration(
                                                  color: Colors.red,
                                                  borderRadius:
                                                      BorderRadius.circular(6),
                                                ),
                                                constraints: BoxConstraints(
                                                  minWidth: 12,
                                                  minHeight: 12,
                                                ),
                                                child: new Text(
                                                  '!',
                                                  style: new TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 8,
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 10),
                                          child: GestureDetector(
                                            onTap: () {
                                              _scaffoldKey.currentState
                                                  .openDrawer();
                                            },
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              child: Image.asset(
                                                'assets/images/sample-dp.png',
                                                height: 35,
                                                width: 35,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ]),
                                ),
                                Container(
                                    padding: EdgeInsets.only(
                                        left: 25, right: 25, bottom: 25),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Container(
                                                child: Row(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                top: 5),
                                                        child: Text(
                                                          '₱ ',
                                                          style: TextStyle(
                                                            fontSize: 22,
                                                            color: Colors.white,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          ),
                                                        )),
                                                    Text(
                                                      NumberFormat
                                                              .decimalPattern()
                                                          .format(3000.25),
                                                      style: TextStyle(
                                                          fontSize: 33,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.white),
                                                    )
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                  margin:
                                                      EdgeInsets.only(top: 2),
                                                  child: Row(
                                                    children: [
                                                      Text(
                                                        'Available Balance',
                                                        style: TextStyle(
                                                            fontSize: 12,
                                                            // fontWeight: FontWeight.bold,
                                                            color:
                                                                Colors.white),
                                                      )
                                                    ],
                                                  )),
                                            ],
                                          ),
                                        ),
                                      ],
                                    )),
                              ],
                            ),
                          ), */
