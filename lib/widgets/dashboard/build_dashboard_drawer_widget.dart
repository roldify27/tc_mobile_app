import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/screens/login_screen.dart';
import 'package:tcmobapp/widgets/prompts/prompt_dialogs.dart';

class BuildDashboardDrawerWidget extends StatelessWidget {
  final String fname;
  final String cid;
  final Uint8List photo;
  final String memberclass;

  const BuildDashboardDrawerWidget({
    this.fname,
    this.cid,
    this.photo,
    this.memberclass,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.zero,
      child: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: DrawerHeader(
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                    ),
                    child: Container(
                      // margin: EdgeInsets.all(120),
                      child: Row(
                        children: [
                          new Stack(
                            children: <Widget>[
                              /* CircleAvatar(
                                radius: 50,
                                backgroundColor: Colors.grey,
                                child: CircleAvatar(
                                  radius: 45,
                                  foregroundImage: MemoryImage(bytes),
                                ),
                              ), */
                              Container(
                                padding: EdgeInsets.all(15),
                                child: Container(
                                  // padding: EdgeInsets.all(3),
                                  decoration: new BoxDecoration(
                                    // color: Colors.grey.withOpacity(0.40),
                                    borderRadius: BorderRadius.circular(20),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.black.withOpacity(0.25),
                                        spreadRadius: 2,
                                        blurRadius: 5,
                                        offset: Offset(0, 2),
                                      ),
                                    ],
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20),
                                    child: (["", null, false, 0]
                                            .contains(photo.toString()))
                                        ? Image.asset(
                                            'assets/images/no-photo-male.jpg',
                                            height: 90,
                                            width: 90,
                                          )
                                        : Image.memory(
                                            photo,
                                            height: 90,
                                            width: 90,
                                          ),
                                  ),
                                ),
                              ),
                              new Positioned(
                                right: 0,
                                child: new Container(
                                  padding: EdgeInsets.fromLTRB(10, 6, 10, 8),
                                  decoration: new BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  constraints: BoxConstraints(
                                    minWidth: 12,
                                    minHeight: 12,
                                  ),
                                  child: new Text(
                                    memberclass,
                                    style: new TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                              )
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.all(5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  width: 130,
                                  margin: EdgeInsets.only(bottom: 8),
                                  child: Text(fname,
                                      // overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          letterSpacing: 2)),
                                ),
                                Text(
                                  cid,
                                  // overflow: TextOverflow.fade,
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: Colors.white,
                                      letterSpacing: 2),
                                ),
                                Container(
                                  width: 130,
                                  margin: EdgeInsets.only(top: 5),
                                  child: Text(
                                    'Regular Member',
                                    style: TextStyle(
                                        fontSize: 13,
                                        color: Colors.white,
                                        letterSpacing: 1),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                ListTile(
                  dense: true,
                  title: Text("Edit Profile"),
                  trailing: IconButton(
                      icon: Icon(Icons.chevron_right_rounded, size: 20)),
                ),
                ListTile(
                  dense: true,
                  title: Text("Rewards Points"),
                  subtitle: Text(
                    "42.26",
                    style: TextStyle(fontSize: 10),
                  ),
                  trailing: IconButton(
                      icon: Icon(Icons.chevron_right_rounded, size: 20)),
                ),
                ListTile(
                  dense: true,
                  title: Text("Job Vacancies"),
                  trailing: IconButton(
                      icon: Icon(Icons.chevron_right_rounded, size: 20)),
                ),
                ListTile(
                  dense: true,
                  title: Text("Terms & Conditions"),
                  trailing: IconButton(
                      icon: Icon(Icons.chevron_right_rounded, size: 20)),
                ),
                ListTile(
                  dense: true,
                  title: Text("Settings"),
                  trailing: IconButton(
                      icon: Icon(Icons.chevron_right_rounded, size: 20)),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(20),
            width: double.maxFinite,
            child: ElevatedButton(
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (ctx) => PromptConfirmationDialog(
                    title: "Log Out",
                    subtitle:
                        "Are you sure you want to end this session and log out?",
                    okText: "Yes",
                    cancelText: "Cancel",
                    onOK: () {
                      Navigator.of(context).popUntil((route) => route.isFirst);
                      Navigator.of(context)
                          .pushReplacementNamed(LoginScreen.routeName);
                    },
                    onCancel: () {
                      Navigator.of(context).pop();
                    },
                  ),
                );
              },
              /* shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(kButtonCircularRadius),
                                                ), */ // optional, in order to add additional space around text if needed
              style: ElevatedButton.styleFrom(
                primary: kPrimaryColor,
                padding: EdgeInsets.all(25),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(kButtonCircularRadius),
                ),
              ),
              child: Text(
                'Log Out',
                style: TextStyle(
                  color: kWhiteColor,
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1,
                ),
              ),
            ),
          ),
          /* Container(
            padding: EdgeInsets.only(top: 50, bottom: 40, left: 10, right: 10),
            width: double.infinity,
            child: RaisedButton(
              color: kLogoutColor,
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 16),
              onPressed: () {
                Navigator.of(context)
                    .pushReplacementNamed(LoginScreen.routeName);
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(23.5),
                // side: BorderSide(color: Color(0xFF00238A))
              ),
              child:
                  /* IconButton(
                  icon: Icon(Icons.logout,
                      size:
                          20)) */
                  Text(
                'Log Out',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    // fontWeight: FontWeight.bold,
                    letterSpacing: 1),
              ),
            ),
          ), */
        ],
      ),
    );
  }
}
