import 'dart:async';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/screens/savings_details.dart';
import 'package:tcmobapp/provider/savings_type.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tcmobapp/widgets/menu_icons.dart';
import 'package:tcmobapp/screens/login_screen.dart';
import 'package:tcmobapp/screens/fund_transfer.dart';

// PRESENTATIONS
import 'package:tcmobapp/presentations/tc_mobapp_icons_icons.dart';

class BuildDashboardMenuWidget extends StatelessWidget {
  final GlobalKey _dashboardGlobalKey;

  const BuildDashboardMenuWidget({
    Key key,
    @required GlobalKey dashboardGlobalKey,
  })  : _dashboardGlobalKey = dashboardGlobalKey,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(top: 20, bottom: 13),
            width: double.infinity,
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.all(15),
                  child: Row(
                    children: [
                      MenuIcons(
                        iconPath: TcMobappIcons.money_bag,
                        iconTitle: 'Savings Details',
                        menuPath: SavingsDetails(),
                      ),
                      MenuIcons(
                        iconPath: TcMobappIcons.menu_transfer,
                        iconTitle: 'Fund Transfer',
                        menuPath: FundTransfer(),
                      ),
                      MenuIcons(
                        iconPath: TcMobappIcons.loan,
                        iconTitle: 'Loans',
                        menuPath: FundTransfer(),
                      ),
                      MenuIcons(
                        iconPath: TcMobappIcons.health_insurance,
                        iconTitle: 'Insurances',
                        menuPath: FundTransfer(),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(15),
                  child: Row(
                    children: [
                      MenuIcons(
                        iconPath: TcMobappIcons.loan,
                        iconTitle: 'Payments',
                        menuPath: FundTransfer(),
                      ),
                      MenuIcons(
                        iconPath: TcMobappIcons.time_deposit,
                        iconTitle: 'Time Deposit',
                        menuPath: FundTransfer(),
                      ),
                      MenuIcons(
                        iconPath: TcMobappIcons.calculator,
                        iconTitle: 'Loan Calculator',
                        menuPath: FundTransfer(),
                      ),
                      Expanded(
                        flex: 1,
                        child: GestureDetector(
                          onTap: () {},
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.2),
                                      spreadRadius: 2,
                                      blurRadius: 5,
                                      offset: Offset(
                                          0, 2), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: Icon(
                                  Icons.more_horiz_rounded,
                                  size: 30,
                                  color: kPrimaryColor,
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                'Others',
                                style: TextStyle(
                                    fontSize: 10,
                                    /* fontWeight:
                                  FontWeight.bold, */
                                    color: Colors.white),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
