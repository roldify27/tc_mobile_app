import 'dart:typed_data';

import 'package:flutter/foundation.dart';

class FundTransferOwnResultClass with ChangeNotifier {
  String status;
  String message;
  String response;
  String reference;

  FundTransferOwnResultClass({
    this.status,
    this.message,
    this.response,
    this.reference,
  });
}
