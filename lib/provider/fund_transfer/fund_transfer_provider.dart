import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

// import 'savings_details_class.dart';
// import 'savings_details_transaction_class.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_own_destination_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_own_result_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_sources_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_transaction_class.dart';
import 'package:tcmobapp/provider/login/login_class.dart';
import 'package:localstorage/localstorage.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:tcmobapp/provider/user/user_details_class.dart';
import 'package:http/http.dart' as http;

class FundTransferProvider with ChangeNotifier {
  final LocalStorage localStorage = new LocalStorage('mobile_app');
  FundTransferClass userFundTransferDetails = new FundTransferClass();
  // var loginProvider = Provider.of<UserDetailsProvider>(context, listen: false);

  // ============================= FUND TRANSFER DETAILS =============================

  void setFundTransferDetails(FundTransferClass details) {
    userFundTransferDetails = details;
    notifyListeners();
  }

  FundTransferClass get getFundTransferDetails {
    return userFundTransferDetails;
  }

  // ============================= CHECK BRANCH AND TRANSACTION STATUS =============================

  /* Future<bool> checkBranchStatus() async {
    try {
      final response = await http.get(
        kNewServerAPI + '/check-status-transaction',
        headers: {
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      // var decrypted = kDecryptData(response.body);
      var res_body = json.decode(response.body);
      // print(res_body);
      var decrypted = json.decode(kDecryptData(res_body['crypted']));
      // print("DECRYPTED: $decrypted");

      // String status = decrypted['status'].toString();
      bool transactionStatus = decrypted['data']['transaction_status'];

      if (transactionStatus == true) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print("ERROR BRANCH STATUS: $e");
    }
  } */

  // ============================= GET SOURCES FUND TRANSFER =============================

  List<FundTransferSourcesClass> sourcesList = [];

  List<FundTransferSourcesClass> get getSourcesList {
    return [...sourcesList];
  }

  Future<void> getSources() async {
    String _transactionType = 'FUNDTRANSFER';

    sourcesList = [];

    try {
      final response = await http.get(
        kNewServerAPI + '/get-sources?transaction_type=$_transactionType',
        headers: {
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      var res_body = json.decode(response.body);
      var decrypted = json.decode(kDecryptData(res_body['crypted']));
      var sources = decrypted['data']['sources'];

      // print(sources);

      sources.forEach((item) {
        print(item);

        FundTransferSourcesClass modelToAdd = new FundTransferSourcesClass();

        modelToAdd.accountNumber = item['AccountNumber'].toString();
        modelToAdd.accountStatus = item['AccountStatus'].toString();
        modelToAdd.availableBalance =
            double.parse(item['AvailableBalance'].toString());
        modelToAdd.balance = double.parse(item['Balance'].toString());
        modelToAdd.customerID = item['fkcustomeridaccount'].toString();
        modelToAdd.holdBalance = double.parse(item['HoldBalance'].toString());
        modelToAdd.maxTries = int.parse(item['maxtries'].toString());
        modelToAdd.minMaintainingBalance =
            double.parse(item['MinMaintainingBalance'].toString());
        modelToAdd.productID = item['ProductID'].toString();
        modelToAdd.productName = item['ProductName'].toString();
        modelToAdd.slCode = item['SLCode'].toString();
        modelToAdd.transMaxAmtPerDay =
            double.parse(item['maxamtperday'].toString());
        modelToAdd.transMaxAmtPerTrans =
            double.parse(item['maxamtpertrans'].toString());
        modelToAdd.transMinAmount = double.parse(item['minamt'].toString());

        sourcesList.add(modelToAdd);
      });
    } catch (e) {
      print("ERROR BRANCH STATUS: $e");
    }
  }

  // ============================= GET SOURCES FUND TRANSFER =============================

  List<FundTransferOwnDestinationClass> ownDestinationList = [];

  List<FundTransferOwnDestinationClass> get getOwnDestinationList {
    return [...ownDestinationList];
  }

  Future<void> fetchOwnDestinations() async {
    ownDestinationList = [];

    try {
      final response = await http.get(
        kNewServerAPI + '/get-fund-transfer-own-destinations',
        headers: {
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      var res_body = json.decode(response.body);
      var decrypted = json.decode(kDecryptData(res_body['crypted']));
      var destinations = decrypted['data']['destinations'];

      print(destinations);

      destinations.forEach((item) {
        print("Product");
        print(item['ProductName'].toString());

        FundTransferOwnDestinationClass modelToAdd =
            new FundTransferOwnDestinationClass();

        modelToAdd.accountNumber = item['AccountNumber'].toString();
        modelToAdd.balance = double.parse(item['Balance'].toString());
        modelToAdd.customerID = item['fkcustomeridaccount'].toString();
        modelToAdd.holdBalance = double.parse(item['HoldBalance'].toString());
        modelToAdd.productID = item['ProductID'].toString();
        modelToAdd.productName = item['ProductName'].toString();
        modelToAdd.slCode = item['SLCode'].toString();
        modelToAdd.customerID = item['fkcustomeridaccount'].toString();

        if (modelToAdd.productID == 'SC') {
          modelToAdd.shareCapLimit =
              double.parse(item['ShareCapLimit'].toString());
        } else {
          modelToAdd.shareCapLimit = 0;
        }

        ownDestinationList.add(modelToAdd);
      });
    } catch (e) {
      print("ERROR BRANCH STATUS: $e");
    }
  }

  // ============================= PROCESS FUND TRANSFER =============================

  FundTransferOwnResultClass resultOwn = new FundTransferOwnResultClass();

  FundTransferOwnResultClass get getResultOwn {
    return resultOwn;
  }

  Future<void> processFundTransferOwn(FundTransferClass e) async {
    DateTime now = new DateTime.now();
    DateTime date = new DateTime(now.year, now.month, now.day);

    resultOwn = new FundTransferOwnResultClass();

    var encryptedBody = kEncryptJSONBody(
      kEncryptData(
        json.encode(
          {
            "data": {
              "cidto": kUserDetails.cid.toString(),
              "accnofrom": e.source.accountNumber.toString(),
              "accnoto": e.transferToOwn.accountNumber.toString(),
              "prodnamefrom": e.source.productName.toString(),
              "prodnameto": e.transferToOwn.productName.toString(),
              "prodidfrom": e.source.productID.toString(),
              "prodidto": e.transferToOwn.productID.toString(),
              "oldbalancefrom": e.source.balance,
              "oldbalanceto": e.transferToOwn.balance,
              "amount": e.amount,
              "transferfee": 0,
              "tracknumber": kUserDetails.trackNumber,
              "datetime": now.toString(),
              "dateonly": date.toString(),
            },
          },
        ),
      ),
    );

    try {
      final response = await http.post(
        kNewServerAPI + '/process-fund-transfer-own',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
        body: encryptedBody,
      );

      var res_body = json.decode(response.body);

      // print(res_body);
      var decrypted = json.decode(kDecryptData(res_body['crypted']));
      print("DECRYPTED");
      print(decrypted['status']);
      // print(decrypted['status'].toString());

      resultOwn.status = decrypted['status'].toString();
      resultOwn.message = decrypted['message'].toString();
      resultOwn.response = decrypted['data']['response'].toString();
      resultOwn.reference = decrypted['data']['reference'].toString();

      print(resultOwn.status);
      // var destinations = decrypted['data']['destinations'];

      /* var res_body = json.decode(response.body);
      var decrypted = json.decode(kDecryptData(res_body['crypted']));
      var destinations = decrypted['data']['destinations'];

      print(destinations);

      destinations.forEach((item) {
        print("Product");
        print(item['ProductName'].toString());

        FundTransferOwnDestinationClass modelToAdd =
            new FundTransferOwnDestinationClass();

        modelToAdd.accountNumber = item['AccountNumber'].toString();
        modelToAdd.balance = double.parse(item['Balance'].toString());
        modelToAdd.customerID = item['fkcustomeridaccount'].toString();
        modelToAdd.holdBalance = double.parse(item['HoldBalance'].toString());
        modelToAdd.productID = item['ProductID'].toString();
        modelToAdd.productName = item['ProductName'].toString();
        modelToAdd.slCode = item['SLCode'].toString();

        if (modelToAdd.productID == 'SC') {
          modelToAdd.shareCapLimit =
              double.parse(item['ShareCapLimit'].toString());
        } else {
          modelToAdd.shareCapLimit = 0;
        }

        ownDestinationList.add(modelToAdd);
      }); */
      print("it goes here");
      print(resultOwn.status);
      print(resultOwn.message);
      print(resultOwn.reference);
      print(resultOwn.response);
      // return resultOwn;
    } catch (e) {
      print("ERROR FT OWN PROCESS: $e");
      throw new Exception(e);
    }
  }

  // ============================= PROCESS FUND TRANSFER =============================

  FundTransferOwnResultClass resultOther = new FundTransferOwnResultClass();

  FundTransferOwnResultClass get getResultOther {
    return resultOther;
  }

  Future<FundTransferOwnResultClass> processFundTransferOther(
      FundTransferClass e) async {
    DateTime now = new DateTime.now();
    DateTime date = new DateTime(now.year, now.month, now.day);

    resultOther = new FundTransferOwnResultClass();

    var encryptedBody = kEncryptJSONBody(
      kEncryptData(
        json.encode(
          {
            "data": {
              // "cidto": kUserDetails.cid.toString(),
              "accnofrom": e.source.accountNumber.toString(),
              "accnamefrom": kUserDetails.fname.toString(),
              "accnoto": e.transferToOther.accountNumber.toString(),
              "accnameto": e.transferToOther.accountName.toString(),
              "prodnamefrom": e.source.productName.toString(),
              // "prodnameto": e.transferToOwn.productName.toString(),
              "prodidfrom": e.source.productID.toString(),
              // "prodidto": e.transferToOwn.productID.toString(),
              "oldbalancefrom": e.source.balance,
              // "oldbalanceto": e.transferToOwn.balance,
              "amount": e.amount,
              "transferfee": 0,
              "tracknumber": kUserDetails.trackNumber,
              "datetime": now.toString(),
              "dateonly": date.toString(),
            },
          },
        ),
      ),
    );

    /* print("PROCESS FUND TRANSFER");
    print(json.encode(
      {
        "data": {
          // "cidto": kUserDetails.cid.toString(),
          "accnofrom": e.source.accountNumber.toString(),
          "accnamefrom": kUserDetails.fname.toString(),
          "accnoto": e.transferToOther.accountNumber.toString(),
          "accnameto": e.transferToOther.accountName.toString(),
          "prodnamefrom": e.source.productName.toString(),
          // "prodnameto": e.transferToOwn.productName.toString(),
          "prodidfrom": e.source.productID.toString(),
          // "prodidto": e.transferToOwn.productID.toString(),
          "oldbalancefrom": e.source.balance,
          // "oldbalanceto": e.transferToOwn.balance,
          "amount": e.amount,
          "transferfee": 0,
          "tracknumber": kUserDetails.trackNumber,
          "datetime": now.toString(),
          "dateonly": date.toString(),
        },
      },
    )); */

    try {
      final response = await http.post(
        kNewServerAPI + '/process-fund-transfer-other',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
        body: encryptedBody,
      );

      var res_body = json.decode(response.body);

      // print(res_body);
      var decrypted = json.decode(kDecryptData(res_body['crypted']));
      print("DECRYPTED");
      print(decrypted);

      resultOther.status = decrypted['status'].toString();
      resultOther.message = decrypted['message'].toString();
      resultOther.response = decrypted['data']['response'].toString();
      resultOther.reference = decrypted['data']['reference'].toString();

      // return resultOwn;
    } catch (e) {
      print("ERROR FT OWN PROCESS: $e");

      throw new Exception(e);
    }
  }

  // ============================= GET FUND TRANSFER TRANSACTIONS =============================

  List<FundTransferTransactionClass> _fundTransferTransactionsAll = [];
  List<FundTransferTransactionClass> _fundTransferTransactionsRange = [];
  int totalTransactionCount = 0;

  List<FundTransferTransactionClass> get getFundTransferTransactionsAll {
    return [..._fundTransferTransactionsAll];
  }

  List<FundTransferTransactionClass> get getFundTransferTransactionsRange {
    return [..._fundTransferTransactionsRange];
  }

  int get getFundTransferTotalTransactionCount {
    return totalTransactionCount;
  }

  Future<void> fetchFundTransferTransactions(int range) async {
    if (range < 1) {
      _fundTransferTransactionsAll = [];
      totalTransactionCount = 0;
    }

    _fundTransferTransactionsRange = [];

    try {
      final response = await http.get(
        kNewServerAPI +
            '/get-fund-transfer-transactions-infinite-scrolling?range=$range',
        headers: {
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      var res_body = json.decode(response.body);
      var decrypted = json.decode(kDecryptData(res_body['crypted']));
      var transactions = decrypted['data']['transactions'];
      // List l = transactions;

      // print(transactions[0]);

      if (range < 1 && transactions.length > 0) {
        totalTransactionCount = int.parse(transactions[0]['count'].toString());
      }

      transactions.forEach((item) {
        // print("HERE");
        // print(item);

        FundTransferTransactionClass modelToAdd =
            new FundTransferTransactionClass();

        modelToAdd.title = item['BalanceLocator'].toString() +
            " " +
            item['TransferType'].toString();
        modelToAdd.amount = double.parse(item['AmountTransferred'].toString());
        modelToAdd.balanceLocator = item['BalanceLocator'].toString();
        modelToAdd.trandate = DateTime.parse(
          item['TransactionTime'].toString(),
        );
        modelToAdd.category = item['Category'];
        modelToAdd.dateGroup = DateTime.parse(
            kFormatter.format(DateTime.parse(item['TransactionTime'])));

        // modelToAdd.accountNumber = item['AccountNumber'].toString();
        // modelToAdd.productName = item['ProductName'].toString();
        // modelToAdd.productID = item['ProductID'].toString();
        // modelToAdd.transferFee = double.parse(item['TransferFee'].toString());
        // modelToAdd.transactionType = item['TransactionType'].toString();
        // modelToAdd.transferType = item['ProductID'].toString();
        // modelToAdd.transactionStatus = item['TransactionStatus'].toString();
        modelToAdd.reference = item['Reference'].toString();
        // modelToAdd.destMember = item['DestMember'].toString();
        // modelToAdd.productIDSourceDest = (isset(item['ProductIDSourceDest']))
        //     ? item['ProductIDSourceDest'].toString()
        //     : '-';
        // modelToAdd.transactionFlow = item['TransactionFlow'].toString();
        // modelToAdd.accNumSourceDest = item['AccNumSourceDest'].toString();
        // modelToAdd.accNumSourceDestLastFour = item['AccNumLastFour'].toString();
        // modelToAdd.accountNameDestOtherTrans = item['AccountName'].toString();
        // modelToAdd.accountNameSourceOtherTrans =
        //     item['AccountNameSource'].toString();
        // modelToAdd.destProductName = item['DestProductName'].toString();
        // modelToAdd.sourceProductName = item['SourceProductName'].toString();
        // modelToAdd.isPrimary = int.parse(item['IsPrimary'].toString());
        // modelToAdd.bankAccName = item['BankAccName'].toString();
        // modelToAdd.bankAccNum = item['BankAccNum'].toString();
        // modelToAdd.bankAddress = item['BankAccAddress'].toString();
        // modelToAdd.bankCode = item['BankAccCode'].toString();

        _fundTransferTransactionsRange.add(modelToAdd);
      });

      _fundTransferTransactionsAll.addAll(_fundTransferTransactionsRange);

      notifyListeners();
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    } finally {
      // setIsLoadingTransactions();
    }
  }
}
