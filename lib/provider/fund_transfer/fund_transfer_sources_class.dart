import 'package:flutter/foundation.dart';

class FundTransferSourcesClass with ChangeNotifier {
  String customerID;
  String accountNumber;
  String productName;
  String productID;
  String accountStatus;
  String slCode;
  num balance;
  num holdBalance;
  num minMaintainingBalance;
  num availableBalance;
  num transMinAmount;
  num transMaxAmtPerTrans;
  num transMaxAmtPerDay;
  int maxTries;

  FundTransferSourcesClass({
    this.customerID,
    this.accountNumber,
    this.productName,
    this.productID,
    this.accountStatus,
    this.slCode,
    this.balance,
    this.holdBalance,
    this.minMaintainingBalance,
    this.availableBalance,
    this.transMinAmount,
    this.transMaxAmtPerTrans,
    this.transMaxAmtPerDay,
    this.maxTries,
  });
}
