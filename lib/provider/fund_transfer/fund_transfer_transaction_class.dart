import 'dart:typed_data';

import 'package:flutter/foundation.dart';

class FundTransferTransactionClass with ChangeNotifier {
  String title;
  String category;
  String balanceLocator;
  DateTime trandate;
  DateTime dateGroup;
  num amount;
  String accountNumber;
  String productName;
  String productID;
  num transferFee;
  String transactionType;
  String transferType;
  String transactionStatus;
  String reference;
  String destMember;
  String productIDSourceDest;
  String transactionFlow;
  String accNumSourceDest;
  String accNumSourceDestLastFour;
  String accountNameDestOtherTrans;
  String accountNameSourceOtherTrans;
  String sourceProductName;
  String destProductName;
  int isPrimary;
  String bankAccName;
  String bankAccNum;
  String bankAddress;
  String bankCode;

  FundTransferTransactionClass({
    this.title,
    this.category,
    this.balanceLocator,
    this.trandate,
    this.dateGroup,
    this.amount,
    this.accountNumber,
    this.productName,
    this.productID,
    this.transferFee,
    this.transactionType,
    this.transferType,
    this.transactionStatus,
    this.reference,
    this.destMember,
    this.productIDSourceDest,
    this.transactionFlow,
    this.accNumSourceDest,
    this.accNumSourceDestLastFour,
    this.accountNameDestOtherTrans,
    this.accountNameSourceOtherTrans,
    this.sourceProductName,
    this.destProductName,
    this.isPrimary,
    this.bankAccName,
    this.bankAccNum,
    this.bankAddress,
    this.bankCode,
  });
}
