import 'package:flutter/foundation.dart';

class FundTransferOwnDestinationClass with ChangeNotifier {
  String customerID;
  String accountNumber;
  String productName;
  String productID;
  String slCode;
  num balance;
  num holdBalance;
  num shareCapLimit;

  FundTransferOwnDestinationClass({
    this.customerID,
    this.accountNumber,
    this.productName,
    this.productID,
    this.slCode,
    this.balance,
    this.holdBalance,
    this.shareCapLimit,
  });
}
