import 'package:flutter/foundation.dart';

class FundTransferOtherDestinationClass with ChangeNotifier {
  String customerID;
  String accountNumber;
  String accountName;
  String productName;
  String productID;
  String slCode;
  num balance;
  num holdBalance;
  num shareCapLimit;

  FundTransferOtherDestinationClass({
    this.customerID,
    this.accountNumber,
    this.accountName,
    this.productName,
    this.productID,
    this.slCode,
    this.balance,
    this.holdBalance,
    this.shareCapLimit,
  });
}
