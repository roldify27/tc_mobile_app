import 'package:flutter/foundation.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_other_destination_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_own_destination_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_sources_class.dart';

class FundTransferClass with ChangeNotifier {
  FundTransferSourcesClass source;
  String transferType;
  String transferTypeFull;
  FundTransferOwnDestinationClass transferToOwn;
  FundTransferOtherDestinationClass transferToOther;
  String transferTo;
  String otherAccountNumber;
  String otherAccountName;
  String beneAccountNumber;
  String beneAccountName;
  String beneAddress;
  double amount;
  String code;
  String remarks;

  FundTransferClass({
    this.source,
    this.transferType,
    this.transferTypeFull,
    this.transferToOwn,
    this.transferToOther,
    this.transferTo,
    this.otherAccountNumber,
    this.otherAccountName,
    this.beneAccountNumber,
    this.beneAccountName,
    this.beneAddress,
    this.amount,
    this.code,
    this.remarks,
  });
}
