import 'package:flutter/foundation.dart';

class MemberRegistrationClass with ChangeNotifier {
  String userName;
  String passWord;
  String email;
  String code;

  MemberRegistrationClass({
    this.userName,
    this.passWord,
    this.email,
    this.code,
  });
}
