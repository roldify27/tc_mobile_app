import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// import 'savings_details_class.dart';
// import 'savings_details_transaction_class.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/member_registration/member_registration_class.dart';
import 'package:tcmobapp/provider/member_registration/member_verification_class.dart';
import 'package:http/http.dart' as http;

class MemberRegistrationProvider with ChangeNotifier {
  MemberVerificationClass registrantData = new MemberVerificationClass();

  MemberVerificationClass get getRegistrantData {
    return registrantData;
  }

  Future<String> verifyCustomer(String cid, String mobileNumber) async {
    try {
      final response = await http.get(
        'http://192.168.23.30:3000/mobile-app/v1/verify-data-registrant/${cid}/${mobileNumber}',
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
        },
      );

      print("RESULT_VERIFICATION: ${response.body}");

      /* print(json.encode({
        "user": {
          "cid": "123456",
          "username": "mickey",
          "name": "Mickey DinoBook",
          "password": "book",
          "email": "testtesting@gmail.com",
          "active_key": "1",
          "last_visit": "04/15/2021",
          "is_admin": "1",
          "is_active": "1"
        },
      })); */

      var res_body = json.decode(response.body);
      print("RESULT_REGISTRATION: ${res_body}");

      if (res_body["account_data"] == null &&
          res_body["customer_data"] != null) {
        registrantData.cid = res_body["customer_data"]["FKCustomerIDAdd"];
        registrantData.mobileNumber = res_body["customer_data"]["PhoneNumber"];
        registrantData.firstName = res_body["customer_data"]["FirstName"];
        registrantData.middleName = res_body["customer_data"]["MiddleName"];
        registrantData.lastName = res_body["customer_data"]["LastName"];

        return "Proceed";
      } else if (res_body["customer_data"] == null &&
          res_body["account_data"] == null) {
        return "No Data";
      } else {
        return "Already Exist";
      }

      notifyListeners();
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    }
  }

  Future<void> saveRegistration(MemberVerificationClass userAccDetails,
      MemberRegistrationClass userRegDetails) async {
    try {
      /* String encrypted_password =
          await kEncryptPassword(userRegDetails.passWord); */

      String encrypted_password =
          await kEncryptPasswordMD5(userRegDetails.passWord);

      final response = await http.post(
        'http://192.168.23.30:3000/insert/v1/insert-users',
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
        body: json.encode({
          "user": {
            "cid": userAccDetails.cid,
            "username": userRegDetails.userName,
            "name": userAccDetails.lastName +
                ", " +
                userAccDetails.firstName +
                " " +
                userAccDetails.middleName,
            "password": encrypted_password,
            "email": userRegDetails.email,
            "active_key": userRegDetails.code,
            "last_visit": "01/01/1990",
            "is_admin": "1",
            "is_active": "1"
          },
        }),
        /* body: json.encode({
          "user": {
            "cid": "123456",
            "username": "mickey",
            "name": "Mickey DinoBook",
            "password": "book",
            "email": "roninobanga@gmail.com",
            "active_key": "1",
            "last_visit": "01/01/1990",
            "is_admin": "1",
            "is_active": "1"
          },
        }), */
      );

      /* print(json.encode({
        "user": {
          "cid": "123456",
          "username": "mickey",
          "name": "Mickey DinoBook",
          "password": "book",
          "email": "testtesting@gmail.com",
          "active_key": "1",
          "last_visit": "04/15/2021",
          "is_admin": "1",
          "is_active": "1"
        },
      })); */

      var res_body = json.decode(response.body);
      print("RESULT_REGISTRATION: ${res_body}");

      notifyListeners();
      /* Timer(Duration(seconds: 5), () {
        notifyListeners();
      }); */
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    }
  }

  Future<bool> checkUniqueUsername(String userName) async {
    try {
      final response = await http.get(
        kNewServerAPI + '/check-unique-username/${userName}',
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      );

      var res_body = json.decode(response.body);
      print("RESULT_UNIQUE_USERNAME: ${res_body['data']['bool_unique']}");

      notifyListeners();

      if (res_body['data']['bool_unique']) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    }
  }

  Future<bool> checkUniqueEmail(String email) async {
    try {
      final response = await http.get(
        kNewServerAPI + '/check-unique-email/${email}',
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
      );

      var res_body = json.decode(response.body);
      print("RESULT_UNIQUE_EMAIL: ${res_body['data']['bool_unique']}");

      notifyListeners();

      if (res_body['data']['bool_unique']) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    }
  }
}
