import 'package:flutter/foundation.dart';

class MemberVerificationClass with ChangeNotifier {
  String cid;
  String mobileNumber;
  String firstName;
  String middleName;
  String lastName;

  MemberVerificationClass({
    this.cid,
    this.mobileNumber,
    this.firstName,
    this.middleName,
    this.lastName,
  });
}
