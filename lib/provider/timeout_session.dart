import 'package:flutter/foundation.dart';

class TimeoutSession with ChangeNotifier {
  final int _timeout = 5;
  bool isTimedOut;

  TimeoutSession({this.isTimedOut = false});

  int get timeout {
    return _timeout;
  }

  void triggerTimeout() {
    this.isTimedOut = true;
    notifyListeners();
  }
}
