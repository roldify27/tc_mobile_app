import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// import 'savings_details_class.dart';
// import 'savings_details_transaction_class.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/login/login_class.dart';
import 'package:localstorage/localstorage.dart';

import 'package:http/http.dart' as http;

class IdleProvider with ChangeNotifier {
  bool isIdle = false;
  bool isInit = false;
  Timer _timer;

  bool get getIdleStatus {
    return this.isIdle;
  }

  void setIdleStatusFalse() {
    this.isIdle = false;
    notifyListeners();
  }

  void setIdleStatusTrue() {
    this._timer.cancel();
    this.isIdle = true;
    notifyListeners();
  }

  void handleUserInteraction([_]) {
    // if (this.isInit == false) {
    //   initializeTimer();
    //   this.isInit = true;
    // }

    if (!this._timer.isActive) {
      // This means the user has been logged out
      return;
    }

    this._timer.cancel();
    initializeTimer();
    // notifyListeners();
  }

  void initializeTimer() {
    // print("${this.isIdle}");
    this._timer = new Timer(const Duration(seconds: 10), () {
      print("Timer Expired!");
      setIdleStatusTrue();
    });
    print("initialized");
    // notifyListeners();
  }
}
