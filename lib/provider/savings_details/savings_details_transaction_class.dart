import 'package:flutter/foundation.dart';

class SavingsDetailsTransactionClass with ChangeNotifier {
  final double amount;
  final double runningBalance;
  final String referenceNo;
  final String balanceLocator;
  final DateTime transactionTime;
  final DateTime dateGroup;
  final int totalRows;

  SavingsDetailsTransactionClass(
      {this.amount,
      this.runningBalance,
      this.referenceNo,
      this.balanceLocator,
      this.transactionTime,
      this.dateGroup,
      this.totalRows});
}
