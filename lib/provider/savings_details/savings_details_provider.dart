import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:localstorage/localstorage.dart';

import 'savings_details_class.dart';
import 'savings_details_transaction_class.dart';
import 'package:tcmobapp/constants.dart';

import 'package:http/http.dart' as http;

class SavingsDetailsClassProvider with ChangeNotifier {
  final LocalStorage localStorage = new LocalStorage('mobile_app');
  List<SavingsDetailsClass> _savingsDetails = [];
  List<SavingsDetailsClass> _filteredSavingDetailsList = [];
  bool isLoading = false;
  String focusedAccountNumber = '';
  int _filterOption = 1;

  String get getFocusedAccountNumber {
    return focusedAccountNumber;
  }

  void setFocusedAccountNumber(String argAccNum) {
    focusedAccountNumber = argAccNum;
    notifyListeners();
  }

  bool get getLoadingStatus {
    return this.isLoading;
  }

  int get getFilterOption {
    return _filterOption;
  }

  void setFilterOption(int filterNum) {
    _filterOption = filterNum;

    WidgetsBinding.instance.addPostFrameCallback((_) {
      notifyListeners();
    });
  }

  Future<void> fetchData() async {
    _savingsDetails = [];

    try {
      print(isLoading);
      final response = await http.get(
        kNewServerAPI + '/get-all-member-savings',
        headers: {
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      var res_body = json.decode(response.body);
      print(res_body['data']['savings']);
      List l = res_body['data']['savings'];

      List.from(l.map((model) {
        final modelToAdd = SavingsDetailsClass(
          accountTitle: model['ProductName'].toString(),
          accountNumber: model['AccountNumber'].toString(),
          accountHolder: model['fkcustomeridaccount'].toString(),
          availableBalance: double.parse(model['Balance'].toString()),
          openedDate: DateTime.parse(model['OpeningDate'].toString()),
          isActive: (model['AccountStatus'].trim() == 'Active') ? true : false,
          cardOpacity: 0.65,
          passbookNo: (["", null].contains(model['PassbookNo'].toString()))
              ? 'No Passbook'
              : (model['PassbookNo'].toString() != '')
                  ? model['PassbookNo'].toString()
                  : 'No Passbook',
          interestRate: double.parse(model['Interest Rate'].toString()),
          productID: model['ProductID'].toString(),
        );

        _savingsDetails.add(modelToAdd);
      }));
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  List<SavingsDetailsClass> get items {
    if (_filterOption != 1) {
      switch (_filterOption) {
        case 1:
          {
            _filteredSavingDetailsList = _savingsDetails;
          }
          break;

        case 2:
          {
            _filteredSavingDetailsList =
                _savingsDetails.where((i) => (i.productID == 'SC')).toList();
          }
          break;

        case 3:
          {
            _filteredSavingDetailsList =
                _savingsDetails.where((i) => (i.productID == 'SV')).toList();
          }
          break;

        case 4:
          {
            _filteredSavingDetailsList =
                _savingsDetails.where((i) => (i.productID == 'TD')).toList();
          }
          break;

        default:
          {}
          break;
      }

      return [..._filteredSavingDetailsList];
    } else {
      return [..._savingsDetails];
    }
  }

  SavingsDetailsClass findByAccountNumber(String accountNumber) {
    return items
        .firstWhere((element) => element.accountNumber == accountNumber);
  }

  setIsLoading() {
    isLoading = !isLoading;
  }

  void addAccount() {
    // _savingsDetails.add(value);
    notifyListeners();
  }

  // ============================= TRANSACTIONS =============================

  List<SavingsDetailsTransactionClass> _selectedSavingsTransactions = [];
  bool isLoadingTransactions = false;

  bool get getLoadingStatusTransactions {
    return this.isLoadingTransactions;
  }

  Future<void> onFilterChange() async {
    if (_filterOption != 1) {
      // if all filter is not selected
      if (_filteredSavingDetailsList.length > 0) {
        setFocusedAccountNumber(_filteredSavingDetailsList[0].accountNumber);
        fetchDataTransactions(_filteredSavingDetailsList[0].accountNumber);
      } else {
        _selectedSavingsTransactions = [];
      }
    } else {
      fetchDataTransactions(_savingsDetails[0].accountNumber);
      setFocusedAccountNumber(_savingsDetails[0].accountNumber);
    }
  }

  Future<void> fetchDataTransactions(String accountNumber) async {
    _selectedSavingsTransactions = [];

    try {
      final response = await http.get(
        kNewServerAPI +
            '/get-selected-savings-transaction?account_number=$accountNumber',
        headers: {
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      var res_body = json.decode(response.body);
      List l = res_body['data']['transactions'];

      final DateFormat formatter = DateFormat('yyyy-MM-dd');

      List.from(l.map((model) {
        final modelToAddTransactions = SavingsDetailsTransactionClass(
          amount: double.parse(model['Amount'].toString()),
          runningBalance: double.parse(model['RunningBalance'].toString()),
          referenceNo: model['ReferenceNo'].toString(),
          balanceLocator: (model['BalanceLocator'].toString() == '1')
              ? 'Deposit'
              : 'Withdrawal',
          transactionTime: DateTime.parse(
            model['TransactionTime'].toString(),
          ),
          dateGroup: DateTime.parse(
              formatter.format(DateTime.parse(model['TransactionTime']))),
        );

        _selectedSavingsTransactions.add(modelToAddTransactions);
      }));

      notifyListeners();
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    } finally {
      // setIsLoadingTransactions();
    }
  }

  List<SavingsDetailsTransactionClass> get transactions {
    return [..._selectedSavingsTransactions];
  }

  setIsLoadingTransactions() {
    isLoadingTransactions = !isLoadingTransactions;
    notifyListeners();
  }

  // ============================= ALL TRANSACTIONS =============================

  List<SavingsDetailsTransactionClass> _allTransactions = [];
  List<SavingsDetailsTransactionClass> _moreTransactions = [];
  bool isAllTransactionsLoading = false;
  int totalTransactionCount = 0;

  bool get getAllTransactionsLoadingStatus {
    return this.isAllTransactionsLoading;
  }

  int get getTotalTransactionCount {
    return this.totalTransactionCount;
  }

  Future<void> fetchAllTransactions(String accountNumber) async {
    print("ALL ACC NUM : $accountNumber");

    _allTransactions = [];
    totalTransactionCount = 0;

    try {
      final response = await http.get(
        kNewServerAPI +
            '/get-selected-savings-transaction?account_number=$accountNumber',
        headers: {
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      var res_body = json.decode(response.body);
      List l = res_body['data']['transactions'];

      if (transactions.length > 0) {
        totalTransactionCount = int.parse(l[0]['count'].toString());
      }

      final DateFormat formatter = DateFormat('yyyy-MM-dd');

      List.from(l.map((model) {
        final modelToAddTransactions = SavingsDetailsTransactionClass(
          amount: double.parse(model['Amount'].toString()),
          runningBalance: double.parse(model['RunningBalance'].toString()),
          referenceNo: model['ReferenceNo'].toString(),
          balanceLocator: (model['BalanceLocator'].toString() == '1')
              ? 'Deposit'
              : 'Withdrawal',
          transactionTime: DateTime.parse(
            model['TransactionTime'].toString(),
          ),
          dateGroup: DateTime.parse(
              formatter.format(DateTime.parse(model['TransactionTime']))),
          totalRows: int.parse(model['count'].toString()),
        );

        _allTransactions.add(modelToAddTransactions);
      }));

      notifyListeners();
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    } finally {
      // setIsLoadingTransactions();
    }
  }

  Future<void> fetchMoreTransactions(String accountNumber, int range) async {
    _moreTransactions = [];

    try {
      final response = await http.get(
        kNewServerAPI +
            '/get-more-selected-savings-transaction?account_number=$accountNumber&&range=$range',
        headers: {
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      var res_body = json.decode(response.body);
      List l = res_body['data']['transactions'];

      final DateFormat formatter = DateFormat('yyyy-MM-dd');

      List.from(l.map((model) {
        final modelToAddTransactions = SavingsDetailsTransactionClass(
          amount: double.parse(model['Amount'].toString()),
          runningBalance: double.parse(model['RunningBalance'].toString()),
          referenceNo: model['ReferenceNo'].toString(),
          balanceLocator: (model['BalanceLocator'].toString() == '1')
              ? 'Deposit'
              : 'Withdrawal',
          transactionTime: DateTime.parse(
            model['TransactionTime'].toString(),
          ),
          dateGroup: DateTime.parse(
              formatter.format(DateTime.parse(model['TransactionTime']))),
          totalRows: int.parse(model['count'].toString()),
        );

        _moreTransactions.add(modelToAddTransactions);
      }));

      notifyListeners();
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    } finally {
      // setIsLoadingTransactions();
    }
  }

  List<SavingsDetailsTransactionClass> get getAllTransactions {
    return [..._allTransactions];
  }

  List<SavingsDetailsTransactionClass> get getMoreTransactions {
    return [..._moreTransactions];
  }

  setIsLoadingAllTransactions() {
    isAllTransactionsLoading = !isAllTransactionsLoading;
    notifyListeners();
  }
}
