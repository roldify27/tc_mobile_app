import 'package:flutter/foundation.dart';
import 'package:tcmobapp/provider/savings_details/savings_details_transaction_class.dart';

class GroupedTransactions with ChangeNotifier {
  final DateTime date;
  final List<dynamic> transactions;

  GroupedTransactions({
    this.date,
    this.transactions,
  });
}
