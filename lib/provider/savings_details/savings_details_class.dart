import 'package:flutter/foundation.dart';

class SavingsDetailsClass with ChangeNotifier {
  final String accountTitle;
  final String accountNumber;
  final String accountHolder;
  final double availableBalance;
  final DateTime openedDate;
  final bool isActive;
  final double cardOpacity;
  final String passbookNo;
  final double interestRate;
  final String productID;

  SavingsDetailsClass(
      {this.accountTitle,
      this.accountNumber,
      this.accountHolder,
      this.availableBalance,
      this.openedDate,
      this.isActive,
      this.cardOpacity,
      this.passbookNo,
      this.interestRate,
      this.productID});
}
