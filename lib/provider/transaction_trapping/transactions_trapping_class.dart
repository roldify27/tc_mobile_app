import 'package:flutter/foundation.dart';

class TransactionTrappingClass with ChangeNotifier {
  bool isOBSATagged;
  bool isBranchActive;
  bool statusFundTransfer;
  bool statusFundTransferOwn;
  bool statusFundTransferOther;
  bool statusFundTransferBanks;
  bool statusPayments;
  bool statusBillsPayment;
  bool statusLoanPayment;
  bool statusInsurancePayment;

  TransactionTrappingClass({
    this.isOBSATagged,
    this.isBranchActive,
    this.statusFundTransfer,
    this.statusFundTransferOwn,
    this.statusFundTransferOther,
    this.statusFundTransferBanks,
    this.statusPayments,
    this.statusBillsPayment,
    this.statusLoanPayment,
    this.statusInsurancePayment,
  });
}
