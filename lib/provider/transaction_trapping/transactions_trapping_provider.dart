import 'package:tcmobapp/provider/transaction_trapping/transactions_trapping_class.dart';

import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

// import 'savings_details_class.dart';
// import 'savings_details_transaction_class.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_own_destination_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_own_result_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_sources_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_transaction_class.dart';
import 'package:tcmobapp/provider/login/login_class.dart';
import 'package:localstorage/localstorage.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:tcmobapp/provider/user/user_details_class.dart';
import 'package:http/http.dart' as http;

class TransactionTrappingProvider with ChangeNotifier {
  final LocalStorage localStorage = new LocalStorage('mobile_app');
  TransactionTrappingClass _transactionStatus = new TransactionTrappingClass();

  TransactionTrappingClass get getTransactionStatus {
    return _transactionStatus;
  }

  /* void setTransactionStatus(TransactionTrappingClass data) {
    _transactionStatus = data;
  } */

  Future<void> checkTransactionStatus() async {
    _transactionStatus = new TransactionTrappingClass();

    try {
      final response = await http.get(
        kNewServerAPI + '/check-status-transaction',
        headers: {
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      var res_body = json.decode(response.body);
      var decrypted = json.decode(kDecryptData(res_body['crypted']));

      print("OBSA TAGGED STATUS");
      print(decrypted);
      print(decrypted['data']['branch_status']);
      print(decrypted['data']['transaction_status']['statusbit_fundtransfer']);

      if (decrypted['status'].toString() == 'success') {
        _transactionStatus.isBranchActive = decrypted['data']['branch_status'];
        _transactionStatus.isOBSATagged =
            decrypted['data']['obsa_tagged_status'];
        _transactionStatus.statusBillsPayment =
            decrypted['data']['transaction_status']['statusbit_billspayment'];
        _transactionStatus.statusFundTransfer =
            decrypted['data']['transaction_status']['statusbit_fundtransfer'];
        _transactionStatus.statusFundTransferBanks = decrypted['data']
            ['transaction_status']['statusbit_fundtransferbanks'];
        _transactionStatus.statusFundTransferOther = decrypted['data']
            ['transaction_status']['statusbit_fundtransferinter'];
        _transactionStatus.statusFundTransferOwn = decrypted['data']
            ['transaction_status']['statusbit_fundtransferintra'];
        _transactionStatus.statusInsurancePayment = decrypted['data']
            ['transaction_status']['statusbit_paymentsinsurances'];
        _transactionStatus.statusLoanPayment =
            decrypted['data']['transaction_status']['statusbit_paymentsloans'];
        _transactionStatus.statusPayments =
            decrypted['data']['transaction_status']['statusbit_payments'];
      } else {
        _transactionStatus.isBranchActive = false;
        _transactionStatus.isOBSATagged = false;
        _transactionStatus.statusBillsPayment = false;
        _transactionStatus.statusFundTransfer = false;
        _transactionStatus.statusFundTransferBanks = false;
        _transactionStatus.statusFundTransferOther = false;
        _transactionStatus.statusFundTransferOwn = false;
        _transactionStatus.statusInsurancePayment = false;
        _transactionStatus.statusLoanPayment = false;
        _transactionStatus.statusPayments = false;
      }

      /* var destinations = decrypted['data']['destinations'];

      print(destinations);

      destinations.forEach((item) {
        print("Product");
        print(item['ProductName'].toString());

        FundTransferOwnDestinationClass modelToAdd =
            new FundTransferOwnDestinationClass();

        modelToAdd.accountNumber = item['AccountNumber'].toString();
        modelToAdd.balance = double.parse(item['Balance'].toString());
        modelToAdd.customerID = item['fkcustomeridaccount'].toString();
        modelToAdd.holdBalance = double.parse(item['HoldBalance'].toString());
        modelToAdd.productID = item['ProductID'].toString();
        modelToAdd.productName = item['ProductName'].toString();
        modelToAdd.slCode = item['SLCode'].toString();
        modelToAdd.customerID = item['fkcustomeridaccount'].toString();

        if (modelToAdd.productID == 'SC') {
          modelToAdd.shareCapLimit =
              double.parse(item['ShareCapLimit'].toString());
        } else {
          modelToAdd.shareCapLimit = 0;
        }

        ownDestinationList.add(modelToAdd);
      }); */
      notifyListeners();
    } catch (e) {
      print("ERROR BRANCH STATUS: $e");
      throw new Exception("Error Fetching Branch/Transaction Status Data!");
    }
  }
}
