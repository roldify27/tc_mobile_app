import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

// import 'savings_details_class.dart';
// import 'savings_details_transaction_class.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/login/login_class.dart';
import 'package:localstorage/localstorage.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:tcmobapp/provider/user/user_details_class.dart';
import 'package:http/http.dart' as http;

class LoginProvider with ChangeNotifier {
  final LocalStorage localStorage = new LocalStorage('mobile_app');
  // var loginProvider = Provider.of<UserDetailsProvider>(context, listen: false);

  Future<String> checkAuthenticateUser(LoginClass userLoginDetails) async {
    print("${userLoginDetails.userName} : ${userLoginDetails.passWord}");

    try {
      // String encrypted_password =
      //     await kEncryptPassword(userLoginDetails.passWord);
      // String encrypted_password = userLoginDetails.passWord;

      String encrypted_password =
          await kEncryptPasswordMD5(userLoginDetails.passWord);

      /* kEncryptData(json.encode({
        "credentials": {
          "username": userLoginDetails.userName,
          "password": encrypted_password,
        },
      })); */

      kDecryptData(
          "keJTwkNNucMRS68gEbMznXLQRDma7Gxvj9XAK/iOnmEv8t+/C7pR5a88Cc9Xi4yQVD3CDsyKfIleuNWmxTSDNO0O6G6OZCs0OlTKNuCbBM/oFyhrJZPktHbeGKB5Q3mD");

      print(encrypted_password);

      final response = await http.post(
        kNewServerAPI + '/login-user',
        headers: {HttpHeaders.contentTypeHeader: 'application/json'},
        body: json.encode({
          "credentials": {
            "username": userLoginDetails.userName,
            "password": encrypted_password,
          },
        }),
      );

      print("RESULT_LOGIN: ${response.body}");

      if (response.body != 'Incorrect Credentials') {
        await localStorage.setItem('jwtauth', response.body);

        Map<String, dynamic> payload = Jwt.parseJwt(response.body);

        print(payload);
        print("Email: ${payload['email']}");
      }

      print("JWT Auth Local: ${localStorage.getItem('jwtauth')}");
      /* var res_body = json.decode(response.body);
      print("RESULT_LOGIN: ${res_body}"); */
      // List l = json.decode(res_body['data']);

      // print('List: ${l}');

      // List.from(l.map((model) {
      //   print(model['PassbookNo'].toString());
      //   // print(
      //   //     'Converted Date: ${DateTime.parse(model['OpeningDate'].toString())}');

      //   final modelToAdd = SavingsDetailsClass(
      //     accountTitle: model['ProductName'].toString(),
      //     accountNumber: model['AccountNumber'].toString(),
      //     accountHolder: model['fkcustomeridaccount'].toString(),
      //     availableBalance: double.parse(model['Balance']),
      //     openedDate: DateTime.parse(model['OpeningDate'].toString()),
      //     isActive: (model['AccountStatus'] == '1') ? true : false,
      //     cardOpacity: 0.65,
      //     passbookNo: (["", null].contains(model['PassbookNo'].toString()))
      //         ? 'No Passbook'
      //         : model['PassbookNo'].toString(),
      //   );

      //   // print('model: ${modelToAdd}');

      //   _savingsDetails.add(modelToAdd);
      // }));

      // fetchDataTransactions();

      // print('Savings: ${_savingsDetails}');
      // setIsLoading();
      notifyListeners();

      return response.body;
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    }
  }
}
