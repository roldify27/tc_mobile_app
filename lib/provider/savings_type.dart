enum SavingsCode {
  Savings,
  Share,
  TimeDep,
}

class SavingsType {
  final String savingsProductName;
  final SavingsCode savingsCode;
  final double
      savingsAmount; //should not be done. for display purposes only. we cannot maximize from this abomination.
  SavingsType({this.savingsProductName, this.savingsCode, this.savingsAmount});
}

class SavingsList {
  List<SavingsType> _savingsType = [
    SavingsType(
      savingsCode: SavingsCode.Share,
      savingsProductName: 'Share Cap',
      savingsAmount: 3000.25,
    ),
    SavingsType(
      savingsCode: SavingsCode.Savings,
      savingsProductName: 'Regular Savings',
      savingsAmount: 800.25,
    ),
    SavingsType(
      savingsCode: SavingsCode.Savings,
      savingsProductName: 'Wedding Savings',
      savingsAmount: 200,
    ),
    SavingsType(
      savingsCode: SavingsCode.Savings,
      savingsProductName: 'Drive Savings',
      savingsAmount: 200,
    ),
    SavingsType(
      savingsCode: SavingsCode.Savings,
      savingsProductName: 'GASAKA',
      savingsAmount: 0,
    ),
    SavingsType(
      savingsCode: SavingsCode.Savings,
      savingsProductName: 'Honey Savings',
      savingsAmount: 0,
    ),
    SavingsType(
      savingsCode: SavingsCode.Savings,
      savingsProductName: 'Vacation Savings',
      savingsAmount: 0,
    ),
    SavingsType(
      savingsCode: SavingsCode.TimeDep,
      savingsProductName: 'Triple Tubo',
      savingsAmount: 50000000,
    ),
  ];

  List<SavingsType> get savingsType {
    return [...this._savingsType];
  }
}
