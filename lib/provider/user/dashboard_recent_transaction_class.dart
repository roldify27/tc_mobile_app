import 'dart:typed_data';

import 'package:flutter/foundation.dart';

class DashboardRecentTransactionClass with ChangeNotifier {
  String title;
  String category;
  String balanceLocator;
  DateTime trandate;
  DateTime dateGroup;
  num amount;

  DashboardRecentTransactionClass({
    this.title,
    this.category,
    this.balanceLocator,
    this.trandate,
    this.dateGroup,
    this.amount,
  });
}
