import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// import 'savings_details_class.dart';
// import 'savings_details_transaction_class.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/login/login_class.dart';
import 'package:localstorage/localstorage.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:tcmobapp/provider/savings_details/grouped_transactions_class.dart';
import 'package:tcmobapp/provider/user/dashboard_recent_transaction_class.dart';
import 'package:tcmobapp/provider/user/user_details_class.dart';
import 'package:http/http.dart' as http;

class UserDetailsProvider with ChangeNotifier {
  final LocalStorage localStorage = new LocalStorage('mobile_app');
  UserDetailsClass userDetails = new UserDetailsClass(
    cid: "",
    email: "",
    fname: "",
    isAdmin: false,
    memberClass: "",
    shareTotal: 0.0,
    savingsTotal: 0.0,
    timeDepoTotal: 0.0,
    loanTotal: 0.0,
    totalPoints: 0.0,
    username: "",
    mobilenumber: "",
    transactionName: "",
    trackNumber: "",
  );

  void setUserDetails(
    String cid,
    String username,
    String email,
    bool isAdmin,
    String fname,
    double totalPoints,
    String memberClass,
    double shareTotal,
    double savingsTotal,
    double timeDepoTotal,
    double loanTotal,
    String photo,
    String mobileNumber,
    String transactionName,
    String trackNumber,
  ) {
    if (!["", null, false, 0].contains(cid)) {
      userDetails.cid = cid;
    }

    if (!["", null, false, 0].contains(username)) {
      userDetails.username = username;
    }

    if (!["", null, false, 0].contains(email)) {
      userDetails.email = email;
    }

    if (!["", null, false, 0].contains(isAdmin)) {
      userDetails.isAdmin = isAdmin;
    }

    if (!["", null, false, 0].contains(fname)) {
      userDetails.fname = fname;
    }

    if (!["", null, false, 0].contains(totalPoints)) {
      userDetails.totalPoints = totalPoints;
    }

    if (!["", null, false, 0].contains(memberClass)) {
      userDetails.memberClass = memberClass;
    }

    if (!["", null, false, 0].contains(shareTotal)) {
      userDetails.shareTotal = shareTotal;
    } else {
      userDetails.shareTotal = 0;
    }

    if (!["", null, false, 0].contains(savingsTotal)) {
      userDetails.savingsTotal = savingsTotal;
    } else {
      userDetails.savingsTotal = 0;
    }

    if (!["", null, false, 0].contains(timeDepoTotal)) {
      userDetails.timeDepoTotal = timeDepoTotal;
    } else {
      userDetails.timeDepoTotal = 0;
    }

    if (!["", null, false, 0].contains(loanTotal)) {
      userDetails.loanTotal = loanTotal;
    } else {
      userDetails.loanTotal = 0;
    }

    if (!["", null, false, 0].contains(photo)) {
      userDetails.photo = base64Decode(photo);
    }

    if (!["", null, false, 0].contains(mobileNumber)) {
      userDetails.mobilenumber = mobileNumber;
    }

    if (!["", null, false, 0].contains(transactionName)) {
      userDetails.transactionName = transactionName;
    }

    if (!["", null, false, 0].contains(trackNumber)) {
      userDetails.trackNumber = trackNumber;
    }

    kUserDetails = userDetails;
  }

  UserDetailsClass get getUserDetails {
    return userDetails;
  }

  Future<String> getUserDetailsAPI() async {
    // print(kNewServerAPI + '/get-one-member-data');
    // print(localStorage.getItem('jwtauth'));

    try {
      final response = await http.get(
        kNewServerAPI + '/get-one-member-data',
        headers: {
          // HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      // print("RESULT_USER_DETAILS: ${response.body}");
      var result = json.decode(response.body);

      setUserDetails(
        result['memberinfo']['CustomerID'],
        result['useraccountinfo']['username'],
        result['useraccountinfo']['email'],
        result['useraccountinfo']['is_admin'],
        result['useraccountinfo']['name'],
        result['rewards']['totalpoints'].toDouble(),
        result['memberclass']['classresult'],
        result['sharetotal']['total'].toDouble(),
        result['savingstotal']['total'].toDouble(),
        result['timedepototal']['total'].toDouble(),
        result['loantotal']['total'].toDouble(),
        result['photo']['ClientPictureString'],
        result['memberinfo']['PhoneNumber'],
        result['memberinfo']['FirstName'].toString() +
            ' ' +
            result['memberinfo']['LastName'].toString(),
        result['tracknumber']['CustomerID'].toString(),
      );

      // print("PHOTO: ${result['photo']['ClientPictureString']}");
      // print(getUserDetails);

      notifyListeners();

      return response.body;
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    }
  }

  // ============================= START - RECENT TRANSACTIONS =============================

  List<DashboardRecentTransactionClass> recentTransactionList = [];
  List<GroupedTransactions> recentTransGrouped = [];

  List<GroupedTransactions> get getGroupedTransactions {
    return [...recentTransGrouped];
  }

  List<DashboardRecentTransactionClass> get getRecentTransactions {
    return [...recentTransactionList];
  }

  Future<List<DashboardRecentTransactionClass>>
      getDashboardRecentTransactions() async {
    // print(kNewServerAPI + '/get-one-member-data');
    // print(localStorage.getItem('jwtauth'));
    recentTransactionList = [];
    recentTransGrouped = [];

    try {
      final response = await http.get(
        kNewServerAPI + '/get-dashboard-recent-transactions',
        headers: {
          // HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      // print("RESULT_USER_DASH_TRANSACTIONS: ${response.body}");

      var result = json.decode(response.body);
      print("RESULT_USER_DASH_TRANSACTIONS: ${result['data']['transactions']}");
      var transactions = result['data']['transactions'];

      List.from(transactions.map((model) {
        final modelToAddTransactions = DashboardRecentTransactionClass(
          title: model['BalanceLocator'].toString() +
              " " +
              model['TransferType'].toString(),
          amount: double.parse(model['AmountTransferred'].toString()),
          balanceLocator: model['BalanceLocator'].toString(),
          trandate: DateTime.parse(
            model['TransactionTime'].toString(),
          ),
          category: model['Category'],
          dateGroup: DateTime.parse(
              kFormatter.format(DateTime.parse(model['TransactionTime']))),
        );

        // print(formatter.format(DateTime.parse(model['TransactionTime'])));
        // print('model: ${modelToAdd}');

        recentTransactionList.add(modelToAddTransactions);

        print("TTILE: ${modelToAddTransactions.title}");
        print("CATEGORY: ${modelToAddTransactions.category}");
        print("BALLOC: ${modelToAddTransactions.balanceLocator}");
        print("AMOUNT: ${modelToAddTransactions.amount}");
        print("TRANDATE: ${modelToAddTransactions.trandate}");
        print("DATEGROUP: ${modelToAddTransactions.dateGroup}");
      }));

      var newMap = groupBy(recentTransactionList, (obj) => obj.dateGroup);

      newMap.forEach((key, value) {
        recentTransGrouped.add(GroupedTransactions(
          date: DateTime.parse(key.toString()),
          transactions: value,
        ));
      });

      print(recentTransGrouped[1]);

      notifyListeners();
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    }
  }

  Future<List<DashboardRecentTransactionClass>>
      getDashboardRecentTransactionsFiltered(int filter) async {
    // print(kNewServerAPI + '/get-one-member-data');
    // print(localStorage.getItem('jwtauth'));
    recentTransactionList = [];
    recentTransGrouped = [];
    String queryFilter = "";

    if (filter == 2) {
      queryFilter = " AND TransactionCode = 'FUNDTRANSFER'";
    } else if (filter == 3) {
      queryFilter =
          " AND TransactionCode = 'BILLSPAYMENT' OR TRANSACTIONCODE = 'PAYMENTS'";
    } else if (filter == 4) {
      queryFilter = " AND TransactionCode = 'CASHIN'";
    }

    try {
      final response = await http.get(
        kNewServerAPI +
            '/get-dashboard-recent-transactions-filtered/${queryFilter}',
        headers: {
          // HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
      );

      // print("RESULT_USER_DASH_TRANSACTIONS: ${response.body}");

      var result = json.decode(response.body);
      print("RESULT_USER_DASH_TRANSACTIONS: ${result['data']['transactions']}");
      var transactions = result['data']['transactions'];

      List.from(transactions.map((model) {
        final modelToAddTransactions = DashboardRecentTransactionClass(
          title: model['BalanceLocator'].toString() +
              " " +
              model['TransferType'].toString(),
          amount: double.parse(model['AmountTransferred'].toString()),
          balanceLocator: model['BalanceLocator'].toString(),
          trandate: DateTime.parse(
            model['TransactionTime'].toString(),
          ),
          category: model['Category'],
          dateGroup: DateTime.parse(
              kFormatter.format(DateTime.parse(model['TransactionTime']))),
        );

        // print(formatter.format(DateTime.parse(model['TransactionTime'])));
        // print('model: ${modelToAdd}');

        recentTransactionList.add(modelToAddTransactions);

        print("TTILE: ${modelToAddTransactions.title}");
        print("CATEGORY: ${modelToAddTransactions.category}");
        print("BALLOC: ${modelToAddTransactions.balanceLocator}");
        print("AMOUNT: ${modelToAddTransactions.amount}");
        print("TRANDATE: ${modelToAddTransactions.trandate}");
        print("DATEGROUP: ${modelToAddTransactions.dateGroup}");
      }));

      var newMap = groupBy(recentTransactionList, (obj) => obj.dateGroup);

      newMap.forEach((key, value) {
        recentTransGrouped.add(GroupedTransactions(
          date: DateTime.parse(key.toString()),
          transactions: value,
        ));
      });

      print(recentTransGrouped);

      notifyListeners();
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    }
  }

  /* List<DashboardRecentTransactionClass> recentTransactions = [];

  Future<void> fetchDataTransactions(String accountNumber) async {
    recentTransactions = [];

    try {
      // print(isLoadingTransactions);
      /* if (_selectedSavingsTransactions.length != 0) {
        _selectedSavingsTransactions = [];
      } */

      print("NIAGI DEREEEEEE");

      final response = await http.post(
        kNewServerAPI + '/newmobapp_getdepotransis_v08172020',
        headers: {
          HttpHeaders.authorizationHeader: localStorage.getItem('jwtauth')
        },
        body: json.encode({
          'cid': '018-0000420',
          'accntno': accountNumber,
          'access': 'W5o4V3q2y1'
        }),
      );

      var res_body = json.decode(response.body);
      List l = json.decode(res_body['data']);

      // print('List: ${l}');
      final DateFormat formatter = DateFormat('yyyy-MM-dd');

      List.from(l.map((model) {
        final modelToAddTransactions = SavingsDetailsTransactionClass(
          amount: double.parse(model['Amount'].toString()),
          runningBalance: double.parse(model['RunningBalance'].toString()),
          referenceNo: model['ReferenceNo'].toString(),
          balanceLocator:
              (model['BalanceLocator'] == '1') ? 'Deposit' : 'Withdrawal',
          transactionTime: DateTime.parse(
            model['TransactionTime'].toString(),
          ),
          dateGroup: DateTime.parse(
              formatter.format(DateTime.parse(model['TransactionTime']))),
        );

        // print(formatter.format(DateTime.parse(model['TransactionTime'])));
        // print('model: ${modelToAdd}');

        _selectedSavingsTransactions.add(modelToAddTransactions);
      }));

      // print('Savings: ${_savingsDetails}');
      // setIsLoading();
      notifyListeners();
    } catch (error) {
      // setIsLoading();
      print(error);
      throw error;
    } finally {
      // setIsLoadingTransactions();
    }
  }

  List<SavingsDetailsTransactionClass> get transactions {
    return [..._selectedSavingsTransactions];
  } */

  // ============================= END - RECENT TRANSACTIONS =============================
}
