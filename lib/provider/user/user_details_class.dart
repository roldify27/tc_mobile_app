import 'dart:typed_data';

import 'package:flutter/foundation.dart';

class UserDetailsClass with ChangeNotifier {
  String cid;
  String username;
  String email;
  bool isAdmin;
  String fname;
  double totalPoints;
  String memberClass;
  double shareTotal;
  double savingsTotal;
  double timeDepoTotal;
  double loanTotal;
  Uint8List photo;
  String mobilenumber;
  String transactionName;
  String trackNumber;

  UserDetailsClass({
    this.cid,
    this.username,
    this.email,
    this.isAdmin,
    this.fname,
    this.totalPoints,
    this.memberClass,
    this.shareTotal,
    this.savingsTotal,
    this.timeDepoTotal,
    this.loanTotal,
    this.photo,
    this.mobilenumber,
    this.transactionName,
    this.trackNumber,
  });
}
