/*
  Here we define the theming of our app. The swatches, fonts
  and everything.
*/

// OUTLINE ICON - 435EAB

// import 'dart:js';
import 'dart:async';
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:skeleton_text/skeleton_text.dart';
import 'package:tcmobapp/provider/user/user_details_class.dart';
import 'package:tcmobapp/screens/login_screen.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:crypto/crypto.dart';

const kServerAPI = "http://192.168.137.1/ictweb/index.php/edemand/tcmobile";
const kNewServerAPI = "http://192.168.23.30:3000/mobile-app/v1";

UserDetailsClass kUserDetails = new UserDetailsClass();

// =================== COLORS
MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  Map swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}

const kPrimaryColor = Color(0xFF00238A);
const kPrimaryColorFaded = Color(0xFF465787);
const kPrimaryColorFaded2 = Color(0xFFaab0bf);
const kSecondaryColor = Colors.red;
const kCancelColor = Colors.white54;
const kLogoutColor = Colors.grey;
const kEnabledFieldColor = Color(0xFFF2F2F2);
const kDisabledFieldColor = Color(0xFFc2c2c2);
const kFieldsHintTextColor = Color(0xFF757575);
const kPrimarySwatch = Color(0xFF032EAB);
const kDarkerFontColor = Color(0xFF000f3d);
const kDarkerFontColor2 = Color(0xFF4f4f4f);
const kDarkerFontColor3 = Color(0xFF00185c);
const kPrimarySwatchColor = Color(0xFF062d9e);
const kOffWhiteColor = Color(0xFFf7f7f7);
const kWhiteColor = Colors.white;
const kTransactionSubtitleColor = Colors.grey;
var kDashboardBackColor = Colors.white;
var kDialogModalBarrierColor = Colors.black.withOpacity(0.90);
final DateFormat kFormatter = DateFormat('yyyy-MM-dd');
List<BoxShadow> kShadowList = [
  BoxShadow(color: Colors.grey[300], blurRadius: 30, offset: Offset(0, 10))
];

var rng = new Random();
double randomBlend = rng.nextDouble();

// =================== FONT SIZE
const double kHeaderTitleFontSize = 30;
const double kDialogHeaderFontSize = 20;
const double kParagraphFontSize = 15;
const double kSubParagraphFontSize = 12;
const double kButtonCircularRadius = 35;
const double kReceiptAmountSize = 22;

// =================== WIDGETS
const kDivider = Divider(
  color: Colors.grey,
  // height: 20,
  // thickness: 5,
  indent: 25,
  endIndent: 30,
);

//======================= SCREEN SIZE =======================//

//======================= LAZY LOADERS =======================//
Container skeletonInstance({double paddingLeft}) {
  return Container(
    height: 20,
    child: SkeletonAnimation(
      shimmerColor: kPrimaryColor.withOpacity(0.25),
      borderRadius: BorderRadius.circular(20),
      shimmerDuration: 1500,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(20),
          boxShadow: kShadowList,
        ),
        margin: EdgeInsets.only(right: paddingLeft == null ? 0 : paddingLeft),
      ),
    ),
  );
}

Container kSkeletonInstance2(BuildContext context, double size) {
  return Container(
    height: 20,
    width: MediaQuery.of(context).size.width * size,
    margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
    child: SkeletonAnimation(
      shimmerColor: kPrimaryColor.withOpacity(0.25),
      borderRadius: BorderRadius.circular(20),
      shimmerDuration: 1500,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(20),
          boxShadow: kShadowList,
        ),
        margin: EdgeInsets.only(right: 0),
      ),
    ),
  );
}

Row kBuildRecentTransactionsDashboardLazyLoader() {
  return Row(
    children: [
      Container(
        margin: EdgeInsets.all(20),
        child: SizedBox(
          width: 50,
          height: 50,
          child: skeletonInstance(),
        ),
      ),
      Expanded(
        child: Container(
          padding: EdgeInsets.only(top: 5),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 5, right: 20),
                child: skeletonInstance(),
              ),
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 10, right: 20),
                child: skeletonInstance(),
              ),
            ],
          ),
        ),
      )
    ],
  );
}

Container skeletonInstanceRecentTransactions({double paddingLeft}) {
  return Container(
    height: 20,
    child: SkeletonAnimation(
      shimmerColor: kPrimaryColor.withOpacity(0.25),
      borderRadius: BorderRadius.circular(20),
      shimmerDuration: 1500,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(20),
          boxShadow: kShadowList,
        ),
        margin: EdgeInsets.only(right: paddingLeft == null ? 0 : paddingLeft),
      ),
    ),
  );
}

Row buildRecentTransactionsLazyLoader() {
  return Row(
    children: [
      Container(
        margin: EdgeInsets.all(20),
        child: SizedBox(
          width: 50,
          height: 50,
          child: skeletonInstance(),
        ),
      ),
      Expanded(
        child: Container(
          padding: EdgeInsets.only(top: 5),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 5, right: 20),
                child: skeletonInstance(),
              ),
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 10, right: 20),
                child: skeletonInstance(),
              ),
            ],
          ),
        ),
      )
    ],
  );
}

kFieldFocusChange(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}

inactivityDialog(BuildContext context) {
  AlertDialog _dialog() {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.all(32.0),
      title: Text(
        "SUPPORT INQUIRY",
        style: TextStyle(
            color: Color(0xFF00238A),
            fontSize: kDialogHeaderFontSize,
            fontWeight: FontWeight.bold,
            letterSpacing: 1),
        textAlign: TextAlign.center,
      ),
      content: Text(
        'If you have questions about something you can ask our support by clicking the button below.',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: kParagraphFontSize),
      ),
      actions: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.maxFinite,
                margin: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                child: ElevatedButton(
                  // onPressed: onStepContinue,
                  /* shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(kButtonCircularRadius),
                                                ), */ // optional, in order to add additional space around text if needed
                  style: ElevatedButton.styleFrom(
                    primary: kPrimaryColor,
                    padding: EdgeInsets.all(20),
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(kButtonCircularRadius),
                    ),
                  ),
                  child: Text(
                    'Continue'.toUpperCase(),
                    style: TextStyle(
                      color: kWhiteColor,
                      fontSize: 10,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                  ),
                ),
                /* RaisedButton(
                  padding:
                      EdgeInsets.only(left: 20, right: 20, top: 17, bottom: 17),
                  onPressed: () {
                    try {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          LoginScreen.routeName, (route) => false);
                    } catch (e) {
                      Navigator.of(context).pushNamed(LoginScreen.routeName);
                    }
                  },
                  color: Color(0xFF00238A),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(23.5),
                      side: BorderSide(color: Color(0xFF00238A))),
                  child: Text(
                    'Your have been inactive for too long!',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.5,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                  ),
                ), */
              ),
              Container(
                  // width: double.maxFinite,
                  margin:
                      EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                  child: TextButton(
                    child: Text(
                      'Cancel',
                      style: TextStyle(
                          color: Color(0xFF00238A),
                          fontSize: 15.5,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: () {
                      // print('Pressed');
                      Navigator.of(context).pop();
                    },
                  )) /* RaisedButton(
                  padding:
                      EdgeInsets.only(left: 20, right: 20, top: 17, bottom: 17),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  color: Colors.white54,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(23.5),
                      side: BorderSide(color: Colors.white54)),
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                        color: kPrimaryColor,
                        fontSize: 15.5,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                  ),
                ), 
              ), */
            ]),
      ],
    );
  }

  return showDialog(
    context: context,
    builder: (context) => _dialog(),
    // barrierDismissible: false,
  );
}

kErrorApiDialog(BuildContext context) {
  return showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
            title: Text('An error occured!'),
            content: Text('Something went wrong.'),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  // savingsProvider.setIsLoading();
                  Navigator.of(ctx).pop();
                },
                child: Text('Okay'),
              ),
            ],
          ));
}

double kReciprocal(double d) => 1 / d;

kButtonProgressIndicator() {
  return FittedBox(
    fit: BoxFit.contain,
    child: SpinKitThreeBounce(
      color: Colors.white,
      size: 15.0,
    ),
  );
}

String kOTPCodeGenerate() {
  int min = 10000; //min and max values act as your 6 digit range
  int max = 99999;
  var randomizer = new Random();
  var rNum = min + randomizer.nextInt(max - min);
  // print("OTPPPPPPPPPPP: ${rNum}");
  return rNum.toString();
}

final key = encrypt.Key.fromUtf8('+qRP6YeltI7OGQj0flyOyg==');
final iv = encrypt.IV.fromUtf8('nYNnH5b5Q+OpLZg=');

String kEncryptData(String data) {
  print(iv.toString());

  print("KEY: ${key.length}");

  final encrypter =
      encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

  final encrypted = encrypter.encrypt(data, iv: iv);
  // final decrypted = encrypter.decrypt(encrypted, iv: iv);

  // print(decrypted); // Lorem ipsum dolor sit amet, consectetur adipiscing elit
  print("Encrypted: $encrypted");
  print(encrypted.base64);

  return encrypted.base64;
}

String kDecryptData(String data) {
  // Uint8List encrypted = base64Decode(data);

  final encrypter =
      encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.cbc));

  final decrypted = encrypter.decrypt64(data, iv: iv);

  // print(decrypted);
  return decrypted;
}

String kEncryptJSONBody(String data) {
  var encryptedBody = json.encode({
    "crypted": data,
  });

  return encryptedBody;
}

String kEncryptPasswordMD5(String password) {
  var bytes = utf8.encode(password); // data being hashed

  var digest = md5.convert(bytes);

  print("Digest as bytes: ${digest.bytes}");
  print("Digest as hex string: $digest");

  return digest.toString();
}

bool isset(dynamic variable) {
  if (!["", null, false, 0].contains(variable)) {
    return true;
  } else {
    return false;
  }
}

BuildContext logoLoadingContext;

kShowLoadingLogoOnly(BuildContext context) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext ctx) {
      logoLoadingContext = ctx;

      return Dialog(
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: Center(
          child: Image.asset(
            "assets/images/tc-logo-looper.gif",
            height: 70.0,
            width: 70.0,
          ),
        ),
      );
    },
  );
}

kDiscardLogoLoadingOnly() {
  Navigator.of(logoLoadingContext).pop();
}

// =================== TIMER ===================

int kTimerDuration = 300;
int kStart = kTimerDuration;
String kTimerTransactionType = "";
Timer kTimer;
DateTime kCancelled = null;
DateTime kRestored = null;
String kRestoredCode = "";

int kGetTimerDifference() {
  Duration difference = kRestored.difference(kCancelled);

  return difference.inSeconds.abs();
}

void kResetOTPTimer() {
  kStart = kTimerDuration;
}

void kResetTransactionType() {
  kTimerTransactionType = "";
}

void kResetAfterSuccess() {
  kTimer.cancel();
  kResetOTPTimer();
  kResetTransactionType();
}

void kGetSetRestoredTimer() {
  int difference = kGetTimerDifference();
  print("Restored Diff: $difference");

  kStart = kStart - difference;

  if (kStart < 1) {
    kResetOTPTimer();
  }
}

// +++++++++++++++++++ TIMER +++++++++++++++++++
