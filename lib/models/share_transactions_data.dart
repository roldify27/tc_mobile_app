import 'package:tcmobapp/models/share_transaction.dart';

class ShareTransactionsData {
  List<ShareTransaction> _tranList = [
    ShareTransaction(
      amount: 2000,
      labelType: 'BATCM',
      totalBalance: 34000,
      transactionDate: DateTime.now(),
      tranNo: 1,
    ),
    ShareTransaction(
      amount: 2000,
      labelType: 'BATDM',
      totalBalance: 34000,
      transactionDate: DateTime.now(),
      tranNo: 2,
    ),
    ShareTransaction(
      amount: 2000,
      labelType: 'BATDM',
      totalBalance: 34000,
      transactionDate: DateTime.now(),
      tranNo: 3,
    ),
    ShareTransaction(
      amount: 2000,
      labelType: 'BATCM',
      totalBalance: 34000,
      transactionDate: DateTime.now(),
      tranNo: 4,
    ),
    ShareTransaction(
      amount: 2000,
      labelType: 'BATDM',
      totalBalance: 34000,
      transactionDate: DateTime.now(),
      tranNo: 5,
    ),
    ShareTransaction(
      amount: 2000,
      labelType: 'BATCM',
      totalBalance: 34000,
      transactionDate: DateTime.now(),
      tranNo: 6,
    ),
  ];

  List get tranList {
    return [..._tranList];
  }
}
