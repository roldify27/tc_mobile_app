import 'package:flutter/foundation.dart';

class ShareTransaction {
  final String labelType;
  final double amount;
  final double totalBalance;
  final DateTime transactionDate;
  final int tranNo;

  ShareTransaction(
      {this.labelType,
      this.amount,
      this.totalBalance,
      this.transactionDate,
      this.tranNo});
}
