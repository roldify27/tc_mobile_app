/* Notes
- Remove Unused Packages
- Categorized Imports 
- Put Commas' for Proper Indexing*/

// DART

// PACKAGES
import 'package:tcmobapp/provider/savings_details/savings_details_class.dart';

// CONSTANT
import '../../constants.dart';

// MODELS

// PROVIDERS

// SCREENS

// WIDGETS

class SavingsDetailsData {
  List<SavingsDetailsClass> _savingsDetailsList = [
    SavingsDetailsClass(
        accountTitle: 'Regular Savings',
        accountNumber: '001-1001-0000000014',
        accountHolder: 'Neil Cajulao',
        availableBalance: 1237.23,
        openedDate: DateTime.now(),
        isActive: true,
        cardOpacity: 0.55),
    SavingsDetailsClass(
        accountTitle: 'Share Capital',
        accountNumber: '001-4001-0000000018',
        accountHolder: 'Neil Cajulao',
        availableBalance: 120.00,
        openedDate: DateTime.now(),
        isActive: true,
        cardOpacity: 0.45),
    SavingsDetailsClass(
        accountTitle: 'Time Deposit',
        accountNumber: '001-2001-0001000011',
        accountHolder: 'Neil Cajulao',
        availableBalance: 18500.45,
        openedDate: DateTime.now(),
        isActive: false,
        cardOpacity: 0.70),
  ];

  List<SavingsDetailsClass> get getData {
    return [..._savingsDetailsList];
  }
}
