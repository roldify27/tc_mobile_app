import 'package:encrypt/encrypt.dart';
import 'dart:convert';
import 'dart:core';
import 'dart:developer' as developer;

encryptData(Map jsonObj) {
  final stringified = jsonEncode(jsonObj);
  final key = Key.fromUtf8("0123456789abcdef0123456789abcdef");
  final iv = IV.fromUtf8("abcdef9876543210");

  final encrypter = Encrypter(AES(key, mode: AESMode.cbc));

  final encrypted = encrypter.encrypt(stringified, iv: iv);
  // final decrypted = encrypter.decrypt(encrypted, iv: iv);

  // print('Encrypted: ${Uri.encodeComponent(encrypted.base64.toString())}');
  developer.log(encrypted.base64.toString());

  // return Uri.encodeComponent(encrypted.toString());
  return encrypted.toString();
}
