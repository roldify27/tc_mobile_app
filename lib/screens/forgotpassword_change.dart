import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/screens/dashboard.dart';
import 'package:tcmobapp/screens/login_screen.dart';
import 'package:tcmobapp/widgets/rounded_text_widget.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';

class ForgotPasswordChange extends StatefulWidget {
  static String routeName = '/ForgotPasswordChange';

  @override
  _ForgotPasswordChangeState createState() => _ForgotPasswordChangeState();
}

class _ForgotPasswordChangeState extends State<ForgotPasswordChange> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  FocusNode passwordFocusNode = new FocusNode();
  FocusNode reTypeFocusNode = new FocusNode();
  FocusNode codeFocusNode = new FocusNode();
  bool passwordVisible;
  bool retypeVisible;

  var _currentSelectedValue;
  var selectedDate;
  var birthdateController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    passwordVisible = false;
    retypeVisible = false;

    passwordFocusNode.addListener(() {
      setState(() {
        FocusScope.of(context).requestFocus(passwordFocusNode);
      });
      print('hiyaa' + passwordFocusNode.hasFocus.toString());
    });
    reTypeFocusNode.addListener(() {
      setState(() {
        FocusScope.of(context).requestFocus(reTypeFocusNode);
      });
      print('hiyaa' + passwordFocusNode.hasFocus.toString());
    });
    codeFocusNode.addListener(() {
      setState(() {
        FocusScope.of(context).requestFocus(codeFocusNode);
      });
      print('hiyaa' + codeFocusNode.hasFocus.toString());
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    passwordFocusNode.removeListener(() {});
    reTypeFocusNode.removeListener(() {});
    codeFocusNode.removeListener(() {});
    // The attachment will automatically be detached in dispose().
    passwordFocusNode.dispose();
    reTypeFocusNode.dispose();
    codeFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var devSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
          child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  // decoration: BoxDecoration(color: Colors.red),
                  // padding: EdgeInsets.only(left: 10),
                  child: IconButton(
                    icon: Icon(Icons.clear_rounded),
                    color: Colors.grey,
                    onPressed: () {
                      Navigator.of(context).popUntil((route) => route
                          .isFirst); //-->context here is this widget's position in the widget tree.
                    },
                  ),
                  alignment: Alignment.center,
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  alignment: Alignment.center,
                  // decoration: BoxDecoration(color: Colors.blue),
                  padding: EdgeInsets.only(top: 35),
                  child: Text(
                    'New Password',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: kHeaderTitleFontSize,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  // decoration: BoxDecoration(color: Colors.red),
                  // padding: EdgeInsets.only(left: 10),
                  child: IconButton(
                    icon: Icon(Icons.arrow_back_ios_rounded),
                    color: kPrimaryColor,
                    onPressed: () {
                      Navigator.of(context)
                          .pop(); //-->context here is this widget's position in the widget tree.
                    },
                  ),
                  alignment: Alignment.center,
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(
                top: 25,
                left: devSize.width * 0.10,
                right: devSize.width * 0.10),
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                      obscureText: !passwordVisible,
                      focusNode: passwordFocusNode,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Password',
                        labelStyle: TextStyle(
                            color: passwordFocusNode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                        suffixIcon: Visibility(
                          visible: passwordFocusNode.hasFocus,
                          child: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Color(0xFF00238A),
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                passwordVisible = !passwordVisible;
                              });
                            },
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.isEmpty || !value.contains('@')) {
                          return 'Invalid email!';
                        }
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                      obscureText: !retypeVisible,
                      focusNode: reTypeFocusNode,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Re-Type Password',
                        labelStyle: TextStyle(
                            color: reTypeFocusNode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                        suffixIcon: Visibility(
                          visible: reTypeFocusNode.hasFocus,
                          child: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              retypeVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Color(0xFF00238A),
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                retypeVisible = !retypeVisible;
                              });
                            },
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.isEmpty || !value.contains('@')) {
                          return 'Invalid email!';
                        }
                      },
                    ),
                  ),
                  Container(
                    child: TextFormField(
                      controller: birthdateController,
                      focusNode: codeFocusNode,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Code',
                        labelStyle: TextStyle(
                            color: codeFocusNode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value.isEmpty || !value.contains('@')) {
                          return 'Invalid email!';
                        }
                      },
                    ),
                  ),
                ],
              )),
            ),
          ),
          Expanded(child: Container()),
          Container(
            padding: EdgeInsets.only(
                top: 50,
                bottom: 40,
                left: devSize.width * 0.05,
                right: devSize.width * 0.05),
            width: double.infinity,
            child: RaisedButton(
              padding: EdgeInsets.all(23),
              onPressed: () {
                FocusScope.of(context).requestFocus(new FocusNode());
                showDialog(
                  context: context,
                  builder: (context) => AboutWidget(),
                  barrierDismissible: false,
                );
              },
              /* {
                Navigator.of(context).pushReplacementNamed(Dashboard.routeName);
              }, */
              color: Color(0xFF00238A),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(kButtonCircularRadius),
                  side: BorderSide(color: Color(0xFF00238A))),
              child: Text(
                'Change Password',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15.5,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1),
              ),
            ),
          ),
        ],
      )),
    );
  }
}

class AboutWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      title: Text("DONE!",
          style: TextStyle(
              color: Color(0xFF00238A),
              fontSize: 15.5,
              fontWeight: FontWeight.bold,
              letterSpacing: 1)),
      content: Text('Password succesfully changed!'),
      actions: [
        Container(
            padding: EdgeInsets.only(right: 10, bottom: 10),
            child: FlatButton(
                onPressed: () {
                  Navigator.of(context)
                      .pushReplacementNamed(LoginScreen.routeName);
                },
                child: Text("Proceed",
                    style: TextStyle(
                        color: Color(0xFF00238A),
                        fontSize: 15.5,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1))))
      ],
    );
  }
}
