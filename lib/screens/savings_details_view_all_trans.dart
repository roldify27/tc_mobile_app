/* Notes
- Remove Unused Packages
- Categorized Imports 
- Put Commas' for Proper Indexing*/

// DART
// import 'dart:js';
import 'dart:async';
import 'dart:math';

// PACKAGES
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:tcmobapp/provider/savings_details/grouped_transactions_class.dart';
import 'package:tcmobapp/provider/savings_details/savings_details_class.dart';
import 'package:tcmobapp/provider/savings_details/savings_details_transaction_class.dart';
import 'package:tcmobapp/widgets/prompts/prompt_dialogs.dart';
import 'package:ticketview/ticketview.dart';
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart'
    as WidgetConditional;

// PRESENTATIONS
import 'package:tcmobapp/presentations/tc_mobapp_icons_icons.dart';

// CONSTANT
import '../constants.dart';

// MODELS

// PROVIDERS
import 'package:tcmobapp/provider/savings_details/savings_details_provider.dart';

// SCREENS

// WIDGETS

class SavingsDetailsViewAllTransScreen extends StatefulWidget {
  static const routeName = '/SavingsDetailsViewAllTransScreen';
  final String accountNumberPassed;

  const SavingsDetailsViewAllTransScreen({
    Key key,
    this.accountNumberPassed,
  }) : super(key: key);

  @override
  _SavingsDetailsViewAllTransScreenState createState() =>
      _SavingsDetailsViewAllTransScreenState();
}

class _SavingsDetailsViewAllTransScreenState
    extends State<SavingsDetailsViewAllTransScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final int increment = 10;

  bool _isSnackbarActive = false;
  bool _isLoading = true;
  bool _isLoadingMore = false;
  bool _showEndOfTransactions = false;
  int _transactionRange = 0;
  int _totalRows = 0;
  int _totalTransactionCount = 0;

  SavingsDetailsClass selectedAccount = new SavingsDetailsClass();
  List<SavingsDetailsTransactionClass> selectedAccountTransactions = [];

  @override
  void initState() {
    super.initState();

    getTransactions(widget.accountNumberPassed);
    _isLoading = true;
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> getTransactions(String selectedAccountNumber) async {
    if (mounted) {
      var savingsDetailsProvider =
          await Provider.of<SavingsDetailsClassProvider>(context,
              listen: false);

      try {
        selectedAccount = await savingsDetailsProvider
            .findByAccountNumber(selectedAccountNumber);
        await savingsDetailsProvider
            .fetchAllTransactions(selectedAccount.accountNumber);
        selectedAccountTransactions =
            await savingsDetailsProvider.getAllTransactions;
        _transactionRange = selectedAccountTransactions.length;

        _totalRows = selectedAccountTransactions[0].totalRows;
        _totalTransactionCount =
            savingsDetailsProvider.getTotalTransactionCount;
      } catch (error) {
        await showDialog(
          context: _scaffoldKey.currentContext,
          builder: (ctx) => PromptAlertDialogError(
            title: "System Error!",
            subtitle:
                "There is something wrong happened within the server. Please try again.",
            okText: "Okay",
          ),
        );
        await Navigator.of(context).pop();
      } finally {
        // await Future.delayed(Duration(seconds: 5));
        setState(() {
          _isLoading = false;
          _showEndOfTransactions = true;
        });
      }
    }
  }

  Future<void> getMoreTransactions() async {
    setState(() {
      _isLoadingMore = true;
    });

    // await Future.delayed(Duration(seconds: 5));

    var savingsDetailsProvider =
        await Provider.of<SavingsDetailsClassProvider>(context, listen: false);

    try {
      await savingsDetailsProvider.fetchMoreTransactions(
          selectedAccount.accountNumber, _transactionRange);
      List<SavingsDetailsTransactionClass> _listToAppend =
          await savingsDetailsProvider.getMoreTransactions;
      selectedAccountTransactions.addAll(_listToAppend);
      _transactionRange = selectedAccountTransactions.length;

      await Future.delayed(Duration(seconds: 5));
    } catch (error) {
      await showDialog(
        context: _scaffoldKey.currentContext,
        builder: (ctx) => PromptAlertDialogError(
          title: "System Error!",
          subtitle:
              "There is something wrong happened within the server. Please try again.",
          okText: "Okay",
        ),
      );
    } finally {
      setState(() {
        _isLoadingMore = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final savingsDetailsProvider =
        Provider.of<SavingsDetailsClassProvider>(context, listen: false);

    final _items = selectedAccountTransactions;

    var newMap = groupBy(_items, (obj) => obj.dateGroup);

    print("newMaps: ${_items}");

    List<GroupedTransactions> newList = [];

    newMap.forEach((key, value) {
      newList.add(GroupedTransactions(
        date: DateTime.parse(key.toString()),
        transactions: value,
      ));
    });

    return WillPopScope(
      onWillPop: () async => false,
      child: SafeArea(
        child: Stack(children: [
          Image.asset(
            "assets/images/background-1.jpg",
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.black.withOpacity(0.55),
            colorBlendMode: BlendMode.darken,
            fit: BoxFit.cover,
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            decoration: BoxDecoration(
              color: kOffWhiteColor,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(30),
                topLeft: Radius.circular(30),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(2, 0), // changes position of shadow
                ),
              ],
            ),
            child: new Container(),
          ),
          Scaffold(
            key: _scaffoldKey,
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.transparent,
            body: SafeArea(
              child: LazyLoadScrollView(
                scrollOffset: 0,
                isLoading: (_isLoadingMore || _isLoading),
                onEndOfPage: () {
                  print(
                      "TOTAL ROWS & CURRENT RANGE: $_totalRows : $_transactionRange");
                  if (_transactionRange < _totalRows) {
                    getMoreTransactions();
                  }
                },
                child: ListView(physics: ClampingScrollPhysics(), children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                      color: kOffWhiteColor,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(30),
                        topLeft: Radius.circular(30),
                      ),
                    ),
                    child: Column(children: [
                      (_isLoading)
                          ? Container(
                              margin: EdgeInsets.only(
                                  top: 20, right: 20, left: 20, bottom: 20),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin:
                                        EdgeInsets.only(bottom: 30, left: 50),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          // alignment: Alignment.centerLeft,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.60,
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 5),
                                          child: skeletonInstance(),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 10),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.40,
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 5),
                                          child: skeletonInstance(),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    // alignment: Alignment.centerLeft,
                                    width: MediaQuery.of(context).size.width *
                                        0.20,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: skeletonInstance(),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    /* padding: EdgeInsets.symmetric(horizontal: 20), */
                                    decoration: BoxDecoration(
                                      color: kWhiteColor,
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 1,
                                          blurRadius: 1,
                                          offset: Offset(0,
                                              1), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      children: [
                                        // skeletonInstance(paddingLeft: 20),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    // alignment: Alignment.centerLeft,
                                    width: MediaQuery.of(context).size.width *
                                        0.20,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: skeletonInstance(),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 20),
                                    /* padding: EdgeInsets.symmetric(horizontal: 20), */
                                    decoration: BoxDecoration(
                                      color: kWhiteColor,
                                      borderRadius: BorderRadius.circular(20),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.1),
                                          spreadRadius: 1,
                                          blurRadius: 1,
                                          offset: Offset(0,
                                              1), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      children: [
                                        // skeletonInstance(paddingLeft: 20),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                        kBuildRecentTransactionsDashboardLazyLoader(),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Container(
                              margin: EdgeInsets.only(
                                  top: 20, right: 20, left: 20, bottom: 20),
                              child: Column(children: [
                                Row(
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        child: IconButton(
                                          icon: Icon(
                                            Icons.arrow_back_ios_rounded,
                                            size: 20,
                                          ),
                                          color: kPrimaryColor,
                                          onPressed: () {
                                            Navigator.of(context)
                                                .pop(); //-->context here is this widget's position in the widget tree.
                                          },
                                        ),
                                        alignment: Alignment.centerLeft,
                                      ),
                                    ),
                                    Expanded(
                                      flex: 6,
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                selectedAccount.accountNumber,
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold,
                                                    color: kDarkerFontColor3),
                                              ),
                                              Text(
                                                selectedAccount.accountTitle,
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    // fontWeight: FontWeight.bold,
                                                    color: kDarkerFontColor3),
                                              ),
                                            ]),
                                      ),
                                    ),
                                  ],
                                ),
                                Column(children: [
                                  Container(
                                    // margin: EdgeInsets.only(top: 15),
                                    child: buildGroupedTransactions(newList),
                                  ),
                                  if (_isLoading)
                                    Container(
                                      child: BuildFirstLoadingSkeleton(),
                                    ),
                                  if (_isLoadingMore)
                                    Container(
                                      child: BuildMoreLoadingSkeleton(),
                                    ),
                                  if (selectedAccountTransactions.length >=
                                          _totalTransactionCount &&
                                      _showEndOfTransactions)
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                        vertical: 20,
                                      ),
                                      child: Text(
                                        '~ End of transactions ~',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 12,
                                          color: kTransactionSubtitleColor,
                                        ),
                                      ),
                                    ),
                                ]),
                              ]),
                            ),
                    ]),
                  ),
                ]),
              ),
            ),
          ),
        ]),
      ),
    );
  }

  Container buildGroupedTransactions(
      List<GroupedTransactions> groupedTransactions) {
    return Container(
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: [
            for (var i in groupedTransactions)
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      (DateFormat.yMMMMd().format(i.date) ==
                              DateFormat.yMMMMd().format(DateTime.now()))
                          ? "Today"
                          : DateFormat.yMMMd().format(i.date).toString(),
                      style: TextStyle(
                          fontSize: 14,
                          color: kPrimaryColorFaded2,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    /* padding: EdgeInsets.symmetric(
                                                          horizontal: 20, vertical: 10), */
                    decoration: BoxDecoration(
                      color: kWhiteColor,
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.1),
                          spreadRadius: 1,
                          blurRadius: 1,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ],
                    ),
                    child:
                        /* Container(), */ Column(
                      children: i.transactions.map<Widget>(
                        (e) {
                          return Column(children: [
                            recentTransactions(
                              e.balanceLocator +
                                  " of ₱ " +
                                  NumberFormat.decimalPattern()
                                      .format(e.amount),
                              "Ref: " + e.referenceNo,
                              e.runningBalance,
                              (e.balanceLocator == "Deposit") ? "+" : "-",
                              DateFormat.yMMMd()
                                  .format(e.transactionTime)
                                  .toString(),
                            ),
                            Divider(
                              color: kOffWhiteColor,
                              height: 1,
                              thickness: 1,
                            ),
                          ]);
                        },
                      ).toList(),
                    ),
                  ),
                ],
              ),
          ],
        ));
  }
}

class BuildMoreLoadingSkeleton extends StatelessWidget {
  const BuildMoreLoadingSkeleton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // skeletonInstance(paddingLeft: 20),
        Container(
          // alignment: Alignment.centerLeft,\
          margin: EdgeInsets.only(top: 20),
          width: MediaQuery.of(context).size.width * 0.20,
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: skeletonInstance(),
        ),
        buildRecentTransactionsLazyLoader(),
        buildRecentTransactionsLazyLoader(),
        buildRecentTransactionsLazyLoader(),
      ],
    );
  }
}

class BuildFirstLoadingSkeleton extends StatelessWidget {
  const BuildFirstLoadingSkeleton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            // alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width * 0.20,
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: skeletonInstance(),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            /* padding: EdgeInsets.symmetric(horizontal: 20), */
            decoration: BoxDecoration(
              color: kWhiteColor,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.1),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
            ),
            child: Column(
              children: [
                // skeletonInstance(paddingLeft: 20),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            // alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width * 0.20,
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: skeletonInstance(),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            /* padding: EdgeInsets.symmetric(horizontal: 20), */
            decoration: BoxDecoration(
              color: kWhiteColor,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.1),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
            ),
            child: Column(
              children: [
                // skeletonInstance(paddingLeft: 20),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

Container recentTransactions(String transDetail, String transtType,
    double transAmount, String transSign, String transDate) {
  IconData iconAssetPath;
  String amount = transSign +
      ' ₱ ' +
      NumberFormat.decimalPattern().format(transAmount).toString();

  print(amount);

  /* if (transtType == 'Fund Transfer') {
    iconAssetPath = 'assets/images/trans-fund-transfer.png';
  } else if (transtType == 'Bills Payment') {
    iconAssetPath = 'assets/images/trans-bills-payment.png';
  } else if (transtType == 'Cash In') {
    iconAssetPath = 'assets/images/trans-cash-in.png';
  } else if (transtType == 'Deposit') {
    iconAssetPath = 'assets/images/trans-fund-transfer.png';
  } else if (transtType == 'Withdrawal') {
    iconAssetPath = 'assets/images/trans-cash-in.png';
  } */

  if (transSign == '-') {
    iconAssetPath = TcMobappIcons.withdrawal;
  } else if (transSign == '+') {
    iconAssetPath = TcMobappIcons.deposit;
  }

  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.only(right: 15),
          decoration: BoxDecoration(
            color: kOffWhiteColor,
            borderRadius: BorderRadius.circular(15),
          ),
          child: ClipRRect(
            child: Icon(
              iconAssetPath,
              size: 20,
              color: Colors.grey,
            ),
          ),
        ),
        Expanded(
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            dense: true,
            title: Text(transDetail, style: TextStyle(color: kPrimaryColor)),
            subtitle: Text(
              transtType,
              style: TextStyle(
                fontSize: 11.5,
                color: Colors.grey,
              ),
            ),
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    margin: EdgeInsets.only(bottom: 2),
                    child: Text(amount,
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: (transSign == '+')
                                ? Colors.green
                                : Colors.red))),
                Container(
                  margin: EdgeInsets.only(top: 2),
                  child: Text(
                    transDate,
                    style: TextStyle(
                      fontSize: 11.5,
                      color: Colors.grey,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

InkWell recentTransactions2(
    String transDetail,
    String transtType,
    double transAmount,
    String transSign,
    String transDate,
    BuildContext context) {
  String iconAssetPath = '';
  String amount = transSign +
      ' ₱ ' +
      NumberFormat.decimalPattern().format(transAmount).toString();

  print(amount);

  if (transtType == 'Fund Transfer') {
    iconAssetPath = 'assets/images/trans-fund-transfer.png';
  } else if (transtType == 'Bills Payment') {
    iconAssetPath = 'assets/images/trans-bills-payment.png';
  } else if (transtType == 'Cash In') {
    iconAssetPath = 'assets/images/trans-cash-in.png';
  }

  return InkWell(
    onTap: () => showDialog(
      context: context,
      builder: (context) => transactionDialog(context),
      barrierDismissible: false,
    ),
    child: Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(right: 15),
            decoration: BoxDecoration(
              color: kOffWhiteColor,
              borderRadius: BorderRadius.circular(15),
            ),
            child: ClipRRect(
              child: Image.asset(
                iconAssetPath,
                height: 23,
                width: 23,
              ),
            ),
          ),
          Expanded(
            child: ListTile(
              contentPadding: EdgeInsets.zero,
              dense: true,
              title: Text(transDetail, style: TextStyle(color: kPrimaryColor)),
              subtitle: Text(
                transtType,
                style: TextStyle(
                  fontSize: 11.5,
                  color: kPrimaryColorFaded,
                ),
              ),
              trailing: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.only(bottom: 2),
                      child: Text(amount,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: (transSign == '+')
                                  ? Colors.green
                                  : Colors.red))),
                  Container(
                    margin: EdgeInsets.only(top: 2),
                    child: Text(
                      transDate,
                      style: TextStyle(
                        fontSize: 11.5,
                        color: kPrimaryColorFaded,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

Dialog transactionDialog(BuildContext context) {
  double _screenWidth = MediaQuery.of(context).size.width;
  double _screenHeight = MediaQuery.of(context).size.height;

  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    child: Column(
      children: [
        Expanded(
          flex: 9,
          child: Container(
            // width: MediaQuery.of(context).size.width * 0.50,
            // margin: EdgeInsets.symmetric(horizontal: _screenWidth * 0.10),
            child: TicketView(
              backgroundPadding:
                  EdgeInsets.symmetric(vertical: 0, horizontal: 15),
              backgroundColor: kWhiteColor,
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
              drawArc: true,
              triangleAxis: Axis.vertical,
              borderRadius: 20,
              dividerColor: Colors.grey,
              drawDivider: true,
              trianglePos: .25,
              dividerStrokeWidth: 1,
              triangleSize: Size(25, 15),
              drawTriangle: false,
              drawShadow: false,
              drawBorder: true,
              corderRadius: 10,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                width: MediaQuery.of(context).size.width * 0.50,
                child: Column(children: [
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.only(top: 30),
                          child: CircleAvatar(
                            backgroundColor: Colors.transparent,
                            radius: 45,
                            child: ClipRRect(
                              borderRadius: BorderRadius.all(
                                Radius.circular(45),
                              ),
                              child: Icon(
                                Icons.check_circle_rounded,
                                color: Colors.greenAccent,
                                size: 60,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          // margin: EdgeInsets.only(top: 0),
                          child: Text(
                            "Deposit Transaction",
                            style: TextStyle(
                                color: Color(0xFF00238A),
                                fontSize: kDialogHeaderFontSize,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 1),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Text(
                          'Your transaction was succesful.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: kParagraphFontSize,
                            color: kPrimaryColorFaded2,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Container(
                      margin: EdgeInsets.only(
                        top: 20,
                      ),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Reference",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: kSubParagraphFontSize,
                                          color: kPrimaryColorFaded2,
                                        ),
                                      ),
                                      Text(
                                        "971",
                                        style: TextStyle(
                                            color: Color(0xFF00238A),
                                            fontSize: kParagraphFontSize,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                        'Transaction Code',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: kSubParagraphFontSize,
                                          color: kPrimaryColorFaded2,
                                        ),
                                      ),
                                      Text(
                                        "SVSC-DR",
                                        style: TextStyle(
                                            color: Color(0xFF00238A),
                                            fontSize: kParagraphFontSize,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Date',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: kSubParagraphFontSize,
                                          color: kPrimaryColorFaded2,
                                        ),
                                      ),
                                      Text(
                                        DateFormat.yMMMd()
                                            .format(DateTime.now())
                                            .toString(),
                                        style: TextStyle(
                                            color: Color(0xFF00238A),
                                            fontSize: kParagraphFontSize,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Text(
                                        'Time',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: kSubParagraphFontSize,
                                          color: kPrimaryColorFaded2,
                                        ),
                                      ),
                                      Text(
                                        DateFormat("hh:mm a")
                                            .format(DateTime.now())
                                            .toString(),
                                        style: TextStyle(
                                            color: Color(0xFF00238A),
                                            fontSize: kParagraphFontSize,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Amount',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: kSubParagraphFontSize,
                                          color: kPrimaryColorFaded2,
                                        ),
                                      ),
                                      Text(
                                        '₱' +
                                            NumberFormat.decimalPattern()
                                                .format(12435)
                                                .toString(),
                                        style: TextStyle(
                                            color: Color(0xFF00238A),
                                            fontSize: kReceiptAmountSize,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1),
                                        textAlign: TextAlign.center,
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                          vertical: 2,
                                          horizontal: 4,
                                        ),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: kPrimaryColorFaded2,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                        child: Text(
                                          'Completed'.toUpperCase(),
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontSize: kSubParagraphFontSize,
                                            color: kPrimaryColorFaded2,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ]),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: InkWell(
            onTap: () => Navigator.of(context).pop(),
            child: CircleAvatar(
              backgroundColor: Colors.black.withOpacity(0.20),
              radius: 20,
              child: Icon(
                Icons.close_rounded,
                color: kPrimaryColorFaded2,
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
