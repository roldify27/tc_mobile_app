/* Notes
- Remove Unused Packages
- Categorized Imports 
- Put Commas' for Proper Indexing*/

// DART
import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:convert'; // for json encoding decoding

// PACKAGES
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:provider/provider.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:skeleton_text/skeleton_text.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_other_destination_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_own_destination_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_own_result_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_provider.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_sources_class.dart';
import 'package:tcmobapp/provider/fund_transfer/fund_transfer_transaction_class.dart';
import 'package:tcmobapp/provider/savings_details/grouped_transactions_class.dart';
import 'package:tcmobapp/provider/timeout_session.dart';
import 'package:tcmobapp/provider/transaction_trapping/transactions_trapping_class.dart';
import 'package:tcmobapp/provider/transaction_trapping/transactions_trapping_provider.dart';
import 'package:tcmobapp/screens/dashboard.dart';
import 'package:tcmobapp/widgets/fund_transfer/fund_transfer_own_receipt.dart';
import 'package:tcmobapp/widgets/prompts/prompt_dialogs.dart';
import 'package:tcmobapp/widgets/savings_info.dart';
import 'package:tcmobapp/constants.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_conditional_rendering/flutter_conditional_rendering.dart'
    as WidgetConditional;
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:slide_to_confirm/slide_to_confirm.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:tcmobapp/widgets/svg_asset_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:animations/animations.dart';
import 'package:animate_do/animate_do.dart';

// CONSTANT
import '../constants.dart';

// MODELS
import '../provider/savings_details/savings_details_class.dart';
import '../models/savings_details/savings_details_data.dart';
import '../models/share_transactions_data.dart';

// PROVIDERS
import '../provider/savings_details/savings_details_provider.dart';
import '../provider/fund_transfer/fund_transfer_class.dart';
import 'package:tcmobapp/provider/idle_provider.dart';

// SCREENS

// WIDGETS
import '../widgets/savings_details/savings_details_transactions_tab_widget.dart';
import '../widgets/savings_details/savings_details_header_widget.dart';
import 'package:tcmobapp/widgets/savings_details/savings_details_scrollable_cards_widget.dart';
import 'package:tcmobapp/widgets/savings_details/savings_details_recent_transactions.dart';
import 'package:tcmobapp/widgets/fund_transfer/fund_transfer_header_widget.dart';
import 'package:tcmobapp/widgets/otp_code_verification_widget.dart';
import 'package:tcmobapp/widgets/fund_transfer/progress_indicator_widget.dart';
import 'package:tcmobapp/screens/dashboard.dart' as dashboardScreen;

import 'login_screen.dart';

// PRESENTATIONS
import 'package:tcmobapp/presentations/tc_mobapp_icons_icons.dart';

class FundTransfer extends StatefulWidget {
  static const routeName = '/FundTransfer';

  @override
  _FundTransferState createState() => _FundTransferState();
}

class _FundTransferState extends State<FundTransfer>
    with SingleTickerProviderStateMixin {
  final GlobalKey<FormState> _formKey = GlobalKey();

  TabController _tabBarController;

  int _currentStep = 0;
  double _currentSliderValue = 0;
  String _radioValue1 = "";
  String _transferType = "";
  String _ownTransferTo = null;
  FundTransferOwnDestinationClass _ownTransferToClass = null;
  int _totalTransactionCount = 0;

  bool _ownSelected = false;
  bool _otherSelected = false;
  bool _banksSelected = false;
  bool _isStepTwoClicked = false;
  bool _hasLoaded = false;
  bool _transactionIsLoadingFirst = false;
  bool _transactionIsLoadingRange = false;
  bool _showEndOfTransactions = false;

  TransactionTrappingClass traps = new TransactionTrappingClass();

  FundTransferClass _userFundTransferDetails = new FundTransferClass(
      otherAccountName: '',
      otherAccountNumber: '',
      transferToOwn: FundTransferOwnDestinationClass(
        accountNumber: '',
        productName: '',
        productID: '',
        balance: 0,
      ),
      transferToOther: FundTransferOtherDestinationClass(
        accountName: '',
        accountNumber: '',
      ),
      source: FundTransferSourcesClass(
          accountNumber: '',
          productName: '',
          productID: '',
          availableBalance: 0));
  List<FundTransferSourcesClass> _fundTransferSources = [];
  FundTransferSourcesClass _selectedSource = new FundTransferSourcesClass(
      accountNumber: "No Data!", productName: "No Data!", availableBalance: 0);

  List<FundTransferTransactionClass> _fundTransferTransactions = [];

  // =============================== FUND TRANSFER OWN ACCOUNT ===============================
  List<FundTransferOwnDestinationClass> _fundTransferOwnDestinationsUnfiltered =
      [];
  List<FundTransferOwnDestinationClass> _fundTransferOwnDestinations = [];
  List<FundTransferOwnDestinationClass> _filteredFundTransferOwnDestinations =
      [];
  List<String> _fundTransferOwnListSelections = [];
  FundTransferOwnResultClass result = new FundTransferOwnResultClass();

  // =============================== FUND TRANSFER OTHER ACCOUNT ===============================

  FundTransferOtherDestinationClass _fundTransferOtherDestinationDetails =
      new FundTransferOtherDestinationClass();

  var fundTransferOwnSelectionController = new TextEditingController();
  var amountController = new TextEditingController();
  var remarksController = new TextEditingController();

  var otherAccountNumberController = new TextEditingController();
  var otherAccountNameController = new TextEditingController();
  var otherAmountController = new TextEditingController();
  var otherRemarksController = new TextEditingController();

  var codeController = new TextEditingController();
  var beneAccountNumberController = new TextEditingController();
  var beneAccountNameController = new TextEditingController();
  var beneAddressController = new TextEditingController();

  FocusNode fundTransferSelection = new FocusNode();
  FocusNode amountFocusnode = new FocusNode();
  FocusNode remarksFocusnode = new FocusNode();

  FocusNode otherAccountNumberFocusnode = new FocusNode();
  FocusNode otherAccountNameFocusnode = new FocusNode();
  FocusNode otherAmountFocusnode = new FocusNode();
  FocusNode otherRemarksFocusnode = new FocusNode();

  FocusNode codeFocusnode = new FocusNode();
  FocusNode beneAccountNumberFocusnode = new FocusNode();
  FocusNode beneAccountNameFocusnode = new FocusNode();
  FocusNode beneAddressFocusnode = new FocusNode();

  /* FocusNode fundTransferSelection = new FocusNode();
  FocusNode amountFocusNode = new FocusNode();
  FocusNode remarksFocusNode = new FocusNode(); */

  bool hasFocus_FundTransferSelection = false;
  bool hasFocus_AmountFocusNode = false;
  bool hasFocus_RemarksFocusNode = false;

  bool hasFocus_OtherAccountNumberFocusnode = false;
  bool hasFocus_OtherAccountNameFocusnode = false;
  bool hasFocus_OtherAmountFocusnode = false;
  bool hasFocus_OtherRemarksFocusnode = false;

  @override
  void initState() {
    super.initState();

    _checkTransactionStatus();

    _ownTransferToClass = null;

    _tabBarController = new TabController(length: 2, vsync: this);

    _tabBarController.addListener(() {
      setState(() {
        if (_tabBarController.index == 1) {
          _fundTransferTransactions = [];
          getFirstTransactions();
        } else {
          _fundTransferTransactions = [];
          _showEndOfTransactions = false;
        }
      });
    });

    fundTransferSelection.addListener(() {
      setState(() {
        hasFocus_FundTransferSelection = fundTransferSelection.hasFocus;
      });
    });

    amountFocusnode.addListener(() {
      setState(() {
        hasFocus_AmountFocusNode = amountFocusnode.hasFocus;
      });
    });

    remarksFocusnode.addListener(() {
      setState(() {
        hasFocus_RemarksFocusNode = remarksFocusnode.hasFocus;
      });
    });

    otherAccountNumberFocusnode.addListener(() {
      setState(() {
        hasFocus_OtherAccountNumberFocusnode =
            otherAccountNumberFocusnode.hasFocus;
      });
    });

    otherAccountNameFocusnode.addListener(() {
      setState(() {
        hasFocus_OtherAccountNameFocusnode = otherAccountNameFocusnode.hasFocus;
      });
    });

    otherAmountFocusnode.addListener(() {
      setState(() {
        hasFocus_OtherAmountFocusnode = otherAmountFocusnode.hasFocus;
      });
    });

    otherRemarksFocusnode.addListener(() {
      setState(() {
        hasFocus_OtherRemarksFocusnode = otherRemarksFocusnode.hasFocus;
      });
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    fundTransferSelection.removeListener(() {});
    amountFocusnode.removeListener(() {});
    remarksFocusnode.removeListener(() {});
    otherAccountNameFocusnode.removeListener(() {});
    otherAccountNumberFocusnode.removeListener(() {});
    otherAmountFocusnode.removeListener(() {});
    otherRemarksFocusnode.removeListener(() {});

    fundTransferSelection.dispose();
    amountFocusnode.dispose();
    remarksFocusnode.dispose();
    otherAccountNameFocusnode.dispose();
    otherAccountNumberFocusnode.dispose();
    otherAmountFocusnode.dispose();
    otherRemarksFocusnode.dispose();

    super.dispose();
  }

  _clearHasFocus() {
    setState(() {
      hasFocus_FundTransferSelection = false;
      hasFocus_AmountFocusNode = false;
      hasFocus_RemarksFocusNode = false;

      hasFocus_OtherAccountNumberFocusnode = false;
      hasFocus_OtherAccountNameFocusnode = false;
      hasFocus_OtherAmountFocusnode = false;
      hasFocus_OtherRemarksFocusnode = false;
    });
  }

  _clearForm() {
    setState(() {
      _ownTransferTo = null;
      _ownTransferToClass = null;
      fundTransferOwnSelectionController.clear();
      amountController.clear();
      remarksController.clear();

      otherAccountNumberController.clear();
      otherAccountNameController.clear();
      otherAmountController.clear();
      otherRemarksController.clear();

      codeController.clear();
      beneAccountNumberController.clear();
      beneAccountNameController.clear();
      beneAddressController.clear();
      _clearHasFocus();
      WidgetsBinding.instance
          .addPostFrameCallback((_) => _formKey.currentState.reset());
    });
  }

  Future<void> _checkTransactionStatus() async {
    try {
      var _transactionTrappingProvider =
          await Provider.of<TransactionTrappingProvider>(context,
              listen: false);
      var _fundTransferProvider =
          await Provider.of<FundTransferProvider>(context, listen: false);

      await _transactionTrappingProvider.checkTransactionStatus();
      traps = _transactionTrappingProvider.getTransactionStatus;

      if (traps.isOBSATagged == false) {
        await showDialog(
          context: context,
          builder: (ctx) => PromptAlertDialogMissingOBSA(
            title: "You're Not Yet OBSA Tagged",
            subtitle:
                "You must first agree with our OBSA's (Online Banking Service Agreement) terms before you can transact with us. Please contact your nearest branch for this matter.",
            okText: "Okay",
            onOk: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
          ),
        );
      } else if (traps.isBranchActive == false ||
          traps.statusFundTransfer == false) {
        await showDialog(
          context: context,
          builder: (ctx) => PromptAlertDialogMaintenance(
            title: "Application Under Maintenance",
            subtitle:
                "Your branch is currently signed off or undergoing maintenance with its servers. We are sorry for the inconvenience. Please try again later.",
            okText: "Okay",
            onOk: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
          ),
        );
      } else {
        // print("goes here");
        await _fundTransferProvider.getSources();
        await _fundTransferProvider.fetchOwnDestinations();

        setState(() {
          _fundTransferSources = _fundTransferProvider.getSourcesList;
          _selectedSource = _fundTransferSources[0];

          _fundTransferOwnDestinations =
              _fundTransferProvider.getOwnDestinationList;
          _fundTransferOwnDestinationsUnfiltered = _fundTransferOwnDestinations;
          List<String> _stringOwnSelections = [];

          _fundTransferOwnDestinations.forEach((element) {
            _stringOwnSelections.add(element.productName);
          });
          _fundTransferOwnListSelections = _stringOwnSelections;
        });
      }

      /* var _fundTransferProvider =
          await Provider.of<FundTransferProvider>(context, listen: false);

      bool status = await _fundTransferProvider.checkBranchStatus();

      if (status == false) {
        await showDialog(
          context: context,
          builder: (ctx) => PromptAlertDialogMaintenance(
            title: "Application Under Maintenance",
            subtitle:
                "Your branch is currently signed off or undergoing maintenance with its servers. We are sorry for the inconvenience. Please try again later.",
            okText: "Okay",
            onOk: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
          ),
        );
        // await Navigator.of(context).pop();
      } else {
        // print("goes here");
        await _fundTransferProvider.getSources();
        await _fundTransferProvider.fetchOwnDestinations();

        setState(() {
          _fundTransferSources = _fundTransferProvider.getSourcesList;
          _selectedSource = _fundTransferSources[0];

          _fundTransferOwnDestinations =
              _fundTransferProvider.getOwnDestinationList;
          _fundTransferOwnDestinationsUnfiltered = _fundTransferOwnDestinations;
          List<String> _stringOwnSelections = [];

          _fundTransferOwnDestinations.forEach((element) {
            _stringOwnSelections.add(element.productName);
          });
          _fundTransferOwnListSelections = _stringOwnSelections;
        });
      } */
    } catch (e) {
      print(e);
      await showDialog(
        context: context,
        builder: (ctx) => PromptAlertDialogError(
          title: "System Error!",
          subtitle:
              "There is something wrong happened within the server. Please try again.",
          okText: "Okay",
        ),
      );
      await Navigator.of(context).pop();
    } finally {
      /* await Future.delayed(Duration(seconds: 10), () async {
        setState(() {
          _hasLoaded = true;
        });
      }); */
      setState(() {
        _hasLoaded = true;
      });
    }
  }

  Future<void> saveFundTransferDetails(FundTransferClass details) async {
    try {
      var _fundTransferProvider =
          await Provider.of<FundTransferProvider>(context, listen: false);

      await _fundTransferProvider.setFundTransferDetails(details);
    } catch (e) {
      print(e);
    } finally {}
  }

  Future<void> processFundTransferOwn(FundTransferClass details) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      barrierColor: kDialogModalBarrierColor,
      builder: (BuildContext context) {
        return ProgressIndicatorWidget();
      },
    );

    try {
      var _fundTransferProvider =
          await Provider.of<FundTransferProvider>(context, listen: false);

      await _fundTransferProvider.processFundTransferOwn(details);
      result = await _fundTransferProvider.getResultOwn;
      print("FUND TRANFER RESULT");
      print(result.reference);
      await Navigator.of(context).pop();

      if (result.status == 'success') {
        await showDialog(
          context: context,
          barrierDismissible: false,
          barrierColor: kDialogModalBarrierColor,
          builder: (BuildContext context) {
            return FundTransferOwnReceipt(
              result: result,
            );
          },
        );
      } else {
        await showDialog(
          context: context,
          builder: (ctx) => PromptAlertDialogError(
            title: "Transaction Error",
            subtitle:
                "An error occured while processing your transaction. Please try again.",
            okText: "Okay",
          ),
        );
      }
    } catch (e) {
      print(e);
      // await Navigator.of(context).pop();
      await showDialog(
        context: context,
        builder: (ctx) => PromptAlertDialogError(
          title: "System Error!",
          subtitle:
              "There is something wrong happened within the server. Please try again.",
          okText: "Okay",
        ),
      );
    } finally {}
  }

  Future<void> processFundTransferOther(FundTransferClass details) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      barrierColor: kDialogModalBarrierColor,
      builder: (BuildContext context) {
        return ProgressIndicatorWidget();
      },
    );

    try {
      var _fundTransferProvider =
          await Provider.of<FundTransferProvider>(context, listen: false);

      await _fundTransferProvider.processFundTransferOther(details);
      /* print("FUND TRANFER OTHER RESULT");
      print(result); */
      Navigator.of(context).pop();

      /*if (result.status == 'success') {
        await showDialog(
          context: context,
          barrierDismissible: false,
          barrierColor: kDialogModalBarrierColor,
          builder: (BuildContext context) {
            return FundTransferOwnReceipt(
              result: result,
            );
          },
        );
      } else {
        await showDialog(
          context: context,
          builder: (ctx) => PromptAlertDialogError(
            title: "Transaction Error",
            subtitle:
                "An error occured while processing your transaction. Please try again.",
            okText: "Okay",
          ),
        );
      } */
    } catch (e) {
      print(e);
      /* await Navigator.of(context).pop();
      await showDialog(
        context: context,
        builder: (ctx) => PromptAlertDialogError(
          title: "System Error!",
          subtitle:
              "There is something wrong happened within the server. Please try again.",
          okText: "Okay",
        ),
      ); */
    } finally {}
  }

  Future<void> getFirstTransactions() async {
    setState(() {
      _transactionIsLoadingFirst = true;
      _totalTransactionCount = 0;
    });

    var _fundTransferProvider =
        await Provider.of<FundTransferProvider>(context, listen: false);

    try {
      await _fundTransferProvider
          .fetchFundTransferTransactions(_fundTransferTransactions.length);

      _fundTransferTransactions
          .addAll(_fundTransferProvider.getFundTransferTransactionsRange);

      setState(() {
        _totalTransactionCount =
            _fundTransferProvider.getFundTransferTotalTransactionCount;
      });
    } catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => PromptAlertDialogError(
          title: "System Error!",
          subtitle:
              "There is something wrong happened within the server. Please try again.",
          okText: "Okay",
        ),
      );

      // _tabBarController.animateTo(0);
    } finally {
      /* Future.delayed(Duration(seconds: 5), () {
        setState(() {
          _transactionIsLoadingFirst = false;
        });
      }); */
      setState(() {
        _transactionIsLoadingFirst = false;
        _showEndOfTransactions = true;
      });
    }
  }

  Future<void> getMoreTransactions() async {
    setState(() {
      _transactionIsLoadingRange = true;
    });

    var _fundTransferProvider =
        await Provider.of<FundTransferProvider>(context, listen: false);

    try {
      await _fundTransferProvider
          .fetchFundTransferTransactions(_fundTransferTransactions.length);

      _fundTransferTransactions
          .addAll(_fundTransferProvider.getFundTransferTransactionsRange);
    } catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => PromptAlertDialogError(
          title: "System Error!",
          subtitle:
              "There is something wrong happened within the server. Please try again.",
          okText: "Okay",
        ),
      );

      // _tabBarController.animateTo(0);
    } finally {
      /* Future.delayed(Duration(seconds: 5), () {
        setState(() {
          _transactionIsLoadingFirst = false;
        });
      }); */
      setState(() {
        _transactionIsLoadingRange = false;
      });
    }
  }

  /* FutureBuilder(
              initialData: false,
              future: _hasLoaded,
              builder: (context, snapshot) {
                print("Snapshot: ${snapshot.hasData}");

                return (Stack(
                  clipBehavior: Clip.antiAliasWithSaveLayer,
                  children: [
                    _buildWidget(snapshot.hasData),
                    Visibility(
                      visible: (snapshot.data != true) ? true : false,
                      child: _buildLoading(snapshot.hasData),
                    ),
                  ],
                ));
              }), */

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    // final Map<String, Object> args = ModalRoute.of(context).settings.arguments;

    var _items = _fundTransferTransactions;

    var newMap = groupBy(_items, (obj) => obj.dateGroup);

    print("newMaps: ${_items}");

    List<GroupedTransactions> newList = [];

    newMap.forEach((key, value) {
      newList.add(GroupedTransactions(
        date: DateTime.parse(key.toString()),
        transactions: value,
      ));
    });

    return new WillPopScope(
      onWillPop: () async => false,
      child: Stack(children: [
        Image.asset(
          "assets/images/background-1.jpg",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.black.withOpacity(0.55),
          colorBlendMode: BlendMode.darken,
          fit: BoxFit.cover,
        ),
        Scaffold(
          // key: _scaffoldKey,
          resizeToAvoidBottomInset: true,
          backgroundColor: Colors.transparent,
          body: DefaultTabController(
            length: 2,
            child: SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(top: 20),
                        decoration: BoxDecoration(
                          color: kOffWhiteColor,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30),
                            topLeft: Radius.circular(30),
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.2),
                              spreadRadius: 5,
                              blurRadius: 7,
                              offset:
                                  Offset(2, 0), // changes position of shadow
                            ),
                          ],
                        ),
                        child: (_hasLoaded)
                            ? buildWidget(context, newList)
                            : buildLoading(context)),
                  ),
                ],
              ),
            ),
          ),
        ),
      ]),
    );
  }

  Container buildLoading(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20, right: 20, left: 20, bottom: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        // crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.symmetric(vertical: 20),
            width: MediaQuery.of(context).size.width * 0.40,
            child: skeletonInstance(),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.70,
            height: 50,
            margin: EdgeInsets.symmetric(vertical: 20),
            child: SkeletonAnimation(
              shimmerColor: kPrimaryColor.withOpacity(0.25),
              borderRadius: BorderRadius.circular(30),
              shimmerDuration: 1500,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(30),
                  boxShadow: kShadowList,
                ),
                // margin: EdgeInsets.only(right: paddingLeft == null ? 0 : paddingLeft),
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.symmetric(vertical: 20),
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: skeletonInstance(),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  width: MediaQuery.of(context).size.width * 0.50,
                  child: skeletonInstance(),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 50),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  // alignment: Alignment.center,
                  // margin: EdgeInsets.symmetric(horizontal: 50),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: kWhiteColor,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.1),
                        spreadRadius: 1,
                        blurRadius: 1,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      kSkeletonInstance2(context, 0.8),
                      kSkeletonInstance2(context, 0.5),
                      kSkeletonInstance2(context, 0.9),
                      kSkeletonInstance2(context, 0.6),
                      kSkeletonInstance2(context, 0.4),
                    ],
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.20,
                      height: 35,
                      margin: EdgeInsets.symmetric(vertical: 20),
                      child: SkeletonAnimation(
                        shimmerColor: kPrimaryColor.withOpacity(0.25),
                        borderRadius: BorderRadius.circular(30),
                        shimmerDuration: 1500,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius: BorderRadius.circular(30),
                            boxShadow: kShadowList,
                          ),
                          // margin: EdgeInsets.only(right: paddingLeft == null ? 0 : paddingLeft),
                        ),
                      ),
                    ),
                    Container(
                      // margin: EdgeInsets.only(top: 20),
                      // alignment: Alignment.centerLeft,
                      margin: EdgeInsets.symmetric(horizontal: 10),
                      width: MediaQuery.of(context).size.width * 0.20,
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: skeletonInstance(),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.symmetric(vertical: 20),
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: skeletonInstance(),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  width: MediaQuery.of(context).size.width * 0.50,
                  child: skeletonInstance(),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.symmetric(vertical: 20),
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: skeletonInstance(),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  width: MediaQuery.of(context).size.width * 0.50,
                  child: skeletonInstance(),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.symmetric(vertical: 20),
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: SizedBox(
                    width: 30,
                    height: 30,
                    child: skeletonInstance(),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  width: MediaQuery.of(context).size.width * 0.50,
                  child: skeletonInstance(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Column buildWidget(BuildContext context, List<GroupedTransactions> newList) {
    return Column(children: [
      FundTransferHeaderWidget(),
      Container(
        margin: EdgeInsets.symmetric(
          horizontal: 50,
          vertical: 10,
        ),
        // padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: kOffWhiteColor,
          // borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Material(
          color: kPrimaryColor,
          borderRadius: BorderRadius.all(Radius.circular(30)),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 7, vertical: 7),
            child: TabBar(
              /* onTap: (index) {
                                          print("INDEX: ");
                                          print(index);

                                          if (index == 1) {
                                            // if transactions tab is clicked
                                            getFirstTransactions();
                                          }
                                        }, */
              controller: _tabBarController,
              unselectedLabelColor: kOffWhiteColor,
              indicatorSize: TabBarIndicatorSize.tab,
              labelColor: kPrimaryColor,
              indicator: BoxDecoration(
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(50),
                color: kOffWhiteColor,
              ),
              unselectedLabelStyle: TextStyle(
                fontWeight: FontWeight.bold,
              ),
              labelStyle: TextStyle(
                fontWeight: FontWeight.bold,
                letterSpacing: 2,
                fontSize: 12,
              ),
              tabs: [
                Tab(icon: Icon(TcMobappIcons.transfer)),
                Tab(icon: Icon(TcMobappIcons.transactions)),
              ],
            ),
          ),
        ),
      ),
      Expanded(
        child: TabBarView(
          controller: _tabBarController,
          physics: NeverScrollableScrollPhysics(),
          children: [
            SingleChildScrollView(
              child: Container(
                child: Stepper(
                  type: StepperType.vertical,
                  physics: ScrollPhysics(),
                  currentStep: _currentStep,
                  onStepTapped: (step) => tapped(step),
                  onStepContinue: continued,
                  onStepCancel: cancel,
                  controlsBuilder: (BuildContext context,
                      {VoidCallback onStepContinue,
                      VoidCallback onStepCancel}) {
                    return Container(
                      margin: EdgeInsets.only(top: 15),
                      child: Column(
                        children: <Widget>[
                          // SizedBox(height: AppSize.smallMedium,),
                          WidgetConditional.ConditionalSwitch.single(
                            context: context,
                            valueBuilder: (BuildContext context) =>
                                _currentStep,
                            caseBuilders: {
                              0: (BuildContext context) {
                                return buildStepperFirst(
                                    onStepContinue, onStepCancel);
                              },
                              1: (BuildContext context) {
                                return buildStepperInBetween(
                                    onStepContinue, onStepCancel);
                              },
                              2: (BuildContext context) {
                                return buildStepperInBetween(
                                    onStepContinue, onStepCancel);
                              },
                              3: (BuildContext context) {
                                return buildStepperLast(
                                    onStepContinue, onStepCancel);
                              },
                            },
                            fallbackBuilder: (BuildContext context) {
                              /* return Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 20, left: 10, right: 10),
                                    padding: EdgeInsets.symmetric(vertical: 5),
                                    child: TextButton(
                                      child: Text(
                                        'Go Back'.toUpperCase(),
                                        style: TextStyle(
                                          color: kPrimaryColor,
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          letterSpacing: 1,
                                        ),
                                      ),
                                      onPressed: onStepCancel,
                                    ),
                                  ),
                                ],
                              ); */
                              return new Container();
                            },
                          ),
                          // SizedBox(height: AppSize.smallMedium,),
                        ],
                      ),
                    );
                  },
                  steps: <Step>[
                    Step(
                      title: new Text('Select Source Account'),
                      content: Column(
                        children: <Widget>[
                          Container(
                            // width: 200,
                            child: GestureDetector(
                              onTap: () {
                                showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  barrierColor: Colors.black.withOpacity(.90),
                                  builder: (BuildContext context) {
                                    return Dialog(
                                      // backgroundColor: Colors.transparent,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                      elevation: 0,
                                      clipBehavior: Clip.hardEdge,
                                      backgroundColor: Colors.transparent,
                                      child: Center(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                bottom: 25,
                                              ),
                                              child: Text(
                                                'Select Account',
                                                style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize:
                                                      kHeaderTitleFontSize,
                                                  fontWeight: FontWeight.bold,
                                                  letterSpacing: 1,
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                            /* 
                                                        buildCardSelection(
                                                            "ATM",
                                                            "001-1001-0001344197",
                                                            124213.33),
                                                        buildCardSelection(
                                                            "ATM",
                                                            "001-1012-0000811096",
                                                            124213.33), */
                                            Column(
                                                children: _fundTransferSources
                                                    .map((item) =>
                                                        buildCardSelection(
                                                            item.productName,
                                                            item.accountNumber,
                                                            item.availableBalance))
                                                    .toList()),
                                            /* for (var item
                                                            in _fundTransferSources)
                                                          {
                                                            buildCardSelection(
                                                                item.productName,
                                                                item.accountNumber,
                                                                item.availableBalance)
                                                          }.toList() */
                                            /* _fundTransferSources
                                                            .map((item) => new Column( children: [buildCardSelection(
                                                                item.productName,
                                                                item.accountNumber,
                                                                item.availableBalance))
                                                            .toList()] ) */
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                );
                              },
                              child: Stack(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(
                                      top: 10,
                                      right: 15,
                                    ),
                                    child: buildCard(
                                        _selectedSource.productName,
                                        _selectedSource.accountNumber,
                                        _selectedSource.availableBalance),
                                  ),
                                  Positioned(
                                    right: 0,
                                    child: CircleAvatar(
                                      backgroundColor: kWhiteColor,
                                      radius: 20,
                                      child: Icon(
                                        Icons.repeat_rounded,
                                        size: 20,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      isActive: _currentStep >= 0,
                      state: _currentStep >= 0
                          ? StepState.complete
                          : StepState.disabled,
                    ),
                    Step(
                      title: new Text('Select Fund Transfer Type'),
                      content: Container(
                        child: Column(
                          children: [
                            Column(
                              children: <Widget>[
                                if (traps.statusFundTransferOwn)
                                  Container(
                                    margin: EdgeInsets.symmetric(vertical: 10),
                                    child: SizedBox(
                                      width: double.infinity,
                                      child: ElevatedButton(
                                        onPressed: () {
                                          setState(() {
                                            _ownSelected = true;
                                            _otherSelected = false;
                                            _banksSelected = false;

                                            _radioValue1 = "OWN";
                                            _transferType =
                                                "Transfer to Own Account";
                                          });
                                        },
                                        /* shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(kButtonCircularRadius),
                                                        ), */ // optional, in order to add additional space around text if needed
                                        style: ElevatedButton.styleFrom(
                                          primary: (_ownSelected)
                                              ? kPrimaryColor
                                              : kWhiteColor,
                                          padding: EdgeInsets.all(25),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                        ),
                                        child: Text(
                                          'Transfer to Own Account'
                                              .toUpperCase(),
                                          style: TextStyle(
                                            color: (_ownSelected)
                                                ? kWhiteColor
                                                : kPrimaryColor,
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                if (traps.statusFundTransferOther)
                                  Container(
                                    margin: EdgeInsets.symmetric(vertical: 10),
                                    child: SizedBox(
                                      width: double.infinity,
                                      child: ElevatedButton(
                                        onPressed: () {
                                          setState(() {
                                            _ownSelected = false;
                                            _otherSelected = true;
                                            _banksSelected = false;

                                            _radioValue1 = "OTHER";
                                            _transferType =
                                                "Transfer to Other Account";
                                          });
                                        },
                                        /* shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(kButtonCircularRadius),
                                                        ), */ // optional, in order to add additional space around text if needed
                                        style: ElevatedButton.styleFrom(
                                          primary: (_otherSelected)
                                              ? kPrimaryColor
                                              : kWhiteColor,
                                          padding: EdgeInsets.all(25),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                        ),
                                        child: Text(
                                          'Transfer to Other Account'
                                              .toUpperCase(),
                                          style: TextStyle(
                                            color: (_otherSelected)
                                                ? kWhiteColor
                                                : kPrimaryColor,
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                if (traps.statusFundTransferBanks)
                                  Container(
                                    margin: EdgeInsets.symmetric(vertical: 10),
                                    child: SizedBox(
                                      width: double.infinity,
                                      child: ElevatedButton(
                                        onPressed: () {
                                          setState(() {
                                            _ownSelected = false;
                                            _otherSelected = false;
                                            _banksSelected = true;

                                            _radioValue1 = "BANKS";
                                            _transferType = "Transfer to Banks";
                                          });
                                        },
                                        /* shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(kButtonCircularRadius),
                                                        ), */ // optional, in order to add additional space around text if needed
                                        style: ElevatedButton.styleFrom(
                                          primary: (_banksSelected)
                                              ? kPrimaryColor
                                              : kWhiteColor,
                                          padding: EdgeInsets.all(25),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                        ),
                                        child: Text(
                                          'Transfer to Banks'.toUpperCase(),
                                          style: TextStyle(
                                            color: (_banksSelected)
                                                ? kWhiteColor
                                                : kPrimaryColor,
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                              ],
                            ),
                            (_radioValue1 == "" && _isStepTwoClicked == true)
                                ? Container(
                                    /* margin:
                                                    EdgeInsets.symmetric(
                                                        vertical: 5), */
                                    alignment: Alignment.centerLeft,
                                    child: DefaultTextStyle(
                                      style: const TextStyle(
                                        color: Colors.red,
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      child: AnimatedTextKit(
                                        animatedTexts: [
                                          FadeAnimatedText(
                                            'Please Select A Type',
                                            duration: Duration(seconds: 5),
                                            fadeInEnd: 0.1,
                                            fadeOutBegin: 1,
                                          ),
                                        ],
                                        isRepeatingAnimation: false,
                                        // pause: Duration(seconds: 20),
                                        onFinished: () {
                                          setState(() {
                                            _isStepTwoClicked = false;
                                          });
                                        },
                                        onTap: () {
                                          print("Tap Event");
                                        },
                                      ),
                                    ),
                                  )
                                : new Container(),
                          ],
                        ),
                      ),
                      isActive: _currentStep >= 1,
                      state: _currentStep >= 1
                          ? StepState.complete
                          : StepState.disabled,
                    ),
                    Step(
                      title: new Text('Fund Transfer Fields'),
                      /* subtitle: new Text(
                                      'For Fund Transfer to Other Accounts or Banks'), */
                      content: Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            WidgetConditional.ConditionalSwitch.single<String>(
                              context: context,
                              valueBuilder: (BuildContext context) =>
                                  _radioValue1,
                              caseBuilders: {
                                'OWN': (BuildContext context) {
                                  return Column(
                                    children: [
                                      buildFundTransferFieldDropdown(context),
                                      buildFundTransferFields2(
                                        amountController,
                                        amountFocusnode,
                                        hasFocus_AmountFocusNode,
                                        'Amount *',
                                        '',
                                        true,
                                        [
                                          FilteringTextInputFormatter.allow(
                                              RegExp(r'^\d*\.?\d{0,2}')),
                                        ],
                                        TextInputType.number,
                                        TextInputAction.next,
                                        (val) {
                                          if (val.isEmpty) {
                                            return 'Amount is required';
                                          } else if (num.parse(val) < 1) {
                                            return 'Amount must be greater than ₱' +
                                                NumberFormat.decimalPattern()
                                                    .format(1)
                                                    .toString();
                                          } else if (num.parse(val) >
                                              _selectedSource
                                                  .availableBalance) {
                                            return 'Amount must not be greater than ₱' +
                                                NumberFormat.decimalPattern()
                                                    .format(_selectedSource
                                                        .availableBalance)
                                                    .toString();
                                          }
                                        },
                                        (val) => kFieldFocusChange(context,
                                            amountFocusnode, remarksFocusnode),
                                        (val) => amountOnSaved(val),
                                        '',
                                      ),
                                      buildFundTransferFieldsWithHelper(
                                        remarksController,
                                        remarksFocusnode,
                                        hasFocus_RemarksFocusNode,
                                        'Remarks (Optional)',
                                        '',
                                        false,
                                        [],
                                        TextInputType.text,
                                        TextInputAction.next,
                                        (val) {},
                                        (val) {},
                                        (val) => remarksOnSaved(val),
                                        'Remarks (if not empty) will be treated as an additional message to the notification after a succesful transaction.',
                                      ),
                                    ],
                                  );
                                },
                                'OTHER': (BuildContext context) {
                                  return Column(children: [
                                    buildFundTransferFields2(
                                      otherAccountNumberController,
                                      otherAccountNumberFocusnode,
                                      hasFocus_OtherAccountNumberFocusnode,
                                      'Account Number *',
                                      '###-####-##########',
                                      false,
                                      [
                                        new MaskTextInputFormatter(
                                            mask: '###-####-##########',
                                            filter: {"#": RegExp(r'[0-9]')})
                                      ],
                                      TextInputType.number,
                                      TextInputAction.next,
                                      (val) => otherAccountNumberValidator(val),
                                      (val) => kFieldFocusChange(
                                          context,
                                          otherAccountNumberFocusnode,
                                          otherAccountNameFocusnode),
                                      (val) {
                                        print("ON SAVED: $val");
                                        otherAccountNumberOnSaved(val);
                                      },
                                      '',
                                    ),
                                    buildFundTransferFields2(
                                      otherAccountNameController,
                                      otherAccountNameFocusnode,
                                      hasFocus_OtherAccountNameFocusnode,
                                      'Account Name *',
                                      '',
                                      false,
                                      [],
                                      TextInputType.text,
                                      TextInputAction.next,
                                      (val) => otherAccountNameValidator(val),
                                      (val) => kFieldFocusChange(
                                          context,
                                          otherAccountNameFocusnode,
                                          otherAmountFocusnode),
                                      (val) => otherAccountNameOnSaved(val),
                                      '',
                                    ),
                                    buildFundTransferFields2(
                                      otherAmountController,
                                      otherAmountFocusnode,
                                      hasFocus_OtherAmountFocusnode,
                                      'Amount *',
                                      '',
                                      true,
                                      [
                                        FilteringTextInputFormatter.allow(
                                            RegExp(r'^\d*\.?\d{0,2}')),
                                      ],
                                      TextInputType.number,
                                      TextInputAction.next,
                                      (val) => otherAmountValidator(val),
                                      (val) => kFieldFocusChange(
                                          context,
                                          otherAmountFocusnode,
                                          otherRemarksFocusnode),
                                      (val) => amountOnSaved(val),
                                      '',
                                    ),
                                    buildFundTransferFieldsWithHelper(
                                      otherRemarksController,
                                      otherRemarksFocusnode,
                                      hasFocus_OtherRemarksFocusnode,
                                      'Remarks (Optional)',
                                      '',
                                      false,
                                      [],
                                      TextInputType.text,
                                      TextInputAction.done,
                                      (val) {},
                                      (val) {},
                                      (val) => remarksOnSaved(val),
                                      'Remarks (if not empty) will be treated as an additional message to the notification after a succesful transaction.',
                                    ),
                                  ]);
                                },
                                'BANKS': (BuildContext context) {
                                  return Column(children: [
                                    /* Container(
                                                  padding:
                                                      EdgeInsets.only(
                                                          top: 10,
                                                          bottom: 10),
                                                  child: DropdownSearch<
                                                      String>(
                                                    validator: (v) => v ==
                                                            null
                                                        ? "required field"
                                                        : null,
                                                    hint: "Select Bank",
                                                    selectedItem:
                                                        _ownTransferTo,
                                                    mode: Mode.DIALOG,
                                                    popupTitle: Container(
                                                      margin: EdgeInsets
                                                          .symmetric(
                                                              vertical:
                                                                  15),
                                                      alignment: Alignment
                                                          .center,
                                                      child: Text(
                                                        "Select Bank",
                                                        style: TextStyle(
                                                          color:
                                                              kPrimaryColor,
                                                          fontSize: 20,
                                                          fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                        ),
                                                      ),
                                                    ),
                                                    popupShape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius
                                                              .circular(
                                                                  10),
                                                    ),
                                                    showSelectedItem:
                                                        true,
                                                    items: [
                                                      "Gcash",
                                                      "Union Bank",
                                                      "Paymaya",
                                                      "Midjobs",
                                                    ],
                                                    label:
                                                        "Transfer To *",
                                                    showClearButton:
                                                        false,
                                                    onChanged: (data) {
                                                      print(
                                                          "data is: ${data}");
                                                    },
                                                    onSaved: (val) =>
                                                        transferToOnSaved(
                                                            val),

                                                    // popupItemDisabled: (String s) => s.startsWith('I'),
                                                    // selectedItem: "",
                                                    dropdownSearchDecoration:
                                                        InputDecoration(
                                                      contentPadding:
                                                          EdgeInsets.all(
                                                              10),
                                                      labelStyle: TextStyle(
                                                          color: fundTransferSelection
                                                                  .hasFocus
                                                              ? Color(
                                                                  0xFF00238A)
                                                              : Color(
                                                                  0xFF828282)),
                                                      fillColor: Color(
                                                          0xffF2F2F2),
                                                      filled: true,
                                                      border:
                                                          new OutlineInputBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                    .circular(
                                                                10.0),
                                                        borderSide:
                                                            new BorderSide(),
                                                      ),
                                                      enabledBorder:
                                                          new OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(
                                                                    10.0),
                                                        borderSide:
                                                            BorderSide(
                                                          color: Color(
                                                              0xFFE0E0E0),
                                                        ),
                                                      ),
                                                      focusedBorder:
                                                          new OutlineInputBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(
                                                                    10.0),
                                                        borderSide:
                                                            BorderSide(
                                                          color: Color(
                                                              0xFF00238A),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                buildFundTransferFields(
                                                  beneAccountNumberController,
                                                  beneAccountNumberFocusnode,
                                                  'Beneficiary Account Number *',
                                                  '',
                                                  false,
                                                  [],
                                                  TextInputType.text,
                                                  TextInputAction.next,
                                                  (val) =>
                                                      accountNumberValidator(
                                                          val),
                                                  (val) => accountNumberOnFieldSubmitted(
                                                      val,
                                                      beneAccountNumberFocusnode,
                                                      beneAccountNameFocusnode),
                                                  (val) =>
                                                      beneAccountNumberOnSaved(
                                                          val),
                                                  '',
                                                ),
                                                buildFundTransferFields(
                                                  beneAccountNameController,
                                                  beneAccountNameFocusnode,
                                                  'Beneficiary Account Name *',
                                                  '',
                                                  false,
                                                  [],
                                                  TextInputType.text,
                                                  TextInputAction.next,
                                                  (val) =>
                                                      accountNumberValidator(
                                                          val),
                                                  (val) => accountNumberOnFieldSubmitted(
                                                      val,
                                                      beneAccountNameFocusnode,
                                                      beneAddressFocusnode),
                                                  (val) =>
                                                      beneAccountNameOnSaved(
                                                          val),
                                                  '',
                                                ),
                                                buildFundTransferFields(
                                                  beneAddressController,
                                                  beneAddressFocusnode,
                                                  'Beneficiary Address *',
                                                  '',
                                                  false,
                                                  [],
                                                  TextInputType.text,
                                                  TextInputAction.next,
                                                  (val) =>
                                                      accountNumberValidator(
                                                          val),
                                                  (val) => accountNumberOnFieldSubmitted(
                                                      val,
                                                      beneAddressFocusnode,
                                                      amountFocusnode),
                                                  (val) =>
                                                      beneAddressOnSaved(
                                                          val),
                                                  '',
                                                ),
                                                buildFundTransferFields(
                                                  amountController,
                                                  amountFocusnode,
                                                  'Amount *',
                                                  '',
                                                  true,
                                                  [],
                                                  TextInputType.number,
                                                  TextInputAction.next,
                                                  (val) =>
                                                      accountNumberValidator(
                                                          val),
                                                  (val) =>
                                                      accountNumberOnFieldSubmitted(
                                                          val,
                                                          amountFocusnode,
                                                          remarksFocusnode),
                                                  (val) =>
                                                      amountOnSaved(val),
                                                  '',
                                                ),
                                                buildFundTransferFields(
                                                  remarksController,
                                                  remarksFocusnode,
                                                  'Remarks (Optional)',
                                                  '',
                                                  false,
                                                  [],
                                                  TextInputType.number,
                                                  TextInputAction.next,
                                                  (val) =>
                                                      accountNumberValidator(
                                                          val),
                                                  (val) {},
                                                  /* accountNumberOnFieldSubmitted(
                                                  val,
                                                  remarks_focusnode,
                                                  remarks_focusnode), */
                                                  (val) =>
                                                      remarksOnSaved(val),
                                                  '',
                                                ), */
                                  ]);
                                },
                              },
                              fallbackBuilder: (BuildContext context) =>
                                  Text('None of the cases matched!'),
                            ),
                          ],
                        ),
                      ),
                      isActive: _currentStep >= 2,
                      state: _currentStep >= 2
                          ? StepState.complete
                          : StepState.disabled,
                    ),
                    Step(
                        title: new Text('Summary'),
                        isActive: _currentStep >= 3,
                        state: _currentStep >= 3
                            ? StepState.complete
                            : StepState.disabled,
                        content: Container(
                          margin: EdgeInsets.symmetric(vertical: 5),
                          decoration: BoxDecoration(
                            color: kWhiteColor,
                            borderRadius: BorderRadius.circular(30),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.1),
                                spreadRadius: 0.5,
                                blurRadius: 3,
                                offset: Offset(
                                    0.5, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          child: Column(
                            children: [
                              Container(
                                padding: EdgeInsets.only(
                                  left: 25,
                                  right: 25,
                                  bottom: 10,
                                  top: 20,
                                ),
                                child: Column(children: [
                                  Container(
                                    margin: EdgeInsets.symmetric(
                                      vertical: 5,
                                    ),
                                    child: Text(
                                      NumberFormat.currency(
                                        locale: 'en_PH',
                                        name: '₱',
                                        decimalDigits: 2,
                                      )
                                          .format((_userFundTransferDetails
                                                      .amount ==
                                                  null)
                                              ? 0
                                              : _userFundTransferDetails.amount)
                                          .toString(),
                                      style: TextStyle(
                                        fontSize: 40,
                                        fontWeight: FontWeight.bold,
                                        letterSpacing: 1,
                                        // color: kPrimaryColor,
                                      ),
                                    ),
                                  ),
                                  WidgetConditional.ConditionalSwitch.single(
                                    context: context,
                                    valueBuilder: (BuildContext context) =>
                                        _userFundTransferDetails.transferType,
                                    caseBuilders: {
                                      'OWN': (BuildContext context) {
                                        return buildSummaryOwn();
                                      },
                                      'OTHER': (BuildContext context) {
                                        return Column(
                                          children: [
                                            Container(
                                              margin: EdgeInsets.only(
                                                top: 5,
                                                bottom: 1,
                                              ),
                                              child: Text(
                                                'Transfer to Other Account',
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  /* fontWeight:
                                                    FontWeight.bold, */
                                                  color: Colors.black
                                                      .withOpacity(.80),
                                                  letterSpacing: 1,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                vertical: 1,
                                              ),
                                              child: Text(
                                                (_userFundTransferDetails
                                                        .transferToOther
                                                        .accountName
                                                        .isNotEmpty)
                                                    ? _userFundTransferDetails
                                                        .transferToOther
                                                        .accountName
                                                    : '',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black
                                                      .withOpacity(.60),
                                                  letterSpacing: 1,
                                                ),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.symmetric(
                                                vertical: 1,
                                              ),
                                              child: Text(
                                                (_userFundTransferDetails
                                                        .transferToOther
                                                        .accountNumber
                                                        .isNotEmpty)
                                                    ? _userFundTransferDetails
                                                        .transferToOther
                                                        .accountNumber
                                                    : '',
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  /* fontWeight:
                                                    FontWeight.bold, */
                                                  color: Colors.black
                                                      .withOpacity(.80),
                                                  letterSpacing: 1,
                                                ),
                                              ),
                                            ),
                                          ],
                                        );
                                      },
                                      'BANKS': (BuildContext context) {
                                        return Container();
                                      },
                                    },
                                    fallbackBuilder: (BuildContext context) {
                                      return new Container();
                                    },
                                  ),
                                  /* (_userFundTransferDetails.transferType ==
                                          'OWN')
                                      ? Container(
                                          margin: EdgeInsets.only(
                                            top: 5,
                                            bottom: 1,
                                          ),
                                          child: Text(
                                            'Transfer to Own Account',
                                            style: TextStyle(
                                              fontSize: 14,
                                              /* fontWeight:
                                                    FontWeight.bold, */
                                              color:
                                                  Colors.black.withOpacity(.80),
                                              letterSpacing: 1,
                                            ),
                                          ),
                                        )
                                      : new Container(), */
                                ]),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 25,
                                  // vertical: 20,
                                ),
                                margin: EdgeInsets.only(
                                  top: 25,
                                ),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        'Source',
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black.withOpacity(.70),
                                          letterSpacing: 1,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Divider(
                                color: Colors.grey.withOpacity(0.30),
                                // height: 20,
                                // thickness: 5,
                                /* indent: 25,
                                            endIndent: 30, */
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                    // vertical: 5,
                                    horizontal: 25),
                                child: Row(children: [
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      margin: EdgeInsets.symmetric(
                                        vertical: 5,
                                      ),
                                      padding: EdgeInsets.symmetric(
                                        vertical: 10,
                                      ),
                                      decoration: BoxDecoration(
                                        color: kPrimaryColor,
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      child: ClipRRect(
                                        child: Image.asset(
                                          "assets/images/atm.png",
                                          height: 23,
                                          width: 23,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                      flex: 4,
                                      child: Container(
                                        margin: EdgeInsets.symmetric(
                                            vertical: 5, horizontal: 10),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              (_userFundTransferDetails.source
                                                      .productName.isNotEmpty)
                                                  ? _userFundTransferDetails
                                                      .source.productName
                                                  : '',
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black
                                                    .withOpacity(.70),
                                                letterSpacing: 1,
                                              ),
                                            ),
                                            Text(
                                              (_userFundTransferDetails.source
                                                      .accountNumber.isNotEmpty)
                                                  ? _userFundTransferDetails
                                                      .source.accountNumber
                                                  : '',
                                              style: TextStyle(
                                                fontSize: 12,
                                                /* fontWeight:
                                                      FontWeight.bold, */
                                                color: Colors.black
                                                    .withOpacity(.80),
                                                letterSpacing: 1,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),
                                ]),
                              ),
                              Divider(
                                color: Colors.grey.withOpacity(0.30),
                              ),
                              Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal: 25,
                                  vertical: 10,
                                ),
                                margin: EdgeInsets.symmetric(vertical: 10),
                                child: ConfirmationSlider(
                                  backgroundColor: kOffWhiteColor,
                                  foregroundColor: kPrimaryColor,
                                  backgroundColorEnd: kPrimaryColor,
                                  shadow: BoxShadow(
                                    color: Colors.black.withOpacity(0.2),
                                    blurRadius: 2,
                                    // offset: Offset(0.5, 0.5),
                                  ),
                                  onConfirmation: () {
                                    String otp = kOTPCodeGenerate();
                                    _userFundTransferDetails.code = otp;
                                    saveFundTransferDetails(
                                        _userFundTransferDetails);

                                    showDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      barrierColor: kDialogModalBarrierColor,
                                      builder: (BuildContext context) {
                                        // return new Container();
                                        return OTPCodeVerificationWidget(
                                            phoneNumber:
                                                kUserDetails.mobilenumber,
                                            otp: otp,
                                            transactionType: "FUND TRANSFER",
                                            onSubmit: () {
                                              if (_userFundTransferDetails
                                                      .transferType ==
                                                  'OWN') {
                                                processFundTransferOwn(
                                                    _userFundTransferDetails);
                                              } else if (_userFundTransferDetails
                                                      .transferType ==
                                                  'OTHER') {
                                                processFundTransferOther(
                                                    _userFundTransferDetails);
                                              } else if (_userFundTransferDetails
                                                      .transferType ==
                                                  'BANKS') {}

                                              /* return showDialog(
                                                context: context,
                                                barrierDismissible: false,
                                                barrierColor:
                                                    kDialogModalBarrierColor,
                                                builder:
                                                    (BuildContext context) {
                                                  return ProgressIndicatorWidget(
                                                    result: result,
                                                  );
                                                },
                                              ); */
                                            });
                                      },
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        )),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: LazyLoadScrollView(
                scrollOffset: 0,
                onEndOfPage: () {
                  if (_fundTransferTransactions.length <
                      _totalTransactionCount) {
                    getMoreTransactions();
                  }
                },
                isLoading:
                    (_transactionIsLoadingFirst || _transactionIsLoadingRange),
                child: ListView(
                  physics: ClampingScrollPhysics(),
                  children: [
                    Column(children: [
                      Container(
                        margin: EdgeInsets.only(
                            // top: 20,
                            right: 20,
                            left: 20,
                            bottom: 20),
                        child: Column(children: [
                          Column(children: [
                            if (!_transactionIsLoadingFirst)
                              Container(
                                // margin: EdgeInsets.only(top: 15),
                                child: buildGroupedTransactions(newList),
                              ),
                            if (_transactionIsLoadingFirst)
                              Container(
                                child: BuildFirstLoadingSkeleton(),
                              ),
                            if (_transactionIsLoadingRange)
                              Container(
                                child: BuildMoreLoadingSkeleton(),
                              ),
                            if (_fundTransferTransactions.length >=
                                    _totalTransactionCount &&
                                _showEndOfTransactions)
                              Container(
                                margin: EdgeInsets.symmetric(
                                  vertical: 20,
                                ),
                                child: Text(
                                  '~ End of transactions ~',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: kTransactionSubtitleColor,
                                  ),
                                ),
                              ),
                          ]),
                        ]),
                      ),
                    ]),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ]);
  }

  Column buildSummaryOwn() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(
            top: 5,
            bottom: 1,
          ),
          child: Text(
            'Transfer to Own Account',
            style: TextStyle(
              fontSize: 14,
              /* fontWeight:
                                                  FontWeight.bold, */
              color: Colors.black.withOpacity(.80),
              letterSpacing: 1,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(
            vertical: 1,
          ),
          child: Text(
            (_userFundTransferDetails.transferToOwn.productName != null)
                ? _userFundTransferDetails.transferToOwn.productName
                : '',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
              color: Colors.black.withOpacity(.60),
              letterSpacing: 1,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(
            vertical: 1,
          ),
          child: Text(
            (_userFundTransferDetails.transferToOwn.accountNumber.isNotEmpty)
                ? _userFundTransferDetails.transferToOwn.accountNumber
                : '',
            style: TextStyle(
              fontSize: 12,
              /* fontWeight:
                                                  FontWeight.bold, */
              color: Colors.black.withOpacity(.80),
              letterSpacing: 1,
            ),
          ),
        ),
      ],
    );
  }

  Row buildStepperFirst(
      VoidCallback onStepContinue, VoidCallback onStepCancel) {
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(
        margin: EdgeInsets.symmetric(horizontal: 5),
        child: ElevatedButton(
          onPressed: onStepContinue,
          style: ElevatedButton.styleFrom(
            primary: kPrimaryColor,
            padding: EdgeInsets.all(20),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(kButtonCircularRadius),
            ),
          ),
          child: Text(
            'Continue'.toUpperCase(),
            style: TextStyle(
              color: kWhiteColor,
              fontSize: 10,
              fontWeight: FontWeight.bold,
              letterSpacing: 1,
            ),
          ),
        ),
      ),
    ]);
  }

  Row buildStepperInBetween(
      VoidCallback onStepContinue, VoidCallback onStepCancel) {
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Container(
        margin: EdgeInsets.symmetric(horizontal: 5),
        child: ElevatedButton(
          onPressed: onStepContinue,
          style: ElevatedButton.styleFrom(
            primary: kPrimaryColor,
            padding: EdgeInsets.all(20),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(kButtonCircularRadius),
            ),
          ),
          child: Text(
            'Continue'.toUpperCase(),
            style: TextStyle(
              color: kWhiteColor,
              fontSize: 10,
              fontWeight: FontWeight.bold,
              letterSpacing: 1,
            ),
          ),
        ),
      ),
      Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        padding: EdgeInsets.symmetric(vertical: 5),
        child: TextButton(
          child: Text(
            'Go Back'.toUpperCase(),
            style: TextStyle(
              color: kPrimaryColor,
              fontSize: 10,
              fontWeight: FontWeight.bold,
              letterSpacing: 1,
            ),
          ),
          onPressed: onStepCancel,
        ),
      ),
    ]);
  }

  Column buildStepperLast(
      VoidCallback onStepContinue, VoidCallback onStepCancel) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 20, left: 10, right: 10),
          padding: EdgeInsets.symmetric(vertical: 5),
          child: TextButton(
            child: Text(
              'Go Back'.toUpperCase(),
              style: TextStyle(
                color: kPrimaryColor,
                fontSize: 10,
                fontWeight: FontWeight.bold,
                letterSpacing: 1,
              ),
            ),
            onPressed: onStepCancel,
          ),
        ),
      ],
    );
  }

  ConstrainedBox buildCard(
      String accountName, String accoutNumber, num balance) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 300),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: kPrimaryColor,
        elevation: 10,
        child: Column(
          children: <Widget>[
            new ListTile(
              title: Text(
                accountName,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 2,
                ),
              ),
              subtitle: Text(
                accoutNumber,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 10,
                  letterSpacing: 2,
                ),
              ),
            ),
            ListTile(
              trailing: Text(
                '₱ ' + NumberFormat.decimalPattern().format(balance),
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 2,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  ConstrainedBox buildCardSelection(
      String accountName, String accountNumber, num balance) {
    return ConstrainedBox(
      constraints: BoxConstraints(maxWidth: 300),
      child: GestureDetector(
        onTap: () {
          setState(() {
            _selectedSource = (_fundTransferSources
                .where((item) => item.accountNumber == accountNumber)
                .toList())[0];
          });
          Navigator.of(context).pop();
        },
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          color: kPrimaryColor,
          elevation: 10,
          child: Column(
            children: <Widget>[
              new ListTile(
                title: Text(
                  accountName,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2,
                  ),
                ),
                subtitle: Text(
                  accountNumber,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 10,
                    letterSpacing: 2,
                  ),
                ),
              ),
              ListTile(
                trailing: Text(
                  '₱ ' + NumberFormat.decimalPattern().format(balance),
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 2,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String otherAccountNumberValidator(val) {
    if (val.isEmpty) {
      return 'Account Number * is required';
    } else if (val.length < 19) {
      return 'Invalid Account Number';
    }
  }

  String otherAccountNameValidator(val) {
    if (val.isEmpty) {
      return 'Account Name * is required';
    }
  }

  String otherAmountValidator(val) {
    if (val.isEmpty) {
      return 'Amount is required';
    } else if (num.parse(val) < 1) {
      return 'Amount must be greater than ₱' +
          NumberFormat.decimalPattern().format(1).toString();
    } else if (num.parse(val) > _selectedSource.availableBalance) {
      return 'Amount must not be greater than ₱' +
          NumberFormat.decimalPattern()
              .format(_selectedSource.availableBalance)
              .toString();
    }
  }

  Function accountNumberOnFieldSubmitted(val, prevFocusNode, nextFocusNode) {
    kFieldFocusChange(context, prevFocusNode, nextFocusNode);
  }

  Function transferToOwnOnSaved(val) {
    setState(() => _userFundTransferDetails.transferToOwn = val);
  }

  Function transferToOnSaved(val) {
    setState(() => _userFundTransferDetails.transferTo = val);
  }

  Function otherAccountNumberOnSaved(val) {
    print("FUNCTION: $val");
    setState(
        () => _userFundTransferDetails.transferToOther.accountNumber = val);
  }

  Function otherAccountNameOnSaved(val) {
    setState(() => _userFundTransferDetails.transferToOther.accountName = val);
  }

  Function amountOnSaved(val) {
    print(val);
    val = double.parse(val);
    setState(() => _userFundTransferDetails.amount = val);
  }

  Function remarksOnSaved(val) {
    setState(() => _userFundTransferDetails.remarks = val);
  }

  Function beneAccountNumberOnSaved(val) {
    setState(() => _userFundTransferDetails.beneAccountNumber = val);
  }

  Function beneAccountNameOnSaved(val) {
    setState(() => _userFundTransferDetails.beneAccountName = val);
  }

  Function beneAddressOnSaved(val) {
    setState(() => _userFundTransferDetails.beneAddress = val);
  }

  Container buildFundTransferFields2(
      TextEditingController tffController,
      FocusNode tffFocusNode,
      bool tffHasFocus,
      String tffLabelText,
      String tffHintText,
      bool isAmount,
      List<TextInputFormatter> tffTextInputFormatter,
      TextInputType tffTextInputType,
      TextInputAction tffTextInputAction,
      Function(String) tffValidator,
      Function tffOnFieldSubmitted,
      Function(String) tffOnSaved,
      String tffHelperText) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        controller: tffController,
        focusNode: tffFocusNode,
        cursorColor: Color(0xFF00238A),
        decoration: InputDecoration(
          labelText: tffLabelText,
          hintText: tffHintText,
          /* helperText: tffHelperText,
          helperMaxLines: (tffHelperText != '') ? 5 : null, */
          hintStyle: TextStyle(fontSize: 14),
          labelStyle: TextStyle(
              color: tffHasFocus ? Color(0xFF00238A) : Color(0xFF828282)),
          fillColor: Color(0xffF2F2F2),
          filled: true,
          border: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(10.0),
            borderSide: new BorderSide(),
          ),
          enabledBorder: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Color(0xFFE0E0E0),
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Color(0xFF00238A),
            ),
          ),
          prefixText: (isAmount == true) ? '₱' : '',
        ),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        textInputAction: tffTextInputAction,
        keyboardType: tffTextInputType,
        inputFormatters: tffTextInputFormatter,
        validator: tffValidator,
        onFieldSubmitted: tffOnFieldSubmitted,
        onSaved: tffOnSaved,
      ),
    );
  }

  Container buildFundTransferFields(
      TextEditingController tffController,
      FocusNode tffFocusNode,
      String tffLabelText,
      String tffHintText,
      bool isAmount,
      List<TextInputFormatter> tffTextInputFormatter,
      TextInputType tffTextInputType,
      TextInputAction tffTextInputAction,
      Function(String) tffValidator,
      Function(String) tffOnFieldSubmitted,
      Function(String) tffOnSaved,
      String tffHelperText) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        controller: tffController,
        focusNode: tffFocusNode,
        cursorColor: Color(0xFF00238A),
        decoration: InputDecoration(
          labelText: tffLabelText,
          hintText: tffHintText,
          /* helperText: tffHelperText,
          helperMaxLines: (tffHelperText != '') ? 5 : null, */
          hintStyle: TextStyle(fontSize: 14),
          labelStyle: TextStyle(
              color: tffFocusNode.hasFocus
                  ? Color(0xFF00238A)
                  : Color(0xFF828282)),
          fillColor: Color(0xffF2F2F2),
          filled: true,
          border: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(10.0),
            borderSide: new BorderSide(),
          ),
          enabledBorder: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Color(0xFFE0E0E0),
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Color(0xFF00238A),
            ),
          ),
          prefixText: (isAmount == true) ? '₱' : '',
        ),
        autovalidateMode: AutovalidateMode.onUserInteraction,
        textInputAction: tffTextInputAction,
        keyboardType: tffTextInputType,
        inputFormatters: tffTextInputFormatter,
        validator: tffValidator,
        onFieldSubmitted: tffOnFieldSubmitted,
        onSaved: tffOnSaved,
      ),
    );
  }

  Container buildFundTransferFieldDropdown(BuildContext context) {
    /* List<FundTransferOwnDestinationClass> dataDropDown =
        getDataFundTransferOwn('');

    List<Container> selection = [];

    for (var i in dataDropDown) {
      selection.add(Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        child: ListTile(
          selected: (_ownTransferToClass != null)
              ? ((_ownTransferToClass.accountNumber == i.accountNumber)
                  ? true
                  : false)
              : false,
          hoverColor: Colors.grey.withOpacity(0.30),
          selectedTileColor: kPrimaryColor.withOpacity(0.20),
          title: Text(
            i.productName,
            style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
          ),
          subtitle: Text(
            i.accountNumber,
            style: TextStyle(fontSize: 12),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          contentPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
          // horizontalTitleGap: 12,
          onTap: () {
            setState(() {
              _ownTransferToClass = i;
              fundTransferOwnSelectionController.text =
                  _ownTransferToClass.productName +
                      '\n' +
                      _ownTransferToClass.accountNumber;
              /* fundTransferOwnSelectionController
                                              .buildTextSpan(
                                                  context: context,
                                                  withComposing: true); */
              Navigator.of(context).pop();
            });
          },
        ),
      ));
    }

    _filterSelection() {} */

    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        onTap: () {
          return showDialog(
              context: context,
              useRootNavigator: true,
              builder: (context) {
                List<FundTransferOwnDestinationClass> dataDropDown =
                    getDataFundTransferOwn('');

                return StatefulBuilder(builder: (context, setState) {
                  return AlertDialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                    contentPadding: EdgeInsets.all(32.0),
                    content: Container(
                      width: MediaQuery.of(context).size.width * 0.80,
                      height: MediaQuery.of(context).size.height * 0.60,
                      child: Column(
                        children: [
                          Text(
                            "Select Account",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18,
                              color: kPrimaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            child: TextFormField(
                              // controller: tffController,
                              // focusNode: tffFocusNode,
                              cursorColor: Color(0xFF00238A),
                              decoration: InputDecoration(
                                labelText: "Search",
                                hintText: "",
                                helperText: "",
                                helperMaxLines: 5,
                                hintStyle: TextStyle(fontSize: 14),
                                /* labelStyle: TextStyle(
                                      color: tffHasFocus
                                          ? Color(0xFF00238A)
                                          : Color(0xFF828282)), */
                                fillColor: Color(0xffF2F2F2),
                                filled: true,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  borderSide: new BorderSide(),
                                ),
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: Color(0xFFE0E0E0),
                                  ),
                                ),
                                focusedBorder: new OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                  borderSide: BorderSide(
                                    color: Color(0xFF00238A),
                                  ),
                                ),
                                prefixIcon: Icon(
                                  Icons.search,
                                  size: 20.0,
                                ),
                              ),
                              onChanged: (filter) {
                                print(filter);
                                setState(() {
                                  dataDropDown = getDataFundTransferOwn(filter);
                                });
                                print(dataDropDown.length);
                              },
                              /* textInputAction: tffTextInputAction,
                                keyboardType: tffTextInputType,
                                inputFormatters: tffTextInputFormatter, */
                            ),
                          ),
                          Expanded(
                            child: ListView(
                              physics: ClampingScrollPhysics(),
                              // shrinkWrap: true,
                              children: [
                                for (var i in dataDropDown)
                                  Container(
                                    margin: EdgeInsets.symmetric(vertical: 5),
                                    child: ListTile(
                                      selected: (_ownTransferToClass != null)
                                          ? ((_ownTransferToClass
                                                      .accountNumber ==
                                                  i.accountNumber)
                                              ? true
                                              : false)
                                          : false,
                                      hoverColor: Colors.grey.withOpacity(0.30),
                                      selectedTileColor:
                                          kPrimaryColor.withOpacity(0.20),
                                      title: Text(
                                        i.productName,
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      subtitle: Text(
                                        i.accountNumber,
                                        style: TextStyle(fontSize: 12),
                                      ),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                      contentPadding: EdgeInsets.only(
                                          left: 10, right: 10, bottom: 10),
                                      // horizontalTitleGap: 12,
                                      onTap: () {
                                        setState(() {
                                          _ownTransferToClass = i;
                                          fundTransferOwnSelectionController
                                              .text = _ownTransferToClass
                                                  .productName +
                                              '\n' +
                                              _ownTransferToClass.accountNumber;
                                          /* fundTransferOwnSelectionController
                                                .buildTextSpan(
                                                    context: context,
                                                    withComposing: true); */
                                          Navigator.of(context).pop();
                                        });
                                      },
                                    ),
                                  ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                });
              });
        },
        readOnly: true,
        keyboardType: TextInputType.multiline,
        maxLines: null,
        controller: fundTransferOwnSelectionController,
        focusNode: fundTransferSelection,
        cursorColor: Color(0xFF00238A),
        decoration: InputDecoration(
          labelText: "Transfer To *",
          hintText: "",
          hintStyle: TextStyle(fontSize: 14),
          labelStyle: TextStyle(
              color: hasFocus_FundTransferSelection
                  ? Color(0xFF00238A)
                  : Color(0xFF828282)),
          fillColor: Color(0xffF2F2F2),
          filled: true,
          border: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(10.0),
            borderSide: new BorderSide(),
          ),
          enabledBorder: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Color(0xFFE0E0E0),
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Color(0xFF00238A),
            ),
          ),
          suffixIcon: Icon(
            Icons.arrow_drop_down_rounded,
            size: 24.0,
          ),
          // prefixText: (isAmount == true) ? '₱' : '',
        ),
        validator: (v) => v == null ? "Transfer To * is required" : null,
        onFieldSubmitted: (val) =>
            kFieldFocusChange(context, fundTransferSelection, amountFocusnode),
        onSaved: (val) {
          setState(() {
            transferToOwnOnSaved(_ownTransferToClass);
          });
        },
        /* textInputAction: tffTextInputAction,
        keyboardType: tffTextInputType,
        inputFormatters: tffTextInputFormatter,
        validator: tffValidator,
        onFieldSubmitted: tffOnFieldSubmitted,
        onSaved: tffOnSaved, */
      ),
    );
  }

  Container buildFundTransferFieldsWithHelper(
      TextEditingController tffController,
      FocusNode tffFocusNode,
      bool tffHasFocus,
      String tffLabelText,
      String tffHintText,
      bool isAmount,
      List<TextInputFormatter> tffTextInputFormatter,
      TextInputType tffTextInputType,
      TextInputAction tffTextInputAction,
      Function(String) tffValidator,
      Function(String) tffOnFieldSubmitted,
      Function(String) tffOnSaved,
      String tffHelperText) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: TextFormField(
        controller: tffController,
        focusNode: tffFocusNode,
        cursorColor: Color(0xFF00238A),
        decoration: InputDecoration(
          labelText: tffLabelText,
          hintText: tffHintText,
          helperText: tffHelperText,
          helperMaxLines: 5,
          hintStyle: TextStyle(fontSize: 14),
          labelStyle: TextStyle(
              color: tffHasFocus ? Color(0xFF00238A) : Color(0xFF828282)),
          fillColor: Color(0xffF2F2F2),
          filled: true,
          border: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(10.0),
            borderSide: new BorderSide(),
          ),
          enabledBorder: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Color(0xFFE0E0E0),
            ),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(
              color: Color(0xFF00238A),
            ),
          ),
          prefixText: (isAmount == true) ? '₱' : '',
        ),
        textInputAction: tffTextInputAction,
        keyboardType: tffTextInputType,
        inputFormatters: tffTextInputFormatter,
        validator: tffValidator,
        onFieldSubmitted: tffOnFieldSubmitted,
        onSaved: tffOnSaved,
      ),
    );
  }

  showConfirmation() {
    showDialog(
      context: context,
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(.80),
      builder: (BuildContext context) {
        return Dialog(
          // backgroundColor: Colors.transparent,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          elevation: 0,
          backgroundColor: Colors.transparent,
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(
                  bottom: 25,
                ),
                child: Text(
                  'Select Account',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: kHeaderTitleFontSize,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                color: kPrimaryColor,
                elevation: 10,
                child: Column(
                  children: <Widget>[
                    const ListTile(
                      title: Text(
                        'ATM',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 2,
                        ),
                      ),
                      subtitle: Text(
                        '001-1012-000230194',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10,
                          letterSpacing: 2,
                        ),
                      ),
                    ),
                    ListTile(
                      trailing: Text(
                        '₱ ' + NumberFormat.decimalPattern().format(12421312),
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 2,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                color: kPrimaryColor,
                elevation: 10,
                child: Column(
                  children: <Widget>[
                    const ListTile(
                      title: Text(
                        'Regular Savings',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 2,
                        ),
                      ),
                      subtitle: Text(
                        '001-1012-000230194',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 10,
                          letterSpacing: 2,
                        ),
                      ),
                    ),
                    ListTile(
                      trailing: Text(
                        '₱ ' + NumberFormat.decimalPattern().format(12421312),
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 2,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
        );
      },
    );
  }

  tapped(int step) {
    setState(() => _currentStep = step);

    /* if (step <= 2) {
      _ownTransferTo = null;
      _formKey.currentState.reset();
    } */

    if (_currentStep <= 2) {
      _clearForm();
    }
  }

  continued() {
    print("STEP: $_currentStep");

    /* if (_currentStep == 0) {
      _filteredFundTransferOwnDestinations = _fundTransferOwnDestinations
          .where((element) =>
              element.accountNumber.toLowerCase() !=
              _selectedSource.accountNumber.toLowerCase())
          .toList();
    } */

    /* if (_currentStep == 1) {
      //Select Fund Transfer Type
      setState(() {
        _isStepTwoClicked = false;
        _isStepTwoClicked = true;
        FocusScope.of(context).requestFocus(new FocusNode());
      });
    } */

    /* if (_currentStep == 2) {
      setState(() {
        FocusScope.of(context).requestFocus(new FocusNode());
      });
    } */

    /* if (_currentStep == 2) {
      print("DEREEEEEEEEEE");
      print("ACCOUNT NUMBER: ${_userFundTransferDetails.accountNumber}");
      print("ACCOUNT NAME: ${_userFundTransferDetails.accountNumber}");
      print("AMOUNT: ${_userFundTransferDetails.amount}");
      print("REMARKS: ${_userFundTransferDetails.remarks}");
      print(
          "BENE ACCOUNT NUMBER: ${_userFundTransferDetails.beneAccountNumber}");
      print("BENE ACCOUNT NAME: ${_userFundTransferDetails.beneAccountName}");
      print("BENE ADDRESS: ${_userFundTransferDetails.beneAddress}");
    } */

    /* if (_currentStep == 3) {
      return showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.black.withOpacity(.80),
        builder: (BuildContext context) {
          return otpDialog(context);
        },
      );
    } */

    // _currentStep < 3 ? setState(() => _currentStep += 1) : null;
    switch (_currentStep) {
      case 0:
        {
          setState(() {
            _filteredFundTransferOwnDestinations = _fundTransferOwnDestinations
                .where((element) =>
                    element.accountNumber.toLowerCase() !=
                    _selectedSource.accountNumber.toLowerCase())
                .toList();

            _userFundTransferDetails.source = _selectedSource;

            _currentStep += 1;
          });
        }

        break;
      case 1:
        {
          setState(() {
            _isStepTwoClicked = false;
            _isStepTwoClicked = true;
          });

          if (_radioValue1 != '') {
            setState(() {
              /* _isStepTwoClicked = false;
              _isStepTwoClicked = true; */
              _userFundTransferDetails.transferType = _radioValue1;
              _userFundTransferDetails.transferTypeFull = _transferType;
              _formKey.currentState.reset();

              FocusScope.of(context).requestFocus(new FocusNode());
              _currentStep += 1;
            });
          }
        }
        break;
      case 2:
        {
          // print(_ownTransferToClass.productName);
          FocusScope.of(context).requestFocus(new FocusNode());
          if (_formKey.currentState.validate()) {
            _formKey.currentState.save();

            setState(() {
              _currentStep += 1;
            });
          }
        }
        break;
      case 3:
        {
          /* return showDialog(
            context: context,
            barrierDismissible: false,
            barrierColor: Colors.black.withOpacity(.80),
            builder: (BuildContext context) {
              return otpDialog(context);
            },
          ); */
        }
        break;

      default:
    }
  }

  cancel() {
    /* if (_currentStep - 1 == 2) {
      _ownTransferTo = null;
      _ownTransferToClass = null;
      amountController.clear();
      remarksController.clear();

      otherAccountNumberController.clear();
      otherAccountNameController.clear();
      otherAmountController.clear();
      otherRemarksController.clear();

      codeController.clear();
      beneAccountNumberController.clear();
      beneAccountNameController.clear();
      beneAddressController.clear();
      WidgetsBinding.instance
          .addPostFrameCallback((_) => _formKey.currentState.reset());
    } */

    switch (_currentStep) {
      case 0:
        {}
        break;
      case 1:
        {}
        break;
      case 2:
        {
          _clearForm();
        }
        break;
      case 3:
        {}
        break;

      default:
    }

    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
  }

  otpDialog(BuildContext context) {
    //===================== VARIABLES FOR OTP Dialog Field
    bool hasError = false;
    String currentText = "";
    final otpFormKey = GlobalKey<FormState>();
    TextEditingController textEditingController = TextEditingController();
    StreamController<ErrorAnimationType> errorController;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 400),
          child: AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(15.0))),
            // contentPadding: EdgeInsets.all(32.0),
            actionsPadding: EdgeInsets.all(15),
            title: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(20),
                  child: Lottie.asset(
                    'assets/images/otp-auth-3.json',
                    alignment: Alignment.center,
                    height: 200,
                    width: 200,
                  ),
                ),
                Text(
                  "OTP Verification".toUpperCase(),
                  style: TextStyle(
                      color: Color(0xFF00238A),
                      fontSize: kDialogHeaderFontSize,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1),
                  textAlign: TextAlign.center,
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child: Column(children: [
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        "Enter the 5-digit authentication code sent to your registered mobile number ending with",
                        style: TextStyle(
                          // color: Color(0xFF00238A),
                          fontSize: kParagraphFontSize,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      child: Text(
                        "+63 9** *** 4563",
                        style: TextStyle(
                          // color: Color(0xFF00238A),
                          fontSize: kParagraphFontSize,
                          letterSpacing: 1,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ]),
                ),
              ],
            ),
            content: Form(
              key: otpFormKey,
              child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20),
                  child: PinCodeTextField(
                    appContext: context,
                    pastedTextStyle: TextStyle(
                      color: kPrimaryColorFaded2,
                      fontWeight: FontWeight.bold,
                    ),
                    length: 5,
                    cursorHeight: 15,

                    obscureText: true,
                    obscuringCharacter: '*',
                    textStyle: TextStyle(color: kPrimaryColor),
                    obscuringWidget: Icon(
                      Icons.lens_rounded,
                      color: kPrimaryColor,
                      size: 14,
                    ),
                    autoFocus: true,
                    textInputAction: TextInputAction.done,
                    useHapticFeedback: true,
                    blinkWhenObscuring: true,
                    blinkDuration: Duration(milliseconds: 1500),
                    animationType: AnimationType.fade,
                    validator: (v) {
                      if (v != "12345" && v.length == 5) {
                        return "Incorrect Code";
                      } else {
                        return null;
                      }
                    },
                    pinTheme: PinTheme(
                      shape: PinCodeFieldShape.underline,
                      // borderRadius: BorderRadius.circular(5),
                      /* fieldHeight: 50,
                      fieldWidth: 40, */

                      activeFillColor: hasError ? kPrimaryColor : Colors.white,
                      activeColor: kPrimaryColor,
                      selectedColor: kPrimaryColor,
                      selectedFillColor: kWhiteColor,
                      inactiveFillColor: kWhiteColor,
                      inactiveColor: kPrimaryColorFaded2,
                    ),
                    cursorColor: kPrimaryColor,
                    animationDuration: Duration(milliseconds: 300),
                    enableActiveFill: true,
                    errorAnimationController: errorController,
                    controller: textEditingController,
                    keyboardType: TextInputType.number,
                    /* boxShadows: [
                      BoxShadow(
                        offset: Offset(0, 1),
                        color: Colors.black12,
                        blurRadius: 10,
                      )
                    ], */
                    onCompleted: (v) {
                      print("Completed");

                      if (v == "12345") {
                        Future.delayed(Duration(milliseconds: 1500), () {
                          setState(() {
                            Navigator.of(context).pop();

                            return showDialog(
                              context: context,
                              barrierDismissible: false,
                              barrierColor: Colors.black.withOpacity(.90),
                              builder: (BuildContext context) {
                                return ProgressIndicatorWidget();
                              },
                            );
                          });
                        });
                      }
                    },
                    // onTap: () {
                    //   print("Pressed");
                    // },
                    onChanged: (value) {
                      print(value);
                      setState(() {
                        currentText = value;
                      });
                    },
                    beforeTextPaste: (text) {
                      print("Allowing to paste $text");
                      //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                      //but you can show anything you want here, like your pop up saying wrong paste format or etc
                      return true;
                    },
                    errorTextSpace: 20,
                  )),
            ),
            actions: [
              Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: double.maxFinite,
                      margin: EdgeInsets.only(
                          left: 20, right: 20, top: 5, bottom: 5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Didn't receive the code? ",
                            style: TextStyle(
                              fontSize: kParagraphFontSize - 2,
                            ),
                          ),
                          Text(
                            "Resend".toUpperCase(),
                            style: TextStyle(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.bold,
                              fontSize: kParagraphFontSize - 2,
                            ),
                          ),
                        ],
                      ),
                      /* ElevatedButton(
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          primary: kPrimaryColor,
                          padding: EdgeInsets.all(25),
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(kButtonCircularRadius),
                          ),
                        ),
                        child: Text(
                          'Proceed',
                          style: TextStyle(
                            color: kWhiteColor,
                            fontSize: 15.5,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1,
                          ),
                        ),
                      ), */
                    ),
                  ]),
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Container(
            child: CircleAvatar(
              backgroundColor: Colors.blueGrey.withOpacity(0.30),
              radius: 20,
              child: Icon(
                Icons.close_rounded,
                color: kWhiteColor,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future<List<FundTransferOwnDestinationClass>> getData(filter) async {
    if (_filteredFundTransferOwnDestinations.length > 0) {
      if (filter.length > 0) {
        return _filteredFundTransferOwnDestinations
            .where((i) => i.productName.toLowerCase().contains(filter))
            .toList();
      } else {
        return _filteredFundTransferOwnDestinations;
      }
    } else {
      return [];
    }
  }

  List<FundTransferOwnDestinationClass> getDataFundTransferOwn(filter) {
    if (_filteredFundTransferOwnDestinations.length > 0) {
      if (filter.length > 0) {
        return _filteredFundTransferOwnDestinations
            .where((i) => i.productName.toLowerCase().contains(filter))
            .toList();
      } else {
        return _filteredFundTransferOwnDestinations;
      }
    } else {
      return [];
    }
  }
}

Widget _customDropDownExample(BuildContext context,
    FundTransferOwnDestinationClass item, String itemDesignation) {
  // return Container();
  if (item == null) {
    return Container();
  } else {
    // return Container();
    return Container(
      child: ListTile(
        contentPadding: EdgeInsets.all(0),
        title: Text((item.productName != null) ? item.productName : ''),
        subtitle: Text((item.accountNumber != null) ? item.accountNumber : ''),
      ),
    );
  }
}

Widget _customPopupItemBuilderExample(BuildContext context,
    FundTransferOwnDestinationClass item, bool isSelected) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 8),
    /* decoration: !isSelected
        ? null
        : BoxDecoration(
            border: Border.all(color: Theme.of(context).primaryColor),
            borderRadius: BorderRadius.circular(5),
            color: Colors.white,
          ), */
    child: ListTile(
      selected: isSelected,
      title: Text(item.productName),
      subtitle: Text(item.accountNumber),
    ),
  );
}

confirmationDialog(BuildContext context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.0))),
        // contentPadding: EdgeInsets.all(32.0),
        actionsPadding: EdgeInsets.all(15),
        title: Text(
          "Confirmation".toUpperCase(),
          style: TextStyle(
              color: Color(0xFF00238A),
              fontSize: kDialogHeaderFontSize,
              fontWeight: FontWeight.bold,
              letterSpacing: 1),
          textAlign: TextAlign.center,
        ),
        content: Text(
          '''You are about to process this fund transfer transaction. Please double check the transaction details before proceeding.
          
          Click the proceed button in order to continue this process.''',
          maxLines: 20,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: kParagraphFontSize),
        ),
        actions: [
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: double.maxFinite,
                  margin:
                      EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      primary: kPrimaryColor,
                      padding: EdgeInsets.all(25),
                      shape: RoundedRectangleBorder(
                        borderRadius:
                            BorderRadius.circular(kButtonCircularRadius),
                      ),
                    ),
                    child: Text(
                      'Proceed',
                      style: TextStyle(
                        color: kWhiteColor,
                        fontSize: 15.5,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1,
                      ),
                    ),
                  ),
                ),
              ]),
        ],
      ),
      GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
          child: CircleAvatar(
            backgroundColor: Colors.grey.withOpacity(0.10),
            radius: 20,
            child: Icon(
              Icons.close_rounded,
              color: kWhiteColor,
            ),
          ),
        ),
      ),
    ],
  );
}

class BuildMoreLoadingSkeleton extends StatelessWidget {
  const BuildMoreLoadingSkeleton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // skeletonInstance(paddingLeft: 20),
        Container(
          // alignment: Alignment.centerLeft,\
          margin: EdgeInsets.only(top: 20),
          width: MediaQuery.of(context).size.width * 0.20,
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: skeletonInstance(),
        ),
        buildRecentTransactionsLazyLoader(),
        buildRecentTransactionsLazyLoader(),
        buildRecentTransactionsLazyLoader(),
      ],
    );
  }
}

class BuildFirstLoadingSkeleton extends StatefulWidget {
  const BuildFirstLoadingSkeleton({
    Key key,
  }) : super(key: key);

  @override
  _BuildFirstLoadingSkeletonState createState() =>
      _BuildFirstLoadingSkeletonState();
}

class _BuildFirstLoadingSkeletonState extends State<BuildFirstLoadingSkeleton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            // alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width * 0.20,
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: skeletonInstance(),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            /* padding: EdgeInsets.symmetric(horizontal: 20), */
            decoration: BoxDecoration(
              color: kWhiteColor,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.1),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
            ),
            child: Column(
              children: [
                // skeletonInstance(paddingLeft: 20),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            // alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width * 0.20,
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: skeletonInstance(),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            /* padding: EdgeInsets.symmetric(horizontal: 20), */
            decoration: BoxDecoration(
              color: kWhiteColor,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.1),
                  spreadRadius: 1,
                  blurRadius: 1,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
            ),
            child: Column(
              children: [
                // skeletonInstance(paddingLeft: 20),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
                kBuildRecentTransactionsDashboardLazyLoader(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

Row buildRecentTransactionsLazyLoader() {
  return Row(
    children: [
      Container(
        margin: EdgeInsets.all(20),
        child: SizedBox(
          width: 50,
          height: 50,
          child: skeletonInstance(),
        ),
      ),
      Expanded(
        child: Container(
          padding: EdgeInsets.only(top: 5),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 5, right: 20),
                child: skeletonInstance(),
              ),
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 10, right: 20),
                child: skeletonInstance(),
              ),
            ],
          ),
        ),
      )
    ],
  );
}

Container recentTransactions(String transDetail, String transtType,
    double transAmount, String transSign, String transDate) {
  IconData iconAssetPath;
  String amount = transSign +
      ' ₱ ' +
      NumberFormat.decimalPattern().format(transAmount).toString();

  print(amount);

  /* if (transtType == 'Fund Transfer') {
    iconAssetPath = 'assets/images/trans-fund-transfer.png';
  } else if (transtType == 'Bills Payment') {
    iconAssetPath = 'assets/images/trans-bills-payment.png';
  } else if (transtType == 'Cash In') {
    iconAssetPath = 'assets/images/trans-cash-in.png';
  } else if (transtType == 'Deposit') {
    iconAssetPath = 'assets/images/trans-fund-transfer.png';
  } else if (transtType == 'Withdrawal') {
    iconAssetPath = 'assets/images/trans-cash-in.png';
  } */

  /* if (transSign == '-') {
      iconAssetPath = 'assets/images/withdrawal.png';
    } else if (transSign == '+') {
      iconAssetPath = 'assets/images/deposit.png';
    } */

  if (transSign == '-') {
    iconAssetPath = TcMobappIcons.withdrawal;
  } else if (transSign == '+') {
    iconAssetPath = TcMobappIcons.deposit;
  }

  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.only(right: 15),
          decoration: BoxDecoration(
            color: kOffWhiteColor,
            borderRadius: BorderRadius.circular(15),
          ),
          child: ClipRRect(
            child: Icon(
              iconAssetPath,
              size: 20,
              color: Colors.grey,
            ),
          ),
        ),
        Expanded(
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            dense: true,
            title: Text(transDetail,
                style: TextStyle(color: kPrimaryColor, fontSize: 12)),
            subtitle: Text(
              transtType,
              style: TextStyle(
                fontSize: 11.5,
                color: kTransactionSubtitleColor,
              ),
            ),
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    margin: EdgeInsets.only(bottom: 2),
                    child: Text(amount,
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: (transSign == '+')
                                ? Colors.green
                                : Colors.red))),
                Container(
                  margin: EdgeInsets.only(top: 2),
                  child: Text(
                    transDate,
                    style: TextStyle(
                      fontSize: 11.5,
                      color: kTransactionSubtitleColor,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

Container buildGroupedTransactions(
    List<GroupedTransactions> groupedTransactions) {
  return Container(
      // margin: EdgeInsets.only(top: 15),
      child: Column(
    children: [
      for (var i in groupedTransactions)
        Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 20),
              alignment: Alignment.centerLeft,
              child: Text(
                (DateFormat.yMMMMd().format(i.date) ==
                        DateFormat.yMMMMd().format(DateTime.now()))
                    ? "Today"
                    : DateFormat.yMMMd().format(i.date).toString(),
                style: TextStyle(
                    fontSize: 14,
                    color: kPrimaryColorFaded2,
                    fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              /* padding: EdgeInsets.symmetric(
                                                          horizontal: 20, vertical: 10), */
              decoration: BoxDecoration(
                color: kWhiteColor,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.1),
                    spreadRadius: 1,
                    blurRadius: 1,
                    offset: Offset(0, 1), // changes position of shadow
                  ),
                ],
              ),
              child:
                  /* Container(), */ Column(
                children: i.transactions.map<Widget>(
                  (e) {
                    return Column(children: [
                      recentTransactions(
                        e.title.toString(),
                        e.category.toString(),
                        e.amount,
                        (e.balanceLocator == "Credit") ? "+" : "-",
                        DateFormat.yMMMd().format(e.trandate).toString(),
                      ),
                      Divider(
                        color: kOffWhiteColor,
                        height: 1,
                        thickness: 1,
                      ),
                    ]);
                  },
                ).toList(),
              ),
            ),
          ],
        ),
    ],
  ));
}
