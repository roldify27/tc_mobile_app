import 'package:flutter/material.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/screens/dashboard.dart';
import 'package:tcmobapp/screens/membership_verification.dart';
import 'package:tcmobapp/widgets/rounded_text_widget.dart';

class TermsConditions extends StatefulWidget {
  static String routeName = '/TermsConditions';

  @override
  _TermsConditionsState createState() => _TermsConditionsState();
}

class _TermsConditionsState extends State<TermsConditions> {
  @override
  Widget build(BuildContext context) {
    var devSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
          child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  // decoration: BoxDecoration(color: Colors.red),
                  // padding: EdgeInsets.only(left: 10),
                  child: IconButton(
                    icon: Icon(Icons.clear_rounded),
                    color: Colors.grey,
                    onPressed: () {
                      Navigator.of(context).popUntil((route) => route
                          .isFirst); //-->context here is this widget's position in the widget tree.
                    },
                  ),
                  alignment: Alignment.center,
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  alignment: Alignment.center,
                  // decoration: BoxDecoration(color: Colors.blue),
                  padding: EdgeInsets.only(top: 35),
                  child: Text(
                    'Terms & Condition',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: kHeaderTitleFontSize,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  alignment: Alignment.center,
                  // decoration: BoxDecoration(color: Colors.yellow),
                  padding: EdgeInsets.only(top: 30),
                  child: Text(
                    '',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 40,
                      fontWeight: FontWeight.bold,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.fromLTRB(55, 20, 55, 10),
            child: Text(
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer pretium, lacus, nibh donec. Sit sed lobortis felis fames rutrum dui. Egestas duis et pellentesque ac ut suscipit. Condimentum non tortor sagittis diam purus, neque massa. Dolor pellentesque et tellus tortor, non. Aliquet non sit arcu id nulla in arcu eu commodo. Elementum aliquam tempor justo, nunc, metus, dui facilisis tellus. Pellentesque faucibus pulvinar auctor at metus lobortis. Lacus vel semper sit nam cras nunc, lorem',
              style: TextStyle(
                color: Colors.black,
                fontSize: 13,
                height: 1.6,
              ),
              textAlign: TextAlign.justify,
            ),
          ),
          Expanded(child: Container()),
          Container(
            padding: EdgeInsets.only(
                top: 50,
                left: devSize.width * 0.05,
                right: devSize.width * 0.05),
            width: double.infinity,
            child: RaisedButton(
              padding: EdgeInsets.all(23),
              onPressed: () {
                Navigator.of(context)
                    .pushNamed(MembershipVerification.routeName);
              },
              color: Color(0xFF00238A),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(kButtonCircularRadius),
                  side: BorderSide(color: Color(0xFF00238A))),
              child: Text(
                'Agree',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15.5,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                top: 20,
                bottom: 40,
                left: devSize.width * 0.05,
                right: devSize.width * 0.05),
            width: double.infinity,
            child: TextButton(
              child: Text(
                'Disagree',
                style: TextStyle(
                    color: Color(0xFF00238A),
                    fontSize: 15.5,
                    fontWeight: FontWeight.bold),
              ),
              onPressed: () {
                // print('Pressed');
                Navigator.of(context).pop();
              },
            ),
          ),
        ],
      )),
    );
  }
}
