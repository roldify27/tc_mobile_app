import 'package:flutter/material.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/login/login_class.dart';
import 'package:tcmobapp/screens/dashboard.dart';
import 'package:tcmobapp/screens/forgotpassword_verification.dart';
import 'package:tcmobapp/screens/terms_condition.dart';
import 'package:tcmobapp/widgets/rounded_text_widget.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:tcmobapp/provider/login/login_class.dart';
import 'package:tcmobapp/provider/login/login_provider.dart';
import 'package:provider/provider.dart';
import 'package:tcmobapp/widgets/prompts/prompt_dialogs.dart';

class LoginScreen extends StatefulWidget {
  static String routeName = '/Login';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  var username_controller = TextEditingController();
  var password_controller = TextEditingController();

  var _isFocused_Username = false;
  var _isFocused_Password = false;
  bool _isLoginClicked = false;

  FocusNode username_focusnode = new FocusNode();
  FocusNode password_focusnode = new FocusNode();

  bool _passwordVisible;

  LoginClass inputUserLoginCredentials = new LoginClass();

  Future<void> _loginUser(LoginClass userLoginCredentials) async {
    // await Provider.of<LoginProvider>(context, listen: false).setIsLoading();

    setState(() {
      _isLoginClicked = true;
    });

    var loginProvider =
        await Provider.of<LoginProvider>(context, listen: false);

    try {
      // await new Future.delayed(const Duration(seconds: 5));
      // throw 500;
      String result =
          await loginProvider.checkAuthenticateUser(userLoginCredentials);

      print("RESULT: ${result}");

      if (result == 'Incorrect Credentials') {
        await showDialog(
          context: context,
          builder: (ctx) => PromptAlertDialogNotify(
            title: "Account Not Found!",
            subtitle:
                "Data corresponding to the entered credentials was not found.",
            okText: "Okay",
          ),
        );
      } else {
        Navigator.pushReplacementNamed(context, Dashboard.routeName);
      }
    } catch (error) {
      print("ERROR: ${error}");
      await showDialog(
        context: context,
        builder: (ctx) => PromptAlertDialogError(
          title: "System Error!",
          subtitle:
              "There is something wrong happened within the server. Please try again.",
          okText: "Okay",
        ),
      );
    } finally {
      setState(() {
        _isLoginClicked = false;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    username_focusnode.addListener(() {
      setState(() {
        // FocusScope.of(context).requestFocus(username_focusnode);
        _isFocused_Username = username_focusnode.hasFocus;
      });
    });

    password_focusnode.addListener(() {
      setState(() {
        // FocusScope.of(context).requestFocus(password_focusnode);
        _isFocused_Password = password_focusnode.hasFocus;
      });
    });
    _passwordVisible = false;
  }

  /* void _handleFocusChanged() {
    setState(() {
      FocusScope.of(context).requestFocus(password_focusnode);
    });
    if (!password_focusnode.hasFocus) {
      FocusScope.of(context).requestFocus(new FocusNode());
    }
  } */

  @override
  void dispose() {
    // TODO: implement dispose
    username_focusnode.removeListener(() {});
    password_focusnode.removeListener(() {});
    username_focusnode.dispose();
    password_focusnode.dispose();
    username_controller.dispose();
    password_controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var devSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 15, right: 30),
              child: IconButton(
                icon: Icon(Icons.help_outline_sharp),
                color: Color(0xFF00238A),
                tooltip:
                    "If you have queries you can go to our support by clicking this button",
                onPressed: () => showDialog(
                  context: context,
                  builder: (context) => SupportInquiry(),
                  // barrierDismissible: false,
                ),
              ),
              alignment: Alignment.topRight,
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Image.asset(
                'assets/images/tc-logo-mockup.png',
                height: 140,
                width: 140,
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  top: 25,
                  left: devSize.width * 0.10,
                  right: devSize.width * 0.10),
              child: Form(
                key: _formKey,
                child: SingleChildScrollView(
                    child: Column(
                  children: [
                    TextFormField(
                      // obscureText: !_passwordVisible,
                      textInputAction: TextInputAction.next,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      controller: username_controller,
                      focusNode: username_focusnode,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Username *',
                        // hintText: 'Enter Your Username Here',
                        labelStyle: TextStyle(
                            color: _isFocused_Username
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                        /* suffixIcon: Visibility(
                          visible: password_focusnode.hasFocus,
                          child: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              _passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Color(0xFF00238A),
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            },
                          ),
                        ), */
                      ),
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Username is required!';
                        }
                      },
                      onSaved: (val) => setState(
                          () => inputUserLoginCredentials.userName = val),
                      onFieldSubmitted: (val) => setState(() =>
                          kFieldFocusChange(
                              context, username_focusnode, password_focusnode)),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      textInputAction: TextInputAction.done,
                      obscureText: !_passwordVisible,
                      controller: password_controller,
                      focusNode: password_focusnode,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Password *',
                        // hintText: 'Enter Your Password Here',
                        labelStyle: TextStyle(
                            color: _isFocused_Password
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                        suffixIcon: Visibility(
                          visible: password_focusnode.hasFocus,
                          child: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              _passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Color(0xFF00238A),
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            },
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Password is required!';
                        }
                      },
                      onSaved: (val) => setState(
                          () => inputUserLoginCredentials.passWord = val),
                      onFieldSubmitted: (val) => setState(() => _loginAction()),
                    ),
                    Container(
                        padding: EdgeInsets.all(13),
                        alignment: Alignment.centerRight,
                        child: TextButton(
                          child: Text(
                            'Forgot Your Password?',
                            style: TextStyle(
                              fontSize: 11.5,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF00238A),
                            ),
                          ),
                          onPressed: () {
                            // print('Pressed');
                            Navigator.of(context).pushNamed(
                                ForgotPasswordVerification.routeName);
                          },
                        )),
                  ],
                )),
              ),
            ),

            // Image(image: AssetImage('../assets/img/tc-logo-mockup.png')),
            // Text('This is Login Page for Testing'),
            Expanded(child: Container()),
            Container(
              height: 60,
              width: double.maxFinite,
              margin: EdgeInsets.only(left: 20, right: 20, top: 5),
              child: ElevatedButton(
                onPressed: () {
                  _loginAction();
                },
                style: ElevatedButton.styleFrom(
                  primary: kPrimaryColor,
                  padding: EdgeInsets.all(25),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(kButtonCircularRadius),
                  ),
                ),
                child: (_isLoginClicked)
                    ? Container(
                        // decoration: (BoxDecoration(color: Colors.red)),
                        // padding: EdgeInsets.all(20),
                        child: kButtonProgressIndicator(),
                      )
                    : Text(
                        'Log In',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.5,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1),
                      ),
              ),
            ),
            /* Container(
              padding: EdgeInsets.only(
                  top: 50,
                  left: devSize.width * 0.05,
                  right: devSize.width * 0.05),
              width: double.infinity,
              child: RaisedButton(
                padding: EdgeInsets.all(23),
                onPressed: () {
                  // Navigator.pushReplacementNamed(context, Dashboard.routeName);
                  _loginAction();
                },
                color: Color(0xFF00238A),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(kButtonCircularRadius),
                    side: BorderSide(color: Color(0xFF00238A))),
                child: (_isLoginClicked)
                    ? Container(
                        // decoration: (BoxDecoration(color: Colors.red)),
                        // padding: EdgeInsets.all(20),
                        child: kButtonProgressIndicator(),
                      )
                    : Text(
                        'Log In',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.5,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1),
                      ),
              ),
            ), */
            Container(
                padding: EdgeInsets.only(
                    top: 20,
                    bottom: 40,
                    left: devSize.width * 0.05,
                    right: devSize.width * 0.05),
                child: TextButton(
                  child: Text(
                    'Register',
                    style: TextStyle(
                        color: Color(0xFF00238A),
                        fontSize: 15.5,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    // print('Pressed');
                    FocusScope.of(context).requestFocus(new FocusNode());
                    Navigator.of(context).pushNamed(TermsConditions.routeName);
                  },
                ))
          ],
        ),
      ),
    );
  }

  _loginAction() {
    if (_isLoginClicked == false) {
      FocusScope.of(context).requestFocus(new FocusNode());
      if (_formKey.currentState.validate()) {
        _formKey.currentState.save();
        print(inputUserLoginCredentials.userName);
        print(inputUserLoginCredentials.passWord);
        _loginUser(inputUserLoginCredentials);
      }
    }
  }
}

_launchURL() async {
  const url = 'https://www.facebook.com/OfficialPageTagumCoop/';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

class SupportInquiry extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      contentPadding: EdgeInsets.all(32.0),
      title: Text(
        "SUPPORT INQUIRY",
        style: TextStyle(
            color: Color(0xFF00238A),
            fontSize: kDialogHeaderFontSize,
            fontWeight: FontWeight.bold,
            letterSpacing: 1),
        textAlign: TextAlign.center,
      ),
      content: Text(
        'If you have questions about something you can ask our support by clicking the button below.',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: kParagraphFontSize),
      ),
      actions: [
        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: double.maxFinite,
                margin: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                child: RaisedButton(
                  padding:
                      EdgeInsets.only(left: 20, right: 20, top: 17, bottom: 17),
                  onPressed: _launchURL,
                  color: Color(0xFF00238A),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(23.5),
                      side: BorderSide(color: Color(0xFF00238A))),
                  child: Text(
                    'Ask our Support Here!',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.5,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                  ),
                ),
              ),
              Container(
                  // width: double.maxFinite,
                  margin:
                      EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                  child: TextButton(
                    child: Text(
                      'Cancel',
                      style: TextStyle(
                          color: Color(0xFF00238A),
                          fontSize: 15.5,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: () {
                      // print('Pressed');
                      Navigator.of(context).pop();
                    },
                  )) /* RaisedButton(
                  padding:
                      EdgeInsets.only(left: 20, right: 20, top: 17, bottom: 17),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  color: Colors.white54,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(23.5),
                      side: BorderSide(color: Colors.white54)),
                  child: Text(
                    'Cancel',
                    style: TextStyle(
                        color: kPrimaryColor,
                        fontSize: 15.5,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1),
                  ),
                ), 
              ), */
            ]),
      ],
    );
  }
}
