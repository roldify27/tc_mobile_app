import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/savings_details/grouped_transactions_class.dart';
import 'package:tcmobapp/provider/user/dashboard_recent_transaction_class.dart';
import 'package:tcmobapp/screens/savings_details.dart';
import 'package:tcmobapp/provider/savings_type.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tcmobapp/widgets/menu_icons.dart';
import 'package:tcmobapp/widgets/prompts/prompt_dialogs.dart';
import 'login_screen.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:tcmobapp/screens/fund_transfer.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:tcmobapp/provider/idle_provider.dart';
import 'package:tcmobapp/widgets/dashboard/build_dashboard_header_widget.dart';
import 'package:tcmobapp/widgets/dashboard/build_dashboard_drawer_widget.dart';
import 'package:tcmobapp/widgets/dashboard/build_dashboard_menu_widget.dart';
import 'package:tcmobapp/widgets/dashboard/build_dashboard_recent_transactions_widget.dart';
import 'package:tcmobapp/provider/user/user_details_provider.dart';
import 'package:tcmobapp/provider/user/user_details_class.dart';

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

class Dashboard extends StatefulWidget {
  static String routeName = '/Dashboard';

  Dashboard({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> with WidgetsBindingObserver {
  // ============================================ START OF VARIABLES
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<_DashboardState> _dashboardGlobalKey =
      new GlobalKey<_DashboardState>();
  final LocalStorage localStorage = new LocalStorage('mobile_app');
  PageController _pageController;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  UserDetailsClass userDetails = new UserDetailsClass(
      cid: "",
      email: "",
      fname: "",
      isAdmin: false,
      memberClass: "",
      savingsTotal: 0,
      totalPoints: 0,
      username: "");

  // List<GroupedTransactions> dateGroupedTrans = [];

  int isChosen = 0;
  String dropdownValue = 'One';
  final _inactivityTimeout = Duration(seconds: 3);
  Timer _keepAliveTimer;
  bool _isInit = true;
  Future<bool> _hasLoaded;

  // ===========================================END OF VARIABLES

  // ============================================ START OF OVERRIDE FUNCTIONS
  /*  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('sure nakaabot ko diri.');
    // TODO: implement didChangeAppLifecycleState

    if (state == AppLifecycleState.inactive) {
      // Navigator.of(context).popUntil((route) => route.isFirst);
      // Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);

      try {
        Navigator.of(context)
            .pushNamedAndRemoveUntil(LoginScreen.routeName, (route) => false);
      } catch (e) {
        Navigator.of(context).pushNamed(LoginScreen.routeName);
      }
    }

    if (state == AppLifecycleState.paused) {
      print('i went for a pause for a while.');
    }

    super.didChangeAppLifecycleState(state);
  } */

  @override
  didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        _keepAlive(true);
        break;
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
        _keepAlive(false); // Conservatively set a timer on all three
        break;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController(
      initialPage: 0,
    );
    // print("JWT Auth Login Page: ${localStorage.getItem('jwtauth')}");
    WidgetsBinding.instance.addObserver(this);
    /* SchedulerBinding.instance.addPostFrameCallback(
        (_) => kShowLoadingLogoOnly(_scaffoldKey.currentContext)); */
    _hasLoaded = _syncLoad();
  }

  @override
  void didChangeDependencies() {
    /* if (_isInit) {
      _getUserDetails();
      _getDashboardRecentTransactions();
    }
    _isInit = false; */

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  // ============================================ END OF OVERRIDE FUNCTIONS

  // ============================================ START OF FUNCTIONS
  void printTest() {
    print("Testing Global Key!");
  }

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()

    if (mounted) {}
    _refreshController.loadComplete();
  }

  void _keepAlive(bool visible) {
    _keepAliveTimer?.cancel();
    if (visible) {
      _keepAliveTimer = null;
    } else {
      _keepAliveTimer = Timer(_inactivityTimeout, () {
        /* try {
          Navigator.of(context)
              .pushNamedAndRemoveUntil(LoginScreen.routeName, (route) => false);
        } catch (e) {
          Navigator.of(context).pushNamed(LoginScreen.routeName);
        } */
        // inactivityDialog(context);
        showDialog(
          context: context,
          barrierDismissible: false,
          barrierColor: kDialogModalBarrierColor,
          builder: (ctx) => PromptAlertSessionTimeout(),
        );
      });
    }
  }

  /// Must be called only when app is visible, and exactly once
  void startKeepAlive() {
    assert(_keepAliveTimer == null);
    _keepAlive(true);
    // WidgetsBinding.instance.addObserver(_KeepAliveObserver());
  }

  // ============================================ END OF FUNCTIONS

  // ============================================ START OF API CALLS
  Future<bool> _syncLoad() async {
    // kShowLoadingLogoOnly(context);
    await _getUserDetails();
    await _getDashboardRecentTransactions();
    return true;
  }

  Future<void> _getUserDetails() async {
    // await Provider.of<LoginProvider>(context, listen: false).setIsLoading();

    await Future.delayed(Duration(seconds: 3), () async {
      // 5s over, navigate to a new page
    });

    var userDetailsProvider =
        await Provider.of<UserDetailsProvider>(context, listen: false);

    try {
      // await new Future.delayed(const Duration(seconds: 5));
      await userDetailsProvider.getUserDetailsAPI();
      userDetails = await userDetailsProvider.getUserDetails;
      await _getDashboardRecentTransactions();
    } catch (error) {
      print("ERROR: ${error}");
      await kErrorApiDialog;
    } finally {}
  }

  Future<void> _getDashboardRecentTransactions() async {
    // await Provider.of<LoginProvider>(context, listen: false).setIsLoading();

    var userDetailsProvider =
        await Provider.of<UserDetailsProvider>(context, listen: false);

    try {
      // await new Future.delayed(const Duration(seconds: 5));
      await userDetailsProvider.getDashboardRecentTransactions();
    } catch (error) {
      print("ERROR: ${error}");
    } finally {}
  }
  // ============================================ END OF API CALLS

  @override
  Widget build(BuildContext context) => FutureBuilder(
      initialData: false,
      future: _hasLoaded,
      builder: (context, snapshot) {
        print("Snapshot: ${snapshot.hasData}");
        /* return (snapshot.hasData == true && snapshot.data == true)
            ? _buildWidget(snapshot.hasData)
            : AnimatedOpacity(
                opacity: (snapshot.data != true) ? 1 : 0,
                duration: Duration(milliseconds: 300),
                child: _buildLoading(snapshot.hasData),
              ); */

        return (Stack(
          clipBehavior: Clip.antiAliasWithSaveLayer,
          children: [
            _buildWidget(snapshot.hasData),
            Visibility(
              visible: (snapshot.data != true) ? true : false,
              child: _buildLoading(snapshot.hasData),
            ),
          ],
        ));

        /*  return AnimatedCrossFade(
          firstCurve: Curves.easeInOut,
          secondCurve: Curves.easeInOut,
          firstChild: _buildLoading(snapshot.hasData),
          secondChild: _buildWidget(snapshot.hasData),
          crossFadeState: (snapshot.data != true)
              ? CrossFadeState.showFirst
              : CrossFadeState.showSecond,
          duration: Duration(milliseconds: 1000),
          // alignment: Alignment.center,
          // sizeCurve: Curves.easeInOut,
        ); */
      });

  Widget _buildLoading(bool data) {
    debugPrint('load hasData status:, $data');
    return Container(
      color: Colors.black.withOpacity(0.70),
      child: Center(
        child: Image.asset(
          "assets/images/tc-logo-looper.gif",
          height: 100.0,
          width: 100.0,
        ),
      ),
    );
  }

  Widget _buildWidget(bool data) {
    debugPrint('build hasData status:, $data');

    var userDetailsProvider =
        Provider.of<UserDetailsProvider>(context, listen: true);
    userDetails =
        Provider.of<UserDetailsProvider>(context, listen: false).getUserDetails;

    List<GroupedTransactions> groupedTransactions =
        userDetailsProvider.getGroupedTransactions;

    List<DashboardRecentTransactionClass> _items =
        userDetailsProvider.getRecentTransactions;

    var newMap = groupBy(_items, (obj) => obj.dateGroup);

    // print("newMaps: ${_items}");

    List<GroupedTransactions> newList = [];

    newMap.forEach((key, value) {
      newList.add(GroupedTransactions(
        date: DateTime.parse(key.toString()),
        transactions: value,
      ));
    });

    print("newMaps: ${newList}");

    return DefaultTabController(
      length: 3,
      child: Stack(
        children: [
          Image.asset(
            "assets/images/background-1.jpg",
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.black.withOpacity(0.55),
            colorBlendMode: BlendMode.darken,
            fit: BoxFit.cover,
          ),
          Container(
            margin:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.50),
            decoration: BoxDecoration(
              color: kOffWhiteColor,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(30),
                topLeft: Radius.circular(30),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(2, 0), // changes position of shadow
                ),
              ],
            ),
            child: new Container(),
          ),
          Scaffold(
            key: _scaffoldKey,
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.transparent,
            // backgroundColor: Color(0xFF00238A),

            drawer: Drawer(
              child: BuildDashboardDrawerWidget(
                cid: userDetails.cid,
                fname: userDetails.fname,
                photo: userDetails.photo,
                memberclass: userDetails.memberClass,
              ),
            ),
            body: SafeArea(
              child: SmartRefresher(
                enablePullDown: true,
                enablePullUp: false,
                controller: _refreshController,
                onRefresh: _onRefresh,
                onLoading: _onLoading,
                child: ListView(physics: ClampingScrollPhysics(), children: [
                  Container(
                    // height: MediaQuery.of(context).size.height,
                    child: Column(
                      children: [
                        Column(
                          children: [
                            BuildDashboardHeaderWidget(
                              scaffoldKey: _scaffoldKey,
                              totalShare: userDetails.shareTotal,
                              totalSavings: userDetails.savingsTotal,
                              totalTimeDepo: userDetails.timeDepoTotal,
                              totalLoans: userDetails.loanTotal,
                              photo: userDetails.photo,
                            ),
                            BuildDashboardMenuWidget(
                              dashboardGlobalKey: _dashboardGlobalKey,
                            ),
                          ],
                        ),
                        BuildDashboardRecentTransactionsWidget(
                          transactionList: newList,
                        ),
                      ],
                    ),
                  ),
                ]),
              ),
            ),
          )
        ],
      ),
    );
  }

  Container recentTransactions(String transDetail, String transtType,
      double transAmount, String transSign, String transDate) {
    String iconAssetPath = '';
    String amount = transSign +
        ' ₱ ' +
        NumberFormat.decimalPattern().format(transAmount).toString();

    print(amount);

    if (transtType == 'Fund Transfer') {
      iconAssetPath = 'assets/images/trans-fund-transfer.png';
    } else if (transtType == 'Bills Payment') {
      iconAssetPath = 'assets/images/trans-bills-payment.png';
    } else if (transtType == 'Cash In') {
      iconAssetPath = 'assets/images/trans-cash-in.png';
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(10),
            margin: EdgeInsets.only(right: 15),
            decoration: BoxDecoration(
              color: kOffWhiteColor,
              borderRadius: BorderRadius.circular(15),
            ),
            child: ClipRRect(
              child: Image.asset(
                iconAssetPath,
                height: 23,
                width: 23,
              ),
            ),
          ),
          Expanded(
            child: ListTile(
              contentPadding: EdgeInsets.zero,
              dense: true,
              title: Text(transDetail, style: TextStyle(color: kPrimaryColor)),
              subtitle: Text(
                transtType,
                style: TextStyle(
                  fontSize: 11.5,
                  color: kPrimaryColorFaded,
                ),
              ),
              trailing: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.only(bottom: 2),
                      child: Text(amount,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: (transSign == '+')
                                  ? Colors.green
                                  : Colors.red))),
                  Container(
                    margin: EdgeInsets.only(top: 2),
                    child: Text(
                      transDate,
                      style: TextStyle(
                        fontSize: 11.5,
                        color: kPrimaryColorFaded,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
