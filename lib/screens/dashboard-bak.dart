import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/screens/savings_details.dart';
import 'package:tcmobapp/provider/savings_type.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:tcmobapp/widgets/menu_icons.dart';
import 'login_screen.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';

/* final List<String> imgList = [
  'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
  'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
  'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
  'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
]; */

final List<String> imgList = [
  'assets/images/advert-1.jpg',
  'assets/images/advert-2.jpg',
  'assets/images/advert-3.jpg',
  'assets/images/advert-4.jpg',
  'assets/images/advert-5.jpg',
  'assets/images/advert-6.jpg'
];

class Dashboard extends StatefulWidget {
  static String routeName = '/Dashboard';

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  PageController _pageController;
  int isChosen = 0;
  int _current = 0;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('sure nakaabot ko diri.');
    // TODO: implement didChangeAppLifecycleState

    if (state == AppLifecycleState.inactive) {
      // Navigator.of(context).popUntil((route) => route.isFirst);
      // Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);

      try {
        Navigator.of(context)
            .pushNamedAndRemoveUntil(LoginScreen.routeName, (route) => false);
      } catch (e) {
        Navigator.of(context).pushNamed(LoginScreen.routeName);
      }
    }

    if (state == AppLifecycleState.paused) {
      print('i went for a pause for a while.');
    }

    super.didChangeAppLifecycleState(state);
  }

  final List<Widget> imageSliders = imgList
      .map((item) => Container(
            child: Container(
              margin: EdgeInsets.all(0.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Stack(
                    children: <Widget>[
                      // Image.network(item, fit: BoxFit.cover, width: 1000.0),
                      Image.asset(
                        item,
                        fit: BoxFit.cover,
                        width: 1000.0,
                      ),
                      Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Color.fromARGB(200, 0, 0, 0),
                                Color.fromARGB(0, 0, 0, 0)
                              ],
                              begin: Alignment.bottomCenter,
                              end: Alignment.topCenter,
                            ),
                          ),
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 20.0),
                          // child: Text(
                          //   'No. ${imgList.indexOf(item)} image',
                          //   style: TextStyle(
                          //     color: Colors.white,
                          //     fontSize: 20.0,
                          //     fontWeight: FontWeight.bold,
                          //   ),
                          // ),
                        ),
                      ),
                    ],
                  )),
            ),
          ))
      .toList();

  List<Map<String, Object>> _menuList = [
    {
      'menuName': 'SAVINGS DETAIL',
      'icon': Icon(Icons.ac_unit, size: 40.0),
      'path': SavingsDetails.routeName
    },
    {
      'menuName': 'FUND TRANSFER',
      'icon': Icon(Icons.ac_unit, size: 40.0),
    },
    {
      'menuName': 'LOANS',
      'icon': Icon(Icons.money, size: 40.0),
    },
    {
      'menuName': 'INSURANCE',
      'icon': Icon(Icons.ac_unit, size: 40.0),
    },
    {
      'menuName': 'BILLS PAYMENT',
      'icon': Icon(Icons.ac_unit, size: 40.0),
    },
    {
      'menuName': 'LOAN CALC.',
      'icon': Icon(Icons.ac_unit, size: 40.0),
    },
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController(
      initialPage: 0,
    );
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()

    if (mounted) {}
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;

    var savingsList = SavingsList();
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: false,
        backgroundColor: kDashboardBackColor,
        // backgroundColor: Color(0xFF00238A),
        appBar: AppBar(
          actions: [
            IconButton(
              padding: EdgeInsets.only(right: 10),
              icon: new Stack(
                children: <Widget>[
                  new Icon(
                    Icons.notifications,
                    size: 27,
                    color: kDashboardBackColor,
                  ),
                  new Positioned(
                    right: 0,
                    child: new Container(
                      padding: EdgeInsets.all(1),
                      decoration: new BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 12,
                        minHeight: 12,
                      ),
                      child: new Text(
                        '!',
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
          title: Text('TC Mobile App'.toUpperCase(),
              style: TextStyle(fontSize: 20)),
          centerTitle: true,
          bottom: TabBar(
            tabs: [
              Tab(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Icon(
                    //   Icons.account_balance_wallet_rounded,
                    //   size: 18,
                    // ),
                    // SizedBox(height: 3),
                    Text('ATM',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              Tab(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Icon(
                    //   Icons.money_rounded,
                    //   size: 18,
                    // ),
                    // SizedBox(height: 3),
                    Text('Regular Savings',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              Tab(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Icon(
                    //   Icons.more_rounded,
                    //   size: 18,
                    // ),
                    // SizedBox(height: 3),
                    Text('Others',
                        style: TextStyle(
                            fontSize: 12, fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
            ],
          ),
        ),
        drawer: new Drawer(
          child: Container(
            padding: EdgeInsets.zero,
            child: Column(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Container(
                        width: double.infinity,
                        child: DrawerHeader(
                            decoration: BoxDecoration(
                              color: kPrimaryColor,
                            ),
                            child: Container(
                              // margin: EdgeInsets.all(120),
                              child: Row(
                                children: [
                                  new Stack(
                                    children: <Widget>[
                                      CircleAvatar(
                                        radius: 50,
                                        backgroundColor: Colors.grey,
                                        child: CircleAvatar(
                                          radius: 45,
                                          backgroundImage: AssetImage(
                                              'assets/images/sample-dp.png'),
                                        ),
                                      ),
                                      new Positioned(
                                        right: 0,
                                        child: new Container(
                                          padding:
                                              EdgeInsets.fromLTRB(10, 6, 10, 8),
                                          decoration: new BoxDecoration(
                                            color: Colors.red,
                                            borderRadius:
                                                BorderRadius.circular(20),
                                          ),
                                          constraints: BoxConstraints(
                                            minWidth: 12,
                                            minHeight: 12,
                                          ),
                                          child: new Text(
                                            'B',
                                            style: new TextStyle(
                                              color: Colors.white,
                                              fontSize: 15,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text('HAROLD CALIO',
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white,
                                                fontWeight: FontWeight.w600,
                                                letterSpacing: 2)),
                                        Text(
                                          '001-0123123',
                                          style: TextStyle(
                                              fontSize: 10,
                                              color: Colors.white,
                                              letterSpacing: 2),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(top: 5),
                                          child: Text(
                                            'Regular Member',
                                            style: TextStyle(
                                                fontSize: 13,
                                                color: Colors.white,
                                                letterSpacing: 1),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                            /* child: CircleAvatar(

                            backgroundImage:
                                AssetImage("assets/images/sample-dp.png"),
                          ), */
                            ),
                      ),
                      ListTile(
                        dense: true,
                        title: Text("Edit Profile"),
                        trailing: IconButton(
                            icon: Icon(Icons.chevron_right_rounded, size: 20)),
                      ),
                      ListTile(
                        dense: true,
                        title: Text("Rewards Points"),
                        subtitle: Text(
                          "42.26",
                          style: TextStyle(fontSize: 10),
                        ),
                        trailing: IconButton(
                            icon: Icon(Icons.chevron_right_rounded, size: 20)),
                      ),
                      ListTile(
                        dense: true,
                        title: Text("Job Vacancies"),
                        trailing: IconButton(
                            icon: Icon(Icons.chevron_right_rounded, size: 20)),
                      ),
                      ListTile(
                        dense: true,
                        title: Text("Terms & Conditions"),
                        trailing: IconButton(
                            icon: Icon(Icons.chevron_right_rounded, size: 20)),
                      ),
                      ListTile(
                        dense: true,
                        title: Text("Settings"),
                        trailing: IconButton(
                            icon: Icon(Icons.chevron_right_rounded, size: 20)),
                      ),
                      /* ListTile(
                        title: Text('001-0123123'),
                        subtitle: Text(
                          'HAROLD M. CALIO',
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                      ListTile(
                        title: Text('MEMBER CLASSIFICATION'),
                        subtitle: Text(
                          'B',
                        ),
                      ),
                      ListTile(
                        title: Text('REWARDS POINTS'),
                        subtitle: Text(
                          '42.26',
                        ),
                      ), */
                    ],
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.only(top: 50, bottom: 40, left: 10, right: 10),
                  width: double.infinity,
                  child: RaisedButton(
                    color: kLogoutColor,
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 16),
                    onPressed: () {
                      Navigator.of(context)
                          .pushReplacementNamed(LoginScreen.routeName);
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(23.5),
                      // side: BorderSide(color: Color(0xFF00238A))
                    ),
                    child:
                        /* IconButton(
                          icon: Icon(Icons.logout,
                              size:
                                  20)) */
                        Text(
                      'Log Out',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                          // fontWeight: FontWeight.bold,
                          letterSpacing: 1),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: Center(
          child: SmartRefresher(
            enablePullDown: true,
            enablePullUp: false,
            controller: _refreshController,
            onRefresh: _onRefresh,
            onLoading: _onLoading,
            child: Expanded(
              child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        // constraints: BoxConstraints(maxWidth: 450),
                        height: 100,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: kDashboardBackColor,
                        ),
                        child: Column(
                          children: [
                            Container(
                              height: 100,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 35, vertical: 20),
                                width: double.infinity,
                                height: 100,
                                child: TabBarView(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(5),
                                      width: double.infinity,
                                      child: Column(
                                        // crossAxisAlignment: CrossAxisAlignment.center,
                                        // mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                  padding:
                                                      EdgeInsets.only(top: 5),
                                                  child: Text(
                                                    'PHP ',
                                                    style: TextStyle(
                                                      color: kDarkerFontColor2,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  )),
                                              Text(
                                                NumberFormat.decimalPattern()
                                                    .format(3000.25),
                                                style: TextStyle(
                                                    fontSize: 35,
                                                    // fontWeight: FontWeight.bold,
                                                    color: kDarkerFontColor2),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(5),
                                      width: double.infinity,
                                      child: Column(
                                        // crossAxisAlignment: CrossAxisAlignment.center,
                                        // mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                  padding:
                                                      EdgeInsets.only(top: 5),
                                                  child: Text(
                                                    'PHP ',
                                                    style: TextStyle(
                                                        color:
                                                            kDarkerFontColor2),
                                                  )),
                                              Text(
                                                NumberFormat.decimalPattern()
                                                    .format(3000.25),
                                                style: TextStyle(
                                                    fontSize: 35,
                                                    color: kDarkerFontColor2),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.all(5),
                                      width: double.infinity,
                                      child: Column(
                                        // crossAxisAlignment: CrossAxisAlignment.center,
                                        // mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                  padding:
                                                      EdgeInsets.only(top: 5),
                                                  child: Text(
                                                    'PHP ',
                                                    style: TextStyle(
                                                        color:
                                                            kDarkerFontColor2),
                                                  )),
                                              Text(
                                                NumberFormat.decimalPattern()
                                                    .format(3000.25),
                                                style: TextStyle(
                                                    fontSize: 35,
                                                    color: kDarkerFontColor2),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Container(
                              width: double.infinity,
                              // height: double.infinity,
                              margin: EdgeInsets.only(
                                  left: 30, right: 30, top: 10, bottom: 20),
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                color: kDashboardBackColor,
                                /* border: Border.all(
                                  color: Colors.grey.withOpacity(0.50),
                                ), */
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0)),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 1,
                                    blurRadius: 3,
                                    offset: Offset(
                                        0, 4), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.symmetric(vertical: 10),
                                    width: double.infinity,
                                    child: Column(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.all(15),
                                          child: Row(
                                            children: [
                                              Icon(
                                                AppIcons.accessibility,
                                                size: 24,
                                              ),
                                              MenuIcons(
                                                iconPath:
                                                    'assets/images/savings.png',
                                                iconTitle: 'Savings Details',
                                              ),
                                              MenuIcons(
                                                iconPath:
                                                    'assets/images/savings2.png',
                                                iconTitle: 'Fund Transfer',
                                              ),
                                              MenuIcons(
                                                iconPath:
                                                    'assets/images/savings3.png',
                                                iconTitle: 'Loans',
                                              ),
                                              MenuIcons(
                                                iconPath:
                                                    'assets/images/savings4.png',
                                                iconTitle: 'Insurances',
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.all(15),
                                          child: Row(
                                            children: [
                                              MenuIcons(
                                                iconPath:
                                                    'assets/images/savings5.png',
                                                iconTitle: 'Payments',
                                              ),
                                              MenuIcons(
                                                iconPath:
                                                    'assets/images/test1.png',
                                                iconTitle: 'Time Deposit',
                                              ),
                                              MenuIcons(
                                                iconPath:
                                                    'assets/images/test2.png',
                                                iconTitle: 'Loan Calculator',
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Icon(
                                                      Icons.more_horiz_rounded,
                                                      size: 30,
                                                      color: kPrimaryColor,
                                                    ),
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      'Others',
                                                      style: TextStyle(
                                                          fontSize: 10,
                                                          /* fontWeight:
                                                              FontWeight.bold, */
                                                          color:
                                                              kDarkerFontColor2),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              width: deviceSize.width,
                              padding: EdgeInsets.only(top: 20),
                              child: Stack(children: [
                                CarouselSlider(
                                  options: CarouselOptions(

                                      // height: 250,
                                      autoPlay: true,
                                      // enlargeCenterPage: true,
                                      aspectRatio: 3,
                                      viewportFraction: 1,
                                      onPageChanged: (index, reason) {
                                        setState(() {
                                          _current = index;
                                        });
                                      }),
                                  items: imgList
                                      .map((item) => Container(
                                            child: Center(
                                                child: Image.asset(item,
                                                    fit: BoxFit.cover,
                                                    width: 1000)),
                                          ))
                                      .toList(),
                                ),
                                Positioned.fill(
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: imgList.map((url) {
                                        int index = imgList.indexOf(url);
                                        return Container(
                                          width: 8.0,
                                          height: 8.0,
                                          margin: EdgeInsets.symmetric(
                                              vertical: 10.0, horizontal: 2.0),
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: _current == index
                                                ? Color.fromRGBO(
                                                    200, 200, 200, 0.9)
                                                : Color.fromRGBO(
                                                    200, 200, 200, 0.4),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                )
                              ]),
                            ),
                            Container(
                              width: double.infinity,
                              // height: double.infinity,
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                color: kDashboardBackColor,
                                // borderRadius: BorderRadius.only(
                                //     topLeft: Radius.circular(10),
                                //     topRight: Radius.circular(10)),
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  /* border: Border.all(
                                    color: Colors.grey.withOpacity(0.50),
                                  ), */
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                ),
                                margin: EdgeInsets.all(15),
                                padding: EdgeInsets.all(20),
                                child: Column(
                                  children: [
                                    Container(
                                        child: Row(
                                      children: [
                                        Expanded(
                                            child: Align(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  'Recent Transactions',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: kDarkerFontColor2),
                                                ))),
                                        Expanded(
                                            child: Container(
                                          margin: EdgeInsets.only(right: 5),
                                          child: Align(
                                              alignment: Alignment.centerRight,
                                              child: Text(
                                                'View All',
                                                style: TextStyle(
                                                    fontSize: 12,
                                                    color: kPrimarySwatchColor,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              )),
                                        )),
                                      ],
                                    )),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 10, vertical: 10),
                                      child: Column(
                                        children: [
                                          /* Container(child: Row(children: [
                                            C
                                          ],),), */
                                          // Divider(),
                                          Row(
                                            children: [
                                              Expanded(
                                                child: ListTile(
                                                  dense: true,
                                                  title: Text(
                                                      "Transfer from your ATM to your RS"),
                                                  subtitle:
                                                      Text("Fund Transfer"),
                                                  trailing: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            bottom: 2),
                                                        child: RichText(
                                                          text: TextSpan(
                                                            text: "+ ",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .green,
                                                                fontSize: 12),
                                                            children: <
                                                                TextSpan>[
                                                              TextSpan(
                                                                  text:
                                                                      ' 435.00',
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .black
                                                                          .withOpacity(
                                                                              .65))),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 2),
                                                        child: Text(
                                                          "24 Dec 2020",
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              IconButton(
                                                  icon: Icon(
                                                Icons.chevron_right_rounded,
                                                size: 20,
                                                color: kPrimaryColor,
                                              )),
                                            ],
                                          ),
                                          Divider(),
                                          Row(
                                            children: [
                                              Expanded(
                                                child: ListTile(
                                                  dense: true,
                                                  title: Text(
                                                      "Payment to Water District Bill"),
                                                  subtitle:
                                                      Text("Bills Payment"),
                                                  trailing: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            bottom: 2),
                                                        child: RichText(
                                                          text: TextSpan(
                                                            text: "- ",
                                                            style: TextStyle(
                                                                color:
                                                                    Colors.red,
                                                                fontSize: 12),
                                                            children: <
                                                                TextSpan>[
                                                              TextSpan(
                                                                  text:
                                                                      ' 435.00',
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .black
                                                                          .withOpacity(
                                                                              .65))),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 2),
                                                        child: Text(
                                                          "24 Dec 2020",
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              IconButton(
                                                  icon: Icon(
                                                Icons.chevron_right_rounded,
                                                size: 20,
                                                color: kPrimaryColor,
                                              )),
                                            ],
                                          ),
                                          Divider(),
                                          Row(
                                            children: [
                                              Expanded(
                                                child: ListTile(
                                                  dense: true,
                                                  title: Text(
                                                      "Bank Transfer to UnionBank"),
                                                  subtitle:
                                                      Text("Fund Transfer"),
                                                  trailing: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: [
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            bottom: 2),
                                                        child: RichText(
                                                          text: TextSpan(
                                                            text: "+ ",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .green,
                                                                fontSize: 12),
                                                            children: <
                                                                TextSpan>[
                                                              TextSpan(
                                                                  text:
                                                                      ' 435.00',
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .black
                                                                          .withOpacity(
                                                                              .65))),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: EdgeInsets.only(
                                                            top: 2),
                                                        child: Text(
                                                          "24 Dec 2020",
                                                          style: TextStyle(
                                                              fontSize: 12,
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              IconButton(
                                                  icon: Icon(
                                                Icons.chevron_right_rounded,
                                                size: 20,
                                                color: kPrimaryColor,
                                              )),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
