/* Notes
- Remove Unused Packages
- Categorized Imports 
- Put Commas' for Proper Indexing*/

// DART
import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'dart:convert'; // for json encoding decoding

// PACKAGES
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tcmobapp/provider/timeout_session.dart';
import 'package:tcmobapp/screens/dashboard.dart';
import 'package:tcmobapp/widgets/prompts/prompt_dialogs.dart';
import 'package:tcmobapp/widgets/savings_info.dart';
import 'package:tcmobapp/constants.dart';

import 'package:http/http.dart' as http;

// CONSTANT
import '../constants.dart';

// MODELS
import '../provider/savings_details/savings_details_class.dart';
import '../models/savings_details/savings_details_data.dart';
import '../models/share_transactions_data.dart';

// PROVIDERS
import '../provider/savings_details/savings_details_provider.dart';

// SCREENS

// WIDGETS
import '../widgets/savings_details/savings_details_transactions_tab_widget.dart';
import '../widgets/savings_details/savings_details_header_widget.dart';
import 'package:tcmobapp/widgets/savings_details/savings_details_scrollable_cards_widget.dart';
import 'package:tcmobapp/widgets/savings_details/savings_details_recent_transactions.dart';

class SavingsDetails extends StatefulWidget {
  static const routeName = '/SavingsDetails';

  @override
  _SavingsDetailsState createState() => _SavingsDetailsState();
}

class _SavingsDetailsState extends State<SavingsDetails> {
  var tranData = ShareTransactionsData();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Timer _timer;

  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   print('initialized');
  //   _initializeTimer();
  // }

  // void _initializeTimer() {
  //   if (_timer != null) {
  //     _timer.cancel();
  //   }

  //   _timer = Timer(
  //     Duration(seconds: 10),
  //     _logOutUser,
  //   );
  // }

  // void _logOutUser() {
  //   _timer?.cancel();
  //   _timer = null;

  //   //Provider.of<TimeoutSession>(context, listen: false).triggerTimeout();
  //   Navigator.of(context)
  //       .pushNamedAndRemoveUntil(Dashboard.routeName, (route) => false);
  // }

  // void _handleUserInteraction([_]) {
  //   print('nahit ko at least');
  //   _initializeTimer();
  // }

  // @override
  // void dispose() {
  //   // TODO: implement dispose

  //   _timer?.cancel();
  //   _timer = null;
  //   super.dispose();
  // }

  var isLoading = true;
  var _isInit = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      _loadAccounts();
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  Future<void> _loadAccounts() async {
    await Provider.of<SavingsDetailsClassProvider>(context, listen: false)
        .setIsLoading();

    var savingsProvider =
        await Provider.of<SavingsDetailsClassProvider>(context, listen: false);

    try {
      // await new Future.delayed(const Duration(seconds: 5));
      await savingsProvider.fetchData();

      List<SavingsDetailsClass> accountsList = await savingsProvider.items;

      await savingsProvider
          .fetchDataTransactions(accountsList[0].accountNumber);

      await savingsProvider
          .setFocusedAccountNumber(accountsList[0].accountNumber);
    } catch (error) {
      await showDialog(
        context: _scaffoldKey.currentContext,
        builder: (ctx) => PromptAlertDialogError(
          title: "System Error!",
          subtitle:
              "There is something wrong happened within the server. Please try again.",
          okText: "Okay",
        ),
      );
      await Navigator.of(context).pop();
    } finally {
      await Provider.of<SavingsDetailsClassProvider>(context, listen: false)
          .setIsLoading();
    }
  }

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    // final Map<String, Object> args = ModalRoute.of(context).settings.arguments;

    return new WillPopScope(
      onWillPop: () async => false,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        // onTap: _handleUserInteraction,
        // onPanDown: _handleUserInteraction,
        child: SafeArea(
          child: Stack(children: [
            Image.asset(
              "assets/images/background-1.jpg",
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              color: Colors.black.withOpacity(0.55),
              colorBlendMode: BlendMode.darken,
              fit: BoxFit.cover,
            ),
            Container(
              margin: EdgeInsets.only(top: 20),
              decoration: BoxDecoration(
                color: kOffWhiteColor,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(2, 0), // changes position of shadow
                  ),
                ],
              ),
              child: new Container(),
            ),
            Scaffold(
              key: _scaffoldKey,
              resizeToAvoidBottomInset: false,
              backgroundColor: Colors.transparent,
              body: SafeArea(
                child: ListView(
                  physics: ClampingScrollPhysics(),
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                        color: kOffWhiteColor,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30),
                          topLeft: Radius.circular(30),
                        ),
                        /* boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            spreadRadius: 3,
                            blurRadius: 4,
                            offset: Offset(2, 0), // changes position of shadow
                          ),
                        ], */
                      ),
                      child: Column(
                        children: [
                          SavingsDetailsHeaderWidget(),
                          // SavingsDetailsScrollableCardsWidget(),
                          SavingsDetailsRecentTransactions(),
                        ],
                      ),

                      /* Column(
                        children: [
                          SavingsDetailsHeaderWidget(),
                          Container(
                            decoration: BoxDecoration(
                              color: kOffWhiteColor,
                            ),
                            child: SingleChildScrollView(
                              child: Container(
                                child: Column(
                                  children: [
                                    SavingsDetailsScrollableCardsWidget(),
                                    SavingsDetailsRecentTransactions(),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ), */
                    ),
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
