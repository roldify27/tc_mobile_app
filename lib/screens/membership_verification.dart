import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/screens/dashboard.dart';
import 'package:tcmobapp/screens/login_screen.dart';
import 'package:tcmobapp/screens/membership_fields.dart';
import 'package:tcmobapp/widgets/prompts/prompt_dialogs.dart';
import 'package:tcmobapp/widgets/rounded_text_widget.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:tcmobapp/provider/member_registration/member_registration_provider.dart';
import 'package:provider/provider.dart';

class MembershipVerification extends StatefulWidget {
  static String routeName = '/MembershipVerification';

  @override
  _MembershipVerificationState createState() => _MembershipVerificationState();
}

class _MembershipVerificationState extends State<MembershipVerification> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  FocusNode membershiptype_focusnode = new FocusNode();
  FocusNode customerid_focusnode = new FocusNode();
  FocusNode lastname_focusnode = new FocusNode();
  FocusNode desiredmobilenumber_focusnode = new FocusNode();
  FocusNode birthdate_focusnode = new FocusNode();

  var _isFocused_MembershipType = false;
  var _isFocused_CustomerID = false;
  var _isFocused_LastName = false;
  var _isFocused_MobileNumber = false;
  var _isFocused_BirthDate = false;

  var membershiptype_controller = TextEditingController();
  var customerid_controller = TextEditingController();
  var lastname_controller = TextEditingController();
  var desiredmobilenumber_controller = TextEditingController();
  var birthdate_controller = TextEditingController();

  var _showClearButton_MembershipType = false;
  var _showClearButton_CustomerID = false;
  var _showClearButton_LastName = false;
  var _showClearButton_MobileNumber = false;
  var _showClearButton_BirthDate = false;

  var _currencies = [
    "Food",
    "Transport",
    "Personal",
    "Shopping",
    "Medical",
    "Rent",
    "Movie",
    "Salary"
  ];

  var _currentSelectedValue;
  var selectedDate;
  var birthdateController = TextEditingController();
  var _isRegistrationCategorySelected;
  var _categorySelected = "";
  var _isToggledMemType = false;
  var _isLoadingSubmitted = false;

  _handleFocusChanged({@required String node_name}) {
    if (node_name == '1') {
      setState(() {
        FocusScope.of(context).requestFocus(membershiptype_focusnode);
      });
    } else if (node_name == '2') {
      setState(() {
        FocusScope.of(context).requestFocus(customerid_focusnode);
      });
    } else if (node_name == '3') {
      setState(() {
        FocusScope.of(context).requestFocus(lastname_focusnode);
      });
    } else if (node_name == '4') {
      setState(() {
        FocusScope.of(context).requestFocus(desiredmobilenumber_focusnode);
      });
    } else if (node_name == '5') {
      setState(() {
        FocusScope.of(context).requestFocus(birthdate_focusnode);
      });
    }
    // print('hi' + password_focusnode.hasFocus.toString());
  }

  void _onFocusChange() {
    debugPrint("Focus: " + customerid_focusnode.hasFocus.toString());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _isRegistrationCategorySelected = false;

    /* customerid_focusnode.addListener(() {
      print('field customer id: ' + customerid_focusnode.hasFocus.toString());
    }); */

    customerid_focusnode.addListener(() {
      setState(() {
        _isFocused_CustomerID = customerid_focusnode.hasFocus;
        _showClearButton_MembershipType = customerid_controller.text.length > 0;
      });
    });
    lastname_focusnode.addListener(() {
      setState(() {
        _isFocused_LastName = lastname_focusnode.hasFocus;
        _showClearButton_LastName = lastname_controller.text.length > 0;
      });
    });
    desiredmobilenumber_focusnode.addListener(() {
      setState(() {
        _isFocused_MobileNumber = desiredmobilenumber_focusnode.hasFocus;
        _showClearButton_MobileNumber =
            desiredmobilenumber_controller.text.length > 0;
      });
    });
    birthdate_focusnode.addListener(() {
      setState(() {
        _isFocused_BirthDate = birthdate_focusnode.hasFocus;
      });
    });

    /* customerid_controller.addListener(() {
      setState(() {
        
      });
    });
    lastname_controller.addListener(() {
      setState(() {
        
      });
    });
    desiredmobilenumber_controller.addListener(() {
      setState(() {
        
      });
    }); */

    /* membershiptype_focusnode.addListener(() {
      // if (_isToggledMemType == false) {
      print("Has focus: ${membershiptype_focusnode.hasFocus}");
      setState(() {
        FocusScope.of(context).requestFocus(membershiptype_focusnode);
      });
      print('field 1: ' + membershiptype_focusnode.hasFocus.toString());
      // }

      // _isToggledMemType = false;
    });
    customerid_focusnode.addListener(() {
      // if (_isToggledMemType == false) {
      // print("Has focus: ${customerid_focusnode.hasFocus}");
      setState(() {
        FocusScope.of(context).requestFocus(customerid_focusnode);
      });
      print('field 2231321dzczxc: ' + customerid_focusnode.hasFocus.toString());
      // }

      // _isToggledMemType = false;
      // _onFocusChange();
    });
    lastname_focusnode.addListener(() {
      // if (_isToggledMemType == false) {
      setState(() {
        FocusScope.of(context).requestFocus(lastname_focusnode);
      });
      print('field 3: ' + customerid_focusnode.hasFocus.toString());
      // }

      // _isToggledMemType = false;
    });
    desiredmobilenumber_focusnode.addListener(() {
      // if (_isToggledMemType == false) {
      setState(() {
        FocusScope.of(context).requestFocus(desiredmobilenumber_focusnode);
      });
      print('field 4: ' + desiredmobilenumber_focusnode.hasFocus.toString());
      // }

      // _isToggledMemType = false;
    });
    birthdate_focusnode.addListener(() {
      // if (_isToggledMemType == false) {
      setState(() {
        FocusScope.of(context).requestFocus(birthdate_focusnode);
      });
      print('field 5: ' + birthdate_focusnode.hasFocus.toString());
      // }

      // _isToggledMemType = false;
    }); */
  }

  @override
  void dispose() {
    // TODO: implement dispose
    membershiptype_focusnode.removeListener(() {});
    customerid_focusnode.removeListener(() {});
    lastname_focusnode.removeListener(() {});
    desiredmobilenumber_focusnode.removeListener(() {});
    birthdate_focusnode.removeListener(() {});
    // The attachment will automatically be detached in dispose().
    membershiptype_focusnode.dispose();
    customerid_focusnode.dispose();
    lastname_focusnode.dispose();
    desiredmobilenumber_focusnode.dispose();
    birthdate_focusnode.dispose();
    super.dispose();
  }

  Future<void> _verifyRegistrant() async {
    // await Provider.of<LoginProvider>(context, listen: false).setIsLoading();
    print("cid: ${customerid_controller.text}");
    print("mobile_number: ${desiredmobilenumber_controller.text}");

    setState(() {
      _isLoadingSubmitted = true;
    });

    await Future.delayed(Duration(seconds: 3), () async {
      // 5s over, navigate to a new page
    });

    var memberRegistrationProvider =
        await Provider.of<MemberRegistrationProvider>(context, listen: false);

    try {
      // await new Future.delayed(const Duration(seconds: 5));
      String hasAccount = await memberRegistrationProvider.verifyCustomer(
          customerid_controller.text, desiredmobilenumber_controller.text);

      if (hasAccount == "Already Exist") {
        showDialog(
          context: context,
          builder: (ctx) => PromptAlertDialogNotify(
            title: "Account Already Exist!",
            subtitle:
                "It looks like there is already an account created for this particular details.",
            okText: "Okay",
            /* onOk: () {
              setState(() {
                _isLoadingSubmitted = false;
              });
            }, */
          ),
        );
      } else if (hasAccount == "No Data") {
        showDialog(
          context: context,
          builder: (ctx) => PromptAlertDialogNotify(
            title: "Account Not Found!",
            subtitle:
                "Data corresponding to the entered credentials was not found.",
            okText: "Okay",
            /* onOk: () {
              setState(() {
                _isLoadingSubmitted = false;
              });
            }, */
          ),
        );
      } else {
        /* Future.delayed(Duration(seconds: 3), () {
          setState(() {
            Navigator.of(context).pushNamed(MembershipFields.routeName);
          });
        }); */
        Navigator.of(context).pushNamed(MembershipFields.routeName);
      }
    } catch (error) {
      print("ERROR: ${error}");
      // await kErrorApiDialog;
      await showDialog(
        context: context,
        builder: (ctx) => PromptAlertDialogError(
          title: "System Error!",
          subtitle:
              "There is something wrong happened within the server. Please try again.",
          okText: "Okay",
        ),
      );
    } finally {
      /* Future.delayed(Duration(seconds: 5), () {
        setState(() {
          _isLoadingSubmitted = false;
        });
      }); */
      setState(() {
        _isLoadingSubmitted = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var devSize = MediaQuery.of(context).size;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
          child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  // decoration: BoxDecoration(color: Colors.red),
                  // padding: EdgeInsets.only(left: 10),
                  child: IconButton(
                    icon: Icon(Icons.clear_rounded),
                    color: Colors.grey,
                    onPressed: () {
                      Navigator.of(context).popUntil((route) => route
                          .isFirst); //-->context here is this widget's position in the widget tree.
                    },
                  ),
                  alignment: Alignment.center,
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  alignment: Alignment.center,
                  // decoration: BoxDecoration(color: Colors.blue),
                  padding: EdgeInsets.only(top: 35),
                  child: Text(
                    'Member Verification',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: kHeaderTitleFontSize,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  // decoration: BoxDecoration(color: Colors.red),
                  // padding: EdgeInsets.only(left: 10),
                  child: IconButton(
                    icon: Icon(Icons.arrow_back_ios_rounded),
                    color: kPrimaryColor,
                    onPressed: () {
                      Navigator.of(context)
                          .pop(); //-->context here is this widget's position in the widget tree.
                    },
                  ),
                  alignment: Alignment.center,
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(
                top: 25,
                left: devSize.width * 0.10,
                right: devSize.width * 0.10),
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    child: DropdownSearch<String>(
                      validator: (v) =>
                          v == null ? "Membership Type is required." : null,
                      hint: "Select A Membership Type",
                      mode: Mode.DIALOG,
                      popupShape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      showSelectedItem: true,
                      items: ["Regular Membership", "Youth Lab Membership"],
                      label: "Membership Type *",
                      showClearButton: false,
                      onChanged: (data) {
                        print("data is: ${data}");

                        print(data != null);

                        setState(() {
                          _isToggledMemType = true;
                        });

                        if (data == null) {
                          setState(() {
                            _isRegistrationCategorySelected = false;
                          });

                          _categorySelected = "Nothing Selected";

                          print("enabled");
                        } else {
                          _formKey.currentState.reset();

                          setState(() {
                            _isRegistrationCategorySelected = true;
                          });

                          _categorySelected = data;

                          print("disabled");
                        }

                        customerid_controller.clear();
                        lastname_controller.clear();
                        desiredmobilenumber_controller.clear();
                        birthdate_controller.clear();
                      },
                      // popupItemDisabled: (String s) => s.startsWith('I'),
                      // selectedItem: "",
                      dropdownSearchDecoration: InputDecoration(
                        contentPadding: EdgeInsets.all(10),
                        labelStyle: TextStyle(
                            color: membershiptype_focusnode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                      enabled: _isRegistrationCategorySelected,
                      controller: customerid_controller,
                      focusNode: customerid_focusnode,
                      textInputAction: TextInputAction.next,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Customer ID',
                        labelStyle: TextStyle(
                            color: customerid_focusnode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: (_isRegistrationCategorySelected)
                            ? kEnabledFieldColor
                            : kDisabledFieldColor,
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                        suffixIcon: Visibility(
                          visible:
                              _showClearButton_MembershipType ? true : false,
                          child: IconButton(
                            onPressed: () {
                              customerid_controller.clear();
                            },
                            icon: Icon(Icons.clear),
                          ),
                        ),
                      ),
                      inputFormatters: [
                        new MaskTextInputFormatter(
                            mask: '###-#######',
                            filter: {"#": RegExp(r'[0-9]')})
                      ],
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Customer ID is required.';
                        } else if (value.length < 11) {
                          return 'Invalid Customer ID.';
                        }
                      },
                    ),
                  ),
                  Visibility(
                    visible: (_categorySelected.toUpperCase() ==
                            "Youth Lab Membership".toUpperCase())
                        ? true
                        : false,
                    child: Container(
                      padding: EdgeInsets.only(bottom: 20),
                      child: TextFormField(
                        enabled: _isRegistrationCategorySelected,
                        focusNode: lastname_focusnode,
                        controller: lastname_controller,
                        textInputAction: TextInputAction.next,
                        cursorColor: Color(0xFF00238A),
                        decoration: InputDecoration(
                          labelText: 'Lastname',
                          labelStyle: TextStyle(
                              color: lastname_focusnode.hasFocus
                                  ? Color(0xFF00238A)
                                  : Color(0xFF828282)),
                          fillColor: (_isRegistrationCategorySelected)
                              ? kEnabledFieldColor
                              : kDisabledFieldColor,
                          filled: true,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                          enabledBorder: new OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Color(0xFFE0E0E0),
                            ),
                          ),
                          focusedBorder: new OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Color(0xFF00238A),
                            ),
                          ),
                          suffixIcon: Visibility(
                            visible: _showClearButton_LastName ? true : false,
                            child: IconButton(
                              onPressed: () {
                                lastname_controller.clear();
                              },
                              icon: Icon(Icons.clear),
                            ),
                          ),
                        ),
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (value.isEmpty || !value.contains('@')) {
                            return 'Invalid email!';
                          }
                        },
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                      enabled: _isRegistrationCategorySelected,
                      focusNode: desiredmobilenumber_focusnode,
                      controller: desiredmobilenumber_controller,
                      textInputAction: TextInputAction.next,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: (_categorySelected.toUpperCase() ==
                                "Youth Lab Membership".toUpperCase())
                            ? 'Desired Mobile Number'
                            : 'Mobile Number',
                        labelStyle: TextStyle(
                            color: desiredmobilenumber_focusnode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: (_isRegistrationCategorySelected)
                            ? kEnabledFieldColor
                            : kDisabledFieldColor,
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                        suffixIcon: Visibility(
                          visible: _showClearButton_MobileNumber ? true : false,
                          child: IconButton(
                            onPressed: () {
                              desiredmobilenumber_controller.clear();
                            },
                            icon: Icon(Icons.clear),
                          ),
                        ),
                        /* prefixIcon: Container(
                          // alignment: Alignment.centerLet,
                          decoration: new BoxDecoration(
                              // borderRadius: new BorderRadius.circular(16.0),
                              border: Border(
                            right: BorderSide(
                                width: 2,
                                color: (_isRegistrationCategorySelected)
                                    ? Color(0xFFE4E6E8)
                                    : Colors.grey.withOpacity(0.30)),
                          )),
                          /* padding: EdgeInsets.only(
                              top: 15, bottom: 10, left: 10, right: 5),
                          margin: EdgeInsets.only(right: 10), */
                          child: Text(
                            '+63 ',
                            style: TextStyle(color: kFieldsHintTextColor),
                          ),
                        ), */
                        prefixText: '+63 ',
                      ),
                      inputFormatters: [
                        new MaskTextInputFormatter(
                            mask: '(###) ### - ####',
                            filter: {"#": RegExp(r'[0-9]')})
                      ],
                      keyboardType: TextInputType.phone,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Mobile Number is required.';
                        }
                      },
                      onFieldSubmitted: (val) {
                        if (!_isLoadingSubmitted &&
                            _categorySelected.toUpperCase() ==
                                "Regular Membership".toUpperCase()) {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();

                            _verifyRegistrant();

                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                          }
                        }
                      },
                    ),
                  ),
                  Visibility(
                    visible: (_categorySelected.toUpperCase() ==
                            "Youth Lab Membership".toUpperCase())
                        ? true
                        : false,
                    child: Container(
                      child: TextFormField(
                        enabled: _isRegistrationCategorySelected,
                        showCursor: true,
                        readOnly: true,
                        controller: birthdate_controller,
                        focusNode: birthdate_focusnode,
                        cursorColor: Color(0xFF00238A),
                        decoration: InputDecoration(
                          labelText: 'Birthdate',
                          labelStyle: TextStyle(
                              color: birthdate_focusnode.hasFocus
                                  ? Color(0xFF00238A)
                                  : Color(0xFF828282)),
                          fillColor: (_isRegistrationCategorySelected)
                              ? kEnabledFieldColor
                              : kDisabledFieldColor,
                          filled: true,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                          enabledBorder: new OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Color(0xFFE0E0E0),
                            ),
                          ),
                          focusedBorder: new OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Color(0xFF00238A),
                            ),
                          ),
                        ),
                        keyboardType: TextInputType.datetime,
                        validator: (value) {
                          if (value.isEmpty || !value.contains('@')) {
                            return 'Invalid email!';
                          }
                        },
                        onTap: () {
                          DatePicker.showDatePicker(context,
                              showTitleActions: true,
                              minTime: DateTime(2018, 1, 1),
                              maxTime: DateTime(2021, 12, 31),
                              onChanged: (date) {
                            print('change $date');
                          }, onConfirm: (date) {
                            selectedDate = date;
                            birthdate_controller.text =
                                new DateFormat.yMMMMd('en_US')
                                    .format(selectedDate);
                            print('confirm $date');
                          },
                              currentTime: DateTime.now(),
                              locale: LocaleType.en);
                        },
                      ),
                    ),
                  ),
                ],
              )),
            ),
          ),
          Expanded(child: Container()),
          Container(
            height: 60,
            width: double.maxFinite,
            margin: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 40),
            child: ElevatedButton(
              onPressed: () {
                if (!_isLoadingSubmitted) {
                  print("Button not loading!");
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();

                    _verifyRegistrant();

                    FocusScope.of(context).requestFocus(new FocusNode());
                  }
                }
              },
              style: ElevatedButton.styleFrom(
                primary: kPrimaryColor,
                padding: EdgeInsets.all(25),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(kButtonCircularRadius),
                ),
              ),
              child: (_isLoadingSubmitted)
                  ? Container(
                      // decoration: (BoxDecoration(color: Colors.red)),
                      // padding: EdgeInsets.all(20),
                      child: kButtonProgressIndicator(),
                    )
                  : Text(
                      'Proceed',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.5,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1),
                    ),
            ),
          ),
          /* Container(
            padding: EdgeInsets.only(
                top: 50,
                bottom: 40,
                left: devSize.width * 0.05,
                right: devSize.width * 0.05),
            width: double.infinity,
            child: RaisedButton(
              padding: EdgeInsets.all(23),
              onPressed: () {
                FocusScope.of(context).requestFocus(new FocusNode());

                _verifyRegistrant();
              },
              color: Color(0xFF00238A),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(kButtonCircularRadius),
                  side: BorderSide(color: Color(0xFF00238A))),
              child: (_isLoadingSubmitted)
                  ? Container(
                      padding: EdgeInsets.all(20),
                      child: Lottie.asset(
                        'assets/images/loading-dot.json',
                        alignment: Alignment.center,
                        fit: BoxFit.contain,
                        height: 10,
                        width: 100,
                      ),
                    )
                  : Text(
                      'Proceed',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 15.5,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 1),
                    ),
            ),
          ), */
        ],
      )),
    );
  }
}
