import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/screens/dashboard.dart';
import 'package:tcmobapp/screens/forgotpassword_change.dart';
import 'package:tcmobapp/screens/login_screen.dart';
import 'package:tcmobapp/widgets/rounded_text_widget.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';

class ForgotPasswordVerification extends StatefulWidget {
  static String routeName = '/ForgotPasswordVerification';

  @override
  _ForgotPasswordVerificationState createState() =>
      _ForgotPasswordVerificationState();
}

class _ForgotPasswordVerificationState
    extends State<ForgotPasswordVerification> {
  final GlobalKey<FormState> _formKey = GlobalKey();

  FocusNode customerIDFocusNode = new FocusNode();
  FocusNode mobileNumberFocusNode = new FocusNode();
  bool passwordVisible;
  bool retypeVisible;

  var _currentSelectedValue;
  var selectedDate;
  var birthdateController = TextEditingController();

  _handleFocusChanged({@required String node_name}) {
    if (node_name == '1') {
      setState(() {
        FocusScope.of(context).requestFocus(customerIDFocusNode);
      });
    } else if (node_name == '2') {
      setState(() {
        FocusScope.of(context).requestFocus(mobileNumberFocusNode);
      });
    }
    // print('hi' + password_focusnode.hasFocus.toString());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    passwordVisible = false;
    retypeVisible = false;

    customerIDFocusNode.addListener(() {
      setState(() {
        FocusScope.of(context).requestFocus(customerIDFocusNode);
      });
      print('hiyaa' + customerIDFocusNode.hasFocus.toString());
    });
    mobileNumberFocusNode.addListener(() {
      setState(() {
        FocusScope.of(context).requestFocus(mobileNumberFocusNode);
      });
      print('hiyaa' + mobileNumberFocusNode.hasFocus.toString());
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    customerIDFocusNode.removeListener(() {});
    mobileNumberFocusNode.removeListener(() {});
    // The attachment will automatically be detached in dispose().
    customerIDFocusNode.dispose();
    mobileNumberFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var devSize = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
          child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  // decoration: BoxDecoration(color: Colors.red),
                  // padding: EdgeInsets.only(left: 10),
                  child: IconButton(
                    icon: Icon(Icons.clear_rounded),
                    color: Colors.grey,
                    onPressed: () {
                      Navigator.of(context).popUntil((route) => route
                          .isFirst); //-->context here is this widget's position in the widget tree.
                    },
                  ),
                  alignment: Alignment.center,
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  alignment: Alignment.center,
                  // decoration: BoxDecoration(color: Colors.blue),
                  padding: EdgeInsets.only(top: 35),
                  child: Text(
                    'User Verification',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: kHeaderTitleFontSize,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                    // decoration: BoxDecoration(color: Colors.red),
                    // padding: EdgeInsets.only(left: 10),
                    /* child: IconButton(
                    icon: Icon(Icons.arrow_back_ios_rounded),
                    color: kPrimaryColor,
                    onPressed: () {
                      Navigator.of(context)
                          .pop(); //-->context here is this widget's position in the widget tree.
                    },
                  ),
                  alignment: Alignment.center, */
                    ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(
                top: 25,
                left: devSize.width * 0.10,
                right: devSize.width * 0.10),
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                      focusNode: customerIDFocusNode,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Customer ID',
                        labelStyle: TextStyle(
                            color: customerIDFocusNode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.isEmpty || !value.contains('@')) {
                          return 'Invalid email!';
                        }
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                      focusNode: mobileNumberFocusNode,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Registered Mobile Number',
                        labelStyle: TextStyle(
                            color: mobileNumberFocusNode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                        prefixIcon: Container(
                            decoration: new BoxDecoration(
                                // borderRadius: new BorderRadius.circular(16.0),
                                border: Border(
                              right: BorderSide(
                                  width: 2, color: Color(0xFFE4E6E8)),
                            )),
                            padding: EdgeInsets.only(
                                top: 10, bottom: 10, left: 10, right: 5),
                            margin: EdgeInsets.only(right: 10),
                            child: Text(
                              '+63 ',
                            )),
                      ),
                      keyboardType: TextInputType.phone,
                      validator: (value) {
                        if (value.isEmpty || !value.contains('@')) {
                          return 'Invalid email!';
                        }
                      },
                    ),
                  ),
                ],
              )),
            ),
          ),
          Expanded(child: Container()),
          Container(
            padding: EdgeInsets.only(
                top: 50,
                bottom: 40,
                left: devSize.width * 0.05,
                right: devSize.width * 0.05),
            width: double.infinity,
            child: RaisedButton(
              padding: EdgeInsets.all(23),
              onPressed: () {
                FocusScope.of(context).requestFocus(new FocusNode());
                Navigator.of(context).pushNamed(ForgotPasswordChange.routeName);
              },
              color: Color(0xFF00238A),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(kButtonCircularRadius),
                  side: BorderSide(color: Color(0xFF00238A))),
              child: Text(
                'Proceed',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15.5,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1),
              ),
            ),
          ),
        ],
      )),
    );
  }
}

class AboutWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("DONE!",
          style: TextStyle(
              color: Color(0xFF00238A),
              fontSize: 15.5,
              fontWeight: FontWeight.bold,
              letterSpacing: 1)),
      content: Text(
          'Your account has been successfully registered. You may now log on with your account.'),
      actions: [
        Container(
            padding: EdgeInsets.only(right: 10, bottom: 10),
            child: FlatButton(
                onPressed: () {
                  Navigator.of(context)
                      .pushReplacementNamed(LoginScreen.routeName);
                },
                child: Text("Proceed",
                    style: TextStyle(
                        color: Color(0xFF00238A),
                        fontSize: 15.5,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1))))
      ],
    );
  }
}
