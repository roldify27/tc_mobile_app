import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:tcmobapp/constants.dart';
import 'package:tcmobapp/provider/member_registration/member_verification_class.dart';
import 'package:tcmobapp/screens/dashboard.dart';
import 'package:tcmobapp/screens/login_screen.dart';
import 'package:tcmobapp/widgets/otp_code_verification_widget.dart';
import 'package:tcmobapp/widgets/prompts/prompt_dialogs.dart';
import 'package:tcmobapp/widgets/rounded_text_widget.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter_password_strength/flutter_password_strength.dart';
import 'package:tcmobapp/provider/member_registration/member_registration_class.dart';
import 'package:provider/provider.dart';
import '../provider/savings_details/savings_details_provider.dart';
import '../provider/member_registration/member_registration_provider.dart';

class MembershipFields extends StatefulWidget {
  static String routeName = '/MembershipFields';

  @override
  _MembershipFieldsState createState() => _MembershipFieldsState();
}

class _MembershipFieldsState extends State<MembershipFields> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  MemberRegistrationClass _userreg = new MemberRegistrationClass();
  MemberVerificationClass _userverify = new MemberVerificationClass();

  var username_controller = TextEditingController();
  var password_controller = TextEditingController();
  var retype_controller = TextEditingController();
  var email_controller = TextEditingController();
  var code_controller = TextEditingController();

  FocusNode userNameFocusNode = new FocusNode();
  FocusNode passwordFocusNode = new FocusNode();
  FocusNode reTypeFocusNode = new FocusNode();
  FocusNode emailFocusNode = new FocusNode();
  FocusNode codeFocusNode = new FocusNode();
  bool passwordVisible;
  bool retypeVisible;

  var _currentSelectedValue;
  var selectedDate;
  var birthdateController = TextEditingController();
  var _password = "";
  bool _isUniqueUsername = true;
  bool _isUniqueEmail = true;

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  _handleFocusChanged({@required String node_name}) {
    if (node_name == '1') {
      setState(() {
        FocusScope.of(context).requestFocus(userNameFocusNode);
      });
    } else if (node_name == '2') {
      setState(() {
        FocusScope.of(context).requestFocus(passwordFocusNode);
      });
    } else if (node_name == '3') {
      setState(() {
        FocusScope.of(context).requestFocus(reTypeFocusNode);
      });
    } else if (node_name == '4') {
      setState(() {
        FocusScope.of(context).requestFocus(emailFocusNode);
      });
    } else if (node_name == '5') {
      setState(() {
        FocusScope.of(context).requestFocus(codeFocusNode);
      });
    }
    // print('hi' + password_focusnode.hasFocus.toString());
  }

  Future<void> getCustomerData() async {
    _userverify =
        await Provider.of<MemberRegistrationProvider>(context, listen: false)
            .getRegistrantData;
  }

  Future<void> saveRegistration(MemberRegistrationClass userDetails,
      MemberVerificationClass userAccount, BuildContext context) async {
    // await Provider.of<MemberRegistrationProvider>(context, listen: false)
    //     .setIsLoading();

    kShowLoadingLogoOnly(_scaffoldKey.currentContext);

    await Future.delayed(Duration(seconds: 3), () async {
      // 5s over, navigate to a new page
    });

    var savingsProvider = await Provider.of<MemberRegistrationProvider>(
        _scaffoldKey.currentContext,
        listen: false);

    try {
      await savingsProvider.saveRegistration(userAccount, userDetails);
      await kDiscardLogoLoadingOnly();
      await showDialog(
        context: _scaffoldKey.currentContext,
        builder: (ctx) => PromptAlertDialogRegistrationDone(
          title: "Registration Done",
          subtitle:
              "Your account has been successfully registered. You may now log on with your account.",
          okText: "Proceed",
        ),
      );
    } catch (error) {
      print("ERROR: ${error}");
      await showDialog(
        context: _scaffoldKey.currentContext,
        builder: (ctx) => PromptAlertDialogError(
          title: "System Error!",
          subtitle:
              "There is something wrong happened within the server. Please try again.",
          okText: "Okay",
        ),
      );
    } finally {
      // await Provider.of<SavingsDetailsClassProvider>(context, listen: false)
      //     .setIsLoading();
    }
  }

  Future<void> checkUniqueUsername(String userName) async {
    var memberRegistrationProvider =
        await Provider.of<MemberRegistrationProvider>(context, listen: false);

    try {
      bool isUniqueBool =
          await memberRegistrationProvider.checkUniqueUsername(userName);

      print("isUniqueBool: ${isUniqueBool}");

      if (isUniqueBool) {
        setState(() {
          _isUniqueUsername = true;
        });
      } else {
        setState(() {
          _isUniqueUsername = false;
        });
      }

      print(_isUniqueUsername);
    } catch (error) {
      setState(() {
        _isUniqueUsername = false;
      });
    } finally {}
  }

  Future<void> checkUniqueEmail(String email) async {
    var memberRegistrationProvider =
        await Provider.of<MemberRegistrationProvider>(context, listen: false);

    try {
      bool isUniqueBool =
          await memberRegistrationProvider.checkUniqueEmail(email);

      print("isUniqueBool: ${isUniqueBool}");

      if (isUniqueBool) {
        setState(() {
          _isUniqueEmail = true;
        });
      } else {
        setState(() {
          _isUniqueEmail = false;
        });
      }

      print(_isUniqueEmail);
    } catch (error) {
      setState(() {
        _isUniqueEmail = false;
      });
    } finally {}
  }

  void proceed() {
    FocusScope.of(context).requestFocus(new FocusNode());

    setState(() {
      passwordVisible = false;
      retypeVisible = false;
    });

    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      String otp = kOTPCodeGenerate();
      _userreg.code = otp;

      showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.black.withOpacity(0.9),
        builder: (BuildContext context) {
          return OTPCodeVerificationWidget(
              phoneNumber: _userverify.mobileNumber,
              otp: otp,
              transactionType: "USER REGISTRATION",
              onSubmit: () {
                saveRegistration(_userreg, _userverify, context);
              });
          // otpDialog(context, "09958801593",)
        },
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getCustomerData();

    passwordVisible = false;
    retypeVisible = false;

    userNameFocusNode.addListener(() {
      setState(() {
        FocusScope.of(context).requestFocus(userNameFocusNode);
      });
      print('hiyaa' + userNameFocusNode.hasFocus.toString());
    });
    passwordFocusNode.addListener(() {
      setState(() {
        FocusScope.of(context).requestFocus(passwordFocusNode);
      });
      print('hiyaa' + passwordFocusNode.hasFocus.toString());
    });
    reTypeFocusNode.addListener(() {
      setState(() {
        FocusScope.of(context).requestFocus(reTypeFocusNode);
      });
      print('hiyaa' + passwordFocusNode.hasFocus.toString());
    });
    emailFocusNode.addListener(() {
      setState(() {
        FocusScope.of(context).requestFocus(emailFocusNode);
      });
      print('hiyaa' + emailFocusNode.hasFocus.toString());
    });
    codeFocusNode.addListener(() {
      setState(() {
        FocusScope.of(context).requestFocus(codeFocusNode);
      });
      print('hiyaa' + codeFocusNode.hasFocus.toString());
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    userNameFocusNode.removeListener(() {});
    passwordFocusNode.removeListener(() {});
    reTypeFocusNode.removeListener(() {});
    emailFocusNode.removeListener(() {});
    codeFocusNode.removeListener(() {});
    // The attachment will automatically be detached in dispose().
    userNameFocusNode.dispose();
    passwordFocusNode.dispose();
    reTypeFocusNode.dispose();
    emailFocusNode.dispose();
    codeFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var devSize = MediaQuery.of(context).size;

    var memberRegistrationProvider =
        Provider.of<MemberRegistrationProvider>(context, listen: true);

    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      body: SafeArea(
          child: Column(
        children: [
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  // decoration: BoxDecoration(color: Colors.red),
                  // padding: EdgeInsets.only(left: 10),
                  child: IconButton(
                    icon: Icon(Icons.clear_rounded),
                    color: Colors.grey,
                    onPressed: () {
                      Navigator.of(context).popUntil((route) => route
                          .isFirst); //-->context here is this widget's position in the widget tree.
                    },
                  ),
                  alignment: Alignment.center,
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  alignment: Alignment.center,
                  // decoration: BoxDecoration(color: Colors.blue),
                  padding: EdgeInsets.only(top: 35),
                  child: Text(
                    'Register',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: kHeaderTitleFontSize,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  // decoration: BoxDecoration(color: Colors.red),
                  // padding: EdgeInsets.only(left: 10),
                  child: IconButton(
                    icon: Icon(Icons.arrow_back_ios_rounded),
                    color: kPrimaryColor,
                    onPressed: () {
                      Navigator.of(context)
                          .pop(); //-->context here is this widget's position in the widget tree.
                    },
                  ),
                  alignment: Alignment.center,
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(
                top: 25,
                left: devSize.width * 0.10,
                right: devSize.width * 0.10),
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                  child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                      controller: username_controller,
                      focusNode: userNameFocusNode,
                      textInputAction: TextInputAction.next,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Username *',
                        labelStyle: TextStyle(
                            color: userNameFocusNode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.text,
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        checkUniqueUsername(value);
                      },
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Invalid Username!';
                        } else if (_isUniqueUsername == false) {
                          return 'Username must be unique.';
                        }
                      },
                      onFieldSubmitted: (data) {
                        kFieldFocusChange(
                            context, userNameFocusNode, passwordFocusNode);
                      },
                      onSaved: (val) => setState(() => _userreg.userName = val),
                    ),
                  ),
                  Visibility(
                    visible: passwordFocusNode.hasFocus ? true : false,
                    child: Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: FlutterPasswordStrength(
                        password: _password,
                        strengthColors: TweenSequence<Color>(
                          [
                            TweenSequenceItem(
                              weight: 1.0,
                              tween: ColorTween(
                                begin: Colors.red,
                                end: Colors.yellow,
                              ),
                            ),
                            TweenSequenceItem(
                              weight: 1.0,
                              tween: ColorTween(
                                begin: Colors.yellow,
                                end: Colors.blue,
                              ),
                            ),
                            TweenSequenceItem(
                              weight: 1.0,
                              tween: ColorTween(
                                begin: Colors.blue,
                                end: kPrimaryColor,
                              ),
                            ),
                          ],
                        ),
                        strengthCallback: (strength) {
                          debugPrint(strength.toString());
                        },
                        radius: 15,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                        obscureText: !passwordVisible,
                        controller: password_controller,
                        focusNode: passwordFocusNode,
                        textInputAction: TextInputAction.next,
                        cursorColor: Color(0xFF00238A),
                        decoration: InputDecoration(
                          labelText: 'Password *',
                          // hintText: 'Enter Password',
                          errorMaxLines: 10,
                          helperText:
                              'Password must consist of all of the following:\n'
                              '  • A minimum of eight(8) characters.\n'
                              '  • One(1) lowercase letter.\n'
                              '  • One(1) uppercase letter.\n'
                              '  • One(1) digit.\n'
                              '  • One(1) special character.\n',
                          helperMaxLines: 10,
                          labelStyle: TextStyle(
                              color: passwordFocusNode.hasFocus
                                  ? Color(0xFF00238A)
                                  : Color(0xFF828282)),
                          fillColor: Color(0xffF2F2F2),
                          filled: true,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                          enabledBorder: new OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Color(0xFFE0E0E0),
                            ),
                          ),
                          focusedBorder: new OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(
                              color: Color(0xFF00238A),
                            ),
                          ),
                          suffixIcon: Visibility(
                            visible: passwordFocusNode.hasFocus,
                            child: IconButton(
                              icon: Icon(
                                // Based on passwordVisible state choose the icon
                                passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Color(0xFF00238A),
                              ),
                              onPressed: () {
                                // Update the state i.e. toogle the state of passwordVisible variable
                                setState(() {
                                  passwordVisible = !passwordVisible;
                                });
                              },
                            ),
                          ),
                          prefixIcon: Container(
                            decoration: new BoxDecoration(
                                // borderRadius: new BorderRadius.circular(16.0),
                                border: Border(
                              right: BorderSide(
                                  width: 2, color: Color(0xFFE4E6E8)),
                            )),
                            padding: EdgeInsets.only(
                                top: 10, bottom: 10, left: 10, right: 5),
                            margin: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.vpn_key_rounded,
                              size: 20,
                              // color: Color(0xFF00238A),
                            ),
                          ),
                        ),
                        keyboardType: TextInputType.text,
                        validator: (password) {
                          /* Pattern pattern =
                            r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$";
                        RegExp regex = new RegExp(pattern);
                        if (!regex.hasMatch(value))
                          return 'Invalid Password';
                        else
                          return null; */
                          Pattern pattern =
                              r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$";
                          RegExp regex = new RegExp(pattern);
                          if (!regex.hasMatch(password))
                            return 'You have entered an invalid password! Your password must consist of all of the following:\n'
                                '  • A minimum of eight(8) characters.\n'
                                '  • One(1) lowercase letter.\n'
                                '  • One(1) uppercase letter.\n'
                                '  • One(1) digit.\n'
                                '  • One(1) special character.\n';
                          else
                            return null;
                        },
                        onFieldSubmitted: (data) {
                          kFieldFocusChange(
                              context, passwordFocusNode, reTypeFocusNode);
                        },
                        onChanged: (value) {
                          setState(() {
                            _password = value;
                          });
                        },
                        onSaved: (value) {
                          setState(() {
                            _password = value;
                            _userreg.passWord = value;
                          });
                        }),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                      obscureText: !retypeVisible,
                      controller: retype_controller,
                      focusNode: reTypeFocusNode,
                      textInputAction: TextInputAction.next,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Re-Type Password *',
                        labelStyle: TextStyle(
                            color: reTypeFocusNode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                        suffixIcon: Visibility(
                          visible: reTypeFocusNode.hasFocus,
                          child: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              retypeVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Color(0xFF00238A),
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                retypeVisible = !retypeVisible;
                              });
                            },
                          ),
                        ),
                        prefixIcon: Container(
                          decoration: new BoxDecoration(
                              // borderRadius: new BorderRadius.circular(16.0),
                              border: Border(
                            right:
                                BorderSide(width: 2, color: Color(0xFFE4E6E8)),
                          )),
                          padding: EdgeInsets.only(
                              top: 10, bottom: 10, left: 10, right: 5),
                          margin: EdgeInsets.only(right: 10),
                          child: Icon(
                            Icons.vpn_key_rounded,
                            size: 20,
                            // color: Color(0xFF00238A),
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Invalid Re-Type Password!';
                        } else if (value != password_controller.text) {
                          return 'Password does not match!';
                        }
                      },
                      onFieldSubmitted: (data) {
                        kFieldFocusChange(
                            context, reTypeFocusNode, emailFocusNode);
                      },
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(bottom: 20),
                    child: TextFormField(
                      controller: email_controller,
                      focusNode: emailFocusNode,
                      textInputAction: TextInputAction.done,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Email *',
                        labelStyle: TextStyle(
                            color: emailFocusNode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) {
                        checkUniqueEmail(value);
                      },
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (email) {
                        if (email.isEmpty) {
                          return 'Invalid Username!';
                        } else if (!EmailValidator.validate(email)) {
                          return 'Invalid email format!';
                        } else if (_isUniqueEmail == false) {
                          return 'Email must be unique.';
                        }
                      },
                      onFieldSubmitted: (data) {
                        /* kFieldFocusChange(
                            context, emailFocusNode, codeFocusNode); */
                        proceed();
                      },
                      onSaved: (val) => setState(() => _userreg.email = val),
                    ),
                  ),
                  /* Container(
                    child: TextFormField(
                      maxLength: 5,
                      controller: code_controller,
                      focusNode: codeFocusNode,
                      textInputAction: TextInputAction.done,
                      cursorColor: Color(0xFF00238A),
                      decoration: InputDecoration(
                        labelText: 'Code',
                        labelStyle: TextStyle(
                            color: codeFocusNode.hasFocus
                                ? Color(0xFF00238A)
                                : Color(0xFF828282)),
                        fillColor: Color(0xffF2F2F2),
                        filled: true,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFFE0E0E0),
                          ),
                        ),
                        focusedBorder: new OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: BorderSide(
                            color: Color(0xFF00238A),
                          ),
                        ),
                      ),
                      keyboardType: TextInputType.number,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Invalid Code!';
                        } else if (!isNumeric(value)) {
                          return 'Invalid Code!';
                        } else if (value.length < 5) {
                          return 'Invalid Code!';
                        }
                      },
                      onFieldSubmitted: (data) {
                        setState(() {
                          passwordVisible = false;
                          retypeVisible = false;
                        });

                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();

                          saveRegistration(_userreg, context);

                          print("USER DETAILS: " +
                              _userreg.userName +
                              ", " +
                              _userreg.passWord +
                              ", " +
                              _userreg.email +
                              ", " +
                              _userreg.code);

                          FocusScope.of(context).requestFocus(new FocusNode());
                        }
                      },
                      onSaved: (val) => setState(() => _userreg.code = val),
                    ),
                  ), */
                ],
              )),
            ),
          ),
          Expanded(child: Container()),
          Container(
            padding: EdgeInsets.only(
                top: 50,
                bottom: 40,
                left: devSize.width * 0.05,
                right: devSize.width * 0.05),
            width: double.infinity,
            child: RaisedButton(
              padding: EdgeInsets.all(23),
              onPressed: () {
                proceed();
              },
              /* {
                Navigator.of(context).pushReplacementNamed(Dashboard.routeName);
              }, */
              color: Color(0xFF00238A),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(kButtonCircularRadius),
                  side: BorderSide(color: Color(0xFF00238A))),
              child: Text(
                'Register',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15.5,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1),
              ),
            ),
          ),
        ],
      )),
    );
  }
}

class AboutWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      title: Text("DONE!",
          style: TextStyle(
              color: Color(0xFF00238A),
              fontSize: 15.5,
              fontWeight: FontWeight.bold,
              letterSpacing: 1)),
      content: Text(
          'Your account has been successfully registered. You may now log on with your account.'),
      actions: [
        Container(
            padding: EdgeInsets.only(right: 10, bottom: 10),
            child: FlatButton(
                onPressed: () {
                  Navigator.of(context)
                      .pushReplacementNamed(LoginScreen.routeName);
                },
                child: Text("Proceed",
                    style: TextStyle(
                        color: Color(0xFF00238A),
                        fontSize: 15.5,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1))))
      ],
    );
  }
}
